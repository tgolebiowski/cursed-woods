#include "GuiManager.h"


GuiManager::GuiManager(Core* core, Scene* scene, Input* input, GameLogicHub* gameLogicHub, Vector2 screensize)
{
	this->core = core;
	this->guiScene = scene;
	this->player = player;
	this->input = input;
	this->gameLogicHub = gameLogicHub;

	guiScreenVirtualScene = new Scene(Scene::SCENE_3D, true);
	guiScreenVirtualScene->getDefaultCamera()->setOrthoSize(screensize.x, screensize.y);
	guiScreenVirtualScene->getDefaultCamera()->setPosition(Vector3(0, 0, 0));
	guiScreenVirtualScene->getDefaultCamera()->lookAt(Vector3(0, 0, 1));

	SceneLabel::defaultAnchor = Vector3(-1.0, 1.0, 0.0);
	SceneLabel::defaultSnapToPixels = true;

	guiScreenRenderTexture = new SceneRenderTexture(guiScreenVirtualScene, 
		guiScreenVirtualScene->getActiveCamera(), screensize.x, screensize.y, true);

	guiScreenImage = new SceneImage(guiScreenRenderTexture->getTargetTexture());
	guiScreenImage->setPosition(screensize.x / 2.0, screensize.y / 2.0);
	scene->addEntity(guiScreenImage);

	currentScreen = NULL;

	blackScreened = false;

	debugInfoSpot = new ScenePrimitive(ScenePrimitive::TYPE_VPLANE, 180.0f, 70.0f);
	debugInfoSpot->setPositionX(95.0);
	debugInfoSpot->setPositionY(37.0);
	debugInfoSpot->setColor(140.0f / 255.0f, 227.0f / 255.0f, 253.0 / 255.0, 0.25);
	scene->addEntity(debugInfoSpot);

	//debug info labels
	//sceneEntityCountLabel = new SceneLabel("Entity Count: ", 12);
	//sceneEntityCountLabel->setAnchorPoint(-1.0f, 1.0f, 0.0f);
	//sceneEntityCountLabel->snapToPixels = true;
	//sceneEntityCountLabel->setPosition(30, 15);
	//sceneEntityCountLabel->setColor(39.0 / 255.0, 27.0 / 255.0, 48.0 / 255.0, 1.0f);
	//scene->addEntity(sceneEntityCountLabel);

	fpsLabel = new SceneLabel("FPS: ", 12);
	fpsLabel->setPosition(30, 15);
	fpsLabel->setColor(39.0 / 255.0, 27.0 / 255.0, 48.0 / 255.0, 1.0f);
	scene->addEntity(fpsLabel);

	renderCountLabel = new SceneLabel("Render Count: ", 12);
	renderCountLabel->setPosition(30, 30);
	renderCountLabel->setColor(39.0 / 255.0, 27.0 / 255.0, 48.0 / 255.0, 1.0f);
	scene->addEntity(renderCountLabel);

	physicsEntityCountLabel = new SceneLabel("Collision Entities: ", 12);
	physicsEntityCountLabel->setPosition(30, 45);
	physicsEntityCountLabel->setColor(39.0 / 255.0, 27.0 / 255.0, 48.0 / 255.0, 1.0f);
	scene->addEntity(physicsEntityCountLabel);

	blackScreen = new ScenePrimitive(ScenePrimitive::TYPE_VPLANE, screensize.x, screensize.y);
	blackScreen->setColor(0,0,0,1.0f);
	blackScreen->setPosition((screensize.x/2.0f), (screensize.y/2.0f));
	scene->addEntity(blackScreen);

	pinkTransitionDot = new ScenePrimitive(ScenePrimitive::TYPE_CIRCLE, 32, 32, 8);
	pinkTransitionDot->setColor(0.77, 0.03, 0.15, 1.0);
	pinkTransitionDot->visible = false;
	pinkTransitionDot->setPosition(blackScreen->getPosition());
	scene->addEntity(pinkTransitionDot);

	emberCurseScreen = new EmberCurseScreen(guiScreenVirtualScene, input, player);
	mapScreen = new MapScreen(guiScreenVirtualScene, input, gameLogicHub->worldMap);
}


GuiManager::~GuiManager(void)
{
}

void GuiManager::update()
{
	if(blackScreened)
	{
		if(blackScreen->getCombinedColor().a < 1.0f)
		{
			Color color = blackScreen->getCombinedColor();
			color.a += 0.2f;
			blackScreen->setColor(color);
		}
	}
	else
	{
		if(blackScreen->getCombinedColor().a > 0.0f)
		{
			Color color = blackScreen->getCombinedColor();
			color.a -= 0.2f;
			blackScreen->setColor(color);
		}
	}

	pinkTransitionDot->visible = gameLogicHub->transitioning && gameLogicHub->transitionDelayLeft == 0;

	if (currentScreen != NULL && currentScreen->closeFlag)
	{
		currentScreen->unload();
		currentScreen = NULL;
	}
	else if (currentScreen != NULL)
	{
		currentScreen->update();
	}


	if (input->pauseWasPressed && currentScreen == NULL)
	{
		currentScreen = emberCurseScreen;
		currentScreen->loadin();
	}
	else if (input->selectWasPressed && currentScreen == NULL)
	{
		currentScreen = mapScreen;
		currentScreen->loadin();
	}

	String fpsText = "FPS: " + String::NumberToString(core->getFPS(), 0);
	fpsLabel->setText(fpsText);

	String renderCountText = "Render Count: " + String::NumberToString(gameLogicHub->currentLevel->lastLargeMeshRenderCount, 0);
	renderCountLabel->setText(renderCountText);

	String entityCountText = "Entity Count: " + String::NumberToString(gameLogicHub->currentLevel->scene->rootEntity.getNumChildren(), 0);
	physicsEntityCountLabel->setText(entityCountText);
}
