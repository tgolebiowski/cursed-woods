#pragma once
#ifndef WORLDGENERATION
#define WORLDGENERATION
#include <Polycode.h>
#include "Delaunay_Src\Poisson_Distributer.h"
#include "Delaunay_Src\Triangulator.h"
#include "SaveStructs.h"
#include "Level.h"
#include "WorldMap.h"
#include "LevelSaverLoader.h"
#include "LightShaderHub.h"

using namespace Polycode;

struct LevelGenInfo
{
	int identifier;
	float objectDensity;
	std::vector<Vector2> transitionDirections;
	std::vector<int> transitionTo;
	std::vector<int> specialObjects;
	std::vector<Vector2> spcObjectLoc;
	std::vector<Vector2> borderPoints;

	float averageCurse;
	String npc_type;
	String gameObj_type;

	LevelGenInfo()
	{
		averageCurse = 0.5f;
		objectDensity = 8.0f;
		npc_type = "NONE";
		gameObj_type = "NONE";
	}
};

class WorldGeneration
{
public:
	WorldGeneration();
	~WorldGeneration();

	void generateLevel(LevelGenInfo genInfo, LevelLayoutStruct* levelInfo, LevelStateInfo* stateInfo);
	void generateBigDebugWorld(WorldMap* map, LevelSaverLoader* loader);
	void generateWorldMap(WorldMap* map, std::vector<LevelGenInfo>* levelGenInfoList);
	Level* generateVisualTestLevel(LevelSaverLoader* saverloader);

private:

	//world generation helper methods
	Triangulator* triangulator;
	void roughLayout(WorldMap* map);
	void generateGenerationInfo(WorldMap* map, std::vector<LevelGenInfo>* levelGenInfoList);

	//level generation helper methods
	//void createPath(Vector2 startPoint, Vector2 endPoint, std::vector<Vector2>* pointProgression, Triangulator* triang);
	
};
#endif

