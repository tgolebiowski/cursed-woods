#include "NPCManager.h"


NPCManager::NPCManager(Player* player, VFXManager* vfxManager)
{
	this->player = player;
	this->vfxManager = vfxManager;
	scene = NULL;
	level = NULL;

	//DemonBoy* demonBoy = new DemonBoy(player, scene, &npcList, level->navMesh);
	//npcList.push_back(demonBoy);
}


NPCManager::~NPCManager(void)
{
}

void NPCManager::update()
{
	for(int i = 0; i < npcList.size(); i++)
	{
		npcList.at(i)->update();

		//Level* level = activeModule->GetChunk(enemies.at(i)->hitBox->getEntity()->getPosition().x, enemies.at(i)->hitBox->getEntity()->getPosition().z);
		//float yHeight = level->getHeightAtPoint(enemies.at(i)->hitBox->getEntity()->getPosition());
		//enemies.at(i)->hitBox->getEntity()->setPositionY(yHeight
		npcList.at(i)->collisionCollider->getEntity()->setPositionY(0.5f);

		CollisionResult effectResult = scene->testCollision(npcList.at(i)->effectCollider->getEntity(), player->collisionSphere->getEntity());
		CollisionResult spearResult = scene->testCollision(npcList.at(i)->collisionCollider->getEntity(), player->spearTip->getEntity());
		CollisionResult emberResult = scene->testCollision(npcList.at(i)->collisionCollider->getEntity(), player->emberSphere->getEntity());

		if(effectResult.collided) //do forced resource trade

		if(spearResult.collided && player->playerState->currentEquip == PlayerState::Equipment::Spear)
		{
			if(player->playerState->spearMovementThisFrame.length() > 0.2f)
			{
				npcList.at(i)->reactToHit(std::log(player->playerState->curseBuildUp));
				Vector3 vec = npcList.at(i)->collisionCollider->getEntity()->getPosition() - player->spearTip->getEntity()->getPosition();
				vec.setLength(player->spearTip->getEntity()->getWidth() / 2.0f);
				//new HitEffect(player->spearTip->getEntity()->getPosition() + vec);
			}
		}

		if (player->playerState->currentEquip == PlayerState::Equipment::Ember && 
			emberResult.collided && 
			player->playerState->emberExtended)
		{
			//do ember stuff
		}
	}
}