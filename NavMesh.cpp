#include "NavMesh.h"


NavMesh::NavMesh(void)
{
	pather = NULL;
	map = NULL;
}


NavMesh::~NavMesh(void)
{
	for (int i = 0; i < debugLines.size(); i++)
		delete debugLines.at(i);

	if (pather != NULL)
		delete pather;

	if (map != NULL)
		delete map;
}

void NavMesh::init(std::vector<Vector2> collisionPoints, std::vector<Vector2> borderPoints)
{
	const float minClearanceCutoff = 1.75;
	const float maxDist = 40.0;

	struct PolygonCheck{
		bool isInterior(Vector2 p, std::vector<Vector2> borderPoints)
		{
			int above = 0;
			int below = 0;

			for (int j = 0; j < borderPoints.size(); j++)
			{
				int index1 = j;
				int index2 = j + 1;
				if (index1 == borderPoints.size() - 1) index2 = 0;

				Vector2 p1 = borderPoints.at(index1);
				Vector2 p2 = borderPoints.at(index2);

				if ((p1.x <= p.x && p2.x >= p.x) || (p2.x <= p.x && p1.x >= p.x))
				{
					Vector2 vecToP = p - p1;
					Vector2 vecToP2 = p2 - p1;
					float cross = vecToP2.crossProduct(vecToP);

					if (cross >= 0.0f)
						above++;
					else
						below++;
				}
			}

			return !((above % 2 == 1) && (below % 2 == 1));
		}

	};
	PolygonCheck borderCheck;

	std::vector<Site*> collisionSites;
	for (int i = 0; i < collisionPoints.size(); i++)
		if (borderCheck.isInterior(collisionPoints.at(i), borderPoints))
			collisionSites.push_back(new Site(collisionPoints.at(i)));

	for (int i = 0; i < borderPoints.size(); i++)
		collisionSites.push_back(new Site(borderPoints[i]));

	siteFinder = new Triangulator(&collisionSites);

	map = new VoronoiGraph(siteFinder);
	
	for (int i = 0; i < map->nodeCount; i++)
	{
		VoronoiNode* node = &map->nodes[i];
		for (int j = 0; j < 3; j++)
		{
			if (node->neighbors[j] != NULL)
			{
				Vector3 p1 = Vector3(node->position.x, 0.1, node->position.y);
				Vector3 p2 = Vector3(node->neighbors[j]->position.x, 0.1, node->neighbors[j]->position.y);
				SceneLine* line = new SceneLine(p1, p2);
				debugLines.push_back(line);
			}
		}
	}

	for (int i = 0; i < siteFinder->tris.size(); i++)
	{
		Triangle* tri = siteFinder->tris[i];
		VoronoiNode* node = &map->nodes[i];

		tri->voronoiHolder = (void*)node;
	}

	pather = new MicroPather(this, map->nodeCount, 4, false);
}

void NavMesh::createPath(std::vector<Vector3>* outPath, Vector3 worldSpaceStart, Vector3 worldSpaceEnd)
{
	Vector2 vec2Start = Vector2(worldSpaceStart.x, worldSpaceStart.z);
	Vector2 vec2End = Vector2(worldSpaceEnd.x, worldSpaceEnd.z);

	Triangle* startTri = siteFinder->getTri(vec2Start.x, vec2Start.y);
	VoronoiNode* startNode = (VoronoiNode*)startTri->voronoiHolder;

	Triangle* endTri = siteFinder->getTri(vec2End.x, vec2End.y);
	VoronoiNode* endNode = (VoronoiNode*)endTri->voronoiHolder;

	micropather::MPVector<void*> path;
	float cost = 0.0f;
	pather->Solve((void*)startNode, (void*)endNode, &path, &cost);

	for(int i = 0; i < path.size(); i++)
	{
		VoronoiNode* node = (VoronoiNode*)path[i];
		Vector3 worldPosition = Vector3(node->position.x, 0.0f, node->position.y);
		outPath->push_back(worldPosition);
	}

	outPath->push_back(worldSpaceEnd);
}

float NavMesh::LeastCostEstimate(void* startState, void* endState)
{
	return (((VoronoiNode*)startState)->position - ((VoronoiNode*)endState)->position).length();
}

void NavMesh::AdjacentCost(void* state, MP_VECTOR< micropather::StateCost > *adjacent)
{
	VoronoiNode* node = (VoronoiNode*)state;

	for(int i = 0; i < 3; i++)
	{
		if(node->neighbors[i] != NULL)
		{
			micropather::StateCost cost = micropather::StateCost();
			cost.cost = (node->position - node->neighbors[i]->position).length();
			cost.state = (void*)node->neighbors[i];
			adjacent->push_back(cost);
		}
	}
}

void NavMesh::PrintStateInfo(void* state)
{

}
