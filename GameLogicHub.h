#pragma once
#include <Polycode.h>
#include <Polycode3DPhysics.h>
#ifndef GAMELOGICHUB_H
#define GAMELOGICHUB_H
#include "Level.h"
#include "Delaunay_Src\Poisson_Distributer.h"
#include "Delaunay_Src\Triangulator.h"
#include "NavMesh.h"
#include "VFXManager.h"
#include "SaveStructs.h"
#include "LevelSaverLoader.h"
#include "WorldGeneration.h"
#include "WorldMap.h"
#include "LightShaderHub.h"
#include "NPCManager.h"
#include "PhysicsManager.h"
#include "Input.h"

using namespace Polycode;

class Player;

class GameLogicHub
{
public:
	GameLogicHub();
	GameLogicHub(CollisionScene* scene, Vector2 screenSize, Input* input);
	~GameLogicHub(void);

	Level* currentLevel;
	WorldMap* worldMap;

	Player* player;
	PhysicsManager* physicsManager;
	NPCManager* npcManager;
	GameCam* gameCam;

	bool transitioning;
	int transitionDelayLeft;
	void update();
	void switchToLevel(int identifier);
	
	std::vector<Level*> levelsInMemory;
	LevelStateInfo* currentLevelState;

	int deleteCountdown;
	std::vector<Level*> toDelete;

private:
	CollisionScene* scene;
	struct RequestedLevelInfo
	{
		Level* levelPointer;
		bool donePointer;
		int identifier;
	};
	Input* input;
	VFXManager* vfxManager;
	LevelSaverLoader* loader;
	LightShaderHub* lightShaderHub;

	LevelStateInfo allLevelState[50];
	int nextLevel;

	std::vector<RequestedLevelInfo*> requestedLevels;
	void checkLoadedLevels();
	void initWorld();

	Vector2 screenSize;
	Vector2 scriptVec;
};
#endif