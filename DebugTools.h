#include <Polycode.h>
#ifndef DEBUG_TOOLS
#define DEBUG_TOOLS
#include "Player.h"
#include "GameLogicHub.h"
#include "GameCam.h"

using namespace Polycode;

struct DebugTools
{

	struct Deeb_Key
	{
		int keyVal;
		bool lastState;
		bool wasPressed;

		Deeb_Key(int val)
		{
			keyVal = val;
			lastState = false;
			wasPressed = false;
		}
	};

	std::vector<Deeb_Key> deebKeys;
	std::vector<String> cmdInfo;

	//Debug Toggles
	bool cameraZoomedOut;
	bool navMeshOn;

	//Requisite Refernces
	Core* core;
	Player* player;
	GameLogicHub* gameLogicHub;
	GameCam* gameCam;

	DebugTools(Core* core, Player* player, GameLogicHub* gameLogicHub, GameCam* gameCam)
	{
		this->core = core;
		this->player = player;
		this->gameLogicHub = gameLogicHub;
		this->gameCam = gameCam;

		deebKeys.push_back(Deeb_Key(Polycode::KEY_p));
		cmdInfo.push_back("P - toggle visible colliders");
		deebKeys.push_back(Deeb_Key(Polycode::KEY_c));
		cmdInfo.push_back("C - toggle Camera zoom");
		deebKeys.push_back(Deeb_Key(Polycode::KEY_n));
		cmdInfo.push_back("N - toggle navmesh");

		cameraZoomedOut = false;
		navMeshOn;
	}

	DebugTools()
	{

	}

	void update()
	{
		for (int i = 0; i < deebKeys.size(); i++)
		{
			bool thisState = core->getInput()->getKeyState((Polycode::PolyKEY)deebKeys.at(i).keyVal);
			if (thisState && !deebKeys.at(i).lastState)
			{
				deebKeys.at(i).wasPressed = true;
			}
			else
			{
				deebKeys.at(i).wasPressed = false;
			}

			deebKeys.at(i).lastState = thisState;
		}

		static bool yes = true;
		if (deebKeys.at(0).wasPressed) { toggleColliders(); }
		if (deebKeys.at(1).wasPressed) { toggleCameraZoom(); }
		if (deebKeys.at(2).wasPressed) { toggleNavMeshLines(); }
		yes = false;
	}

	void toggleColliders()
	{
		Level* l = gameLogicHub->currentLevel;
		for (int i = 0; i < l->levelSites.size(); i++)
		{
			LevelSite* site = l->levelSites.at(i);
			for (int j = 0; j < site->colliderCount; j++)
			{
				site->colliders[j]->visible = !site->colliders[j]->visible;
			}
		}
	};

	void toggleCameraZoom()
	{
		cameraZoomedOut = !cameraZoomedOut; 

		if (cameraZoomedOut)
		{
			float newSizeX = gameCam->camera->getOrthoSizeX() * 3.0f;
			float newSizeY = gameCam->camera->getOrthoSizeY() * 3.0f;
			gameCam->camera->setOrthoSize(newSizeX, newSizeY);
		}
		else
		{
			float newSizeX = gameCam->camera->getOrthoSizeX() / 3.0f;
			float newSizeY = gameCam->camera->getOrthoSizeY() / 3.0f;
			gameCam->camera->setOrthoSize(newSizeX, newSizeY);
		}
	};

	void toggleNavMeshLines()
	{
		std::vector<SceneLine*>* navmeshDebugLines = &gameLogicHub->currentLevel->navMesh.debugLines;

		if (navMeshOn)
			for (int i = 0; i < navmeshDebugLines->size(); i++)
				gameLogicHub->currentLevel->scene->addEntity(navmeshDebugLines->at(i));
		else
			for (int i = 0; i < navmeshDebugLines->size(); i++)
				gameLogicHub->currentLevel->scene->removeEntity(navmeshDebugLines->at(i));

		navMeshOn = !navMeshOn;
	}
};

#endif