#include "Actions.h"

Action::Action()
{
	doneFlag = false;
}

Action::~Action()
{

}

PathTo::PathTo(NPC* npc, NavMesh* navMesh, float nextWaypointDist)
{
	this->npc = npc;
	this->navMesh = navMesh;
	this->changeWayPointDist = nextWaypointDist;
}

float PathTo::describeChangeInWorldState(WorldState* outState, WorldState* targetState)
{
	float costOfExecution = targetState->getDiff(*outState);
	outState->position = targetState->position;
	return costOfExecution;
}

void PathTo::run(WorldState* current, WorldState* target)
{
	Vector3 npcLocation = npc->collisionCollider->getEntity()->getPosition();
	Vector3 targetLocation = Vector3(target->position.x, 0.0f, target->position.y);

	float dist = (targetLocation - npcLocation).length();
	if (dist < changeWayPointDist) doneFlag = true;

	createPath(npcLocation, targetLocation);
	if (path.size() == 0)
	{
		npc->setCurrentTargetPoint(targetLocation);
		return;
	}

	Vector3 nextWaypoint = path.front();
	float distToNext = (nextWaypoint - npcLocation).length();
	if (distToNext < changeWayPointDist)
		path.erase(path.begin());

	if (path.size() != 0)
		nextWaypoint = path.front();
	else
		nextWaypoint = targetLocation;

	npc->setCurrentTargetPoint(nextWaypoint);
}

void PathTo::createPath(Vector3 npcPos, Vector3 targetPOS)
{
	std::vector<Vector3> newPath;
	navMesh->createPath(&newPath, npcPos, targetPOS);

	if ((path.size() != 0 && newPath.back() != path.back()) || (path.size() == 0))
	{
		path.clear();
		for (int i = 0; i < newPath.size(); i++)
		{
			path.push_back(newPath.at(i));
		}

		Vector3 vecToTarget = targetPOS - npcPos;
		vecToTarget.Normalize();
		Vector3 vecToFirstNode = path.front() - npcPos;
		vecToFirstNode.Normalize();

		//If the beginning of the path makes the enemy move away from the target, then just erase the first
		float angle = vecToTarget.angleBetween(vecToFirstNode);
		if (angle > (3.1415926 / 2.0)) path.erase(path.begin());

		//If the last point makes the npc move past the target and then turn around then remove it
		if (path.size() >= 2)
		{
			Vector3 vecToLast = (path.at(path.size() - 1) - path.at(path.size() - 2));
			Vector3 vecFromLastToTarget = targetPOS - path.at(path.size() - 1);
			vecToLast.Normalize();
			vecFromLastToTarget.Normalize();
			if (std::abs(vecToLast.angleBetween(vecFromLastToTarget)) < 45.0f)
				path.pop_back();
		}
	}
}
