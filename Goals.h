#include <Polycode.h>
#ifndef AI_GOALS_H
#define AI_GOALS_H
#include "WorldState.h"
#include "NPC.h"
#include "Player.h"

using namespace Polycode;

class Goal
{
public:
	Goal(NPC* npc, float priority);
	~Goal();

	float priority;
	virtual WorldState getTargetState() = 0;
protected:
	NPC* npc;
};

class BeNextTo : public Goal
{
public:
	BeNextTo(NPC* npc, Player* player, float priority);
	~BeNextTo();

	virtual WorldState getTargetState();
private:
	Player* player;
};

#endif