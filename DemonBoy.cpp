#include "DemonBoy.h"

DemonBoy::DemonBoy(Player* player, PhysicsScene* scene, std::vector<NPC*>* npcList, NavMesh* navMesh) : 
NPC(player, scene, npcList)
{
	type = NPCTYPES::DemonBae;

	moveInfo = MovementInfo(0.16f, 16, 16);
	facingInfo = FacingInfo(25.0f, 8);
	facing = Facing();

	SceneMesh* demonBoyBody = new SceneMesh("Data/NPCs/Demonboy/Body.mesh");
	SceneMesh* demonBoySkullFace = new SceneMesh("Data/NPCs/Demonboy/SkullFace.mesh");
	SceneMesh* demonBoyArms = new SceneMesh("Data/NPCs/Demonboy/Arms.mesh");
	SceneMesh* demonBoyEars = new SceneMesh("Data/NPCs/Demonboy/Ears.mesh");
	SceneMesh* demonBoyHorns = new SceneMesh("Data/NPCs/Demonboy/Horns.mesh");
	SceneMesh* demonBoyLegs = new SceneMesh("Data/NPCs/Demonboy/Legs.mesh");
	SceneMesh* demonBoyHead = new SceneMesh("Data/NPCs/Demonboy/Head.mesh");

	demonBoyBody->colorAffectsChildren = false;
	demonBoyBody->setColor(ColorTable::gray_charcoal);
	demonBoyHead->colorAffectsChildren = false;
	demonBoyHead->setColor(ColorTable::gray_charcoal);
	demonBoyHead->addChild(demonBoyEars);
	demonBoyHead->addChild(demonBoySkullFace);
	demonBoySkullFace->setColor(ColorTable::yellow_gray);
	demonBoyHead->addChild(demonBoyHorns);
	demonBoyHorns->setColor(ColorTable::yellow_gray);
	demonBoyEars->setColor(ColorTable::gray_charcoal);
	demonBoyBody->addChild(demonBoyArms);
	demonBoyBody->addChild(demonBoyHead);
	demonBoyArms->setColor(ColorTable::gray_charcoal);
	demonBoyBody->addChild(demonBoyLegs);
	demonBoyLegs->setColor(ColorTable::gray_charcoal);

	demonBoyBody->setScale(Vector3(0.25));
	demonBoyBody->setYaw(210.0f);

	registerMeshWithShader(demonBoyBody);
	registerMeshWithShader(demonBoyArms);
	registerMeshWithShader(demonBoyEars);
	registerMeshWithShader(demonBoyHorns);
	registerMeshWithShader(demonBoyLegs);
	registerMeshWithShader(demonBoySkullFace);

	ScenePrimitive* colliderShape = new ScenePrimitive(ScenePrimitive::TYPE_CYLINDER, 1.5, 0.4, 8.0);
	colliderShape->setColor(1.0, 1.0, 1.0, 0.33);
	colliderShape->visible = false;
	collisionCollider = scene->addCollisionChild(colliderShape, CollisionEntity::SHAPE_CYLINDER);
	ScenePrimitive* effectColliderShape = new ScenePrimitive(ScenePrimitive::TYPE_ICOSPHERE, 0.33, 1.0f);
	effectColliderShape->setColor(1.0, 1.0, 1.0, 0.33);
	effectColliderShape->visible = false;
	effectCollider = scene->addCollisionChild(effectColliderShape, CollisionEntity::SHAPE_SPHERE);

	body = demonBoyBody;
	addToScene(scene, Vector3(0.0, 1.0, 10.5));

	std::vector<Goal*> goals;
	std::vector<Action*> actions;

	goals.push_back(new BeNextTo(this, player, 50.0f));
	actions.push_back(new PathTo(this, navMesh, 0.16f * 15.0f));

	brainCore = new NPC_Brain(this, player, goals, actions);
}

DemonBoy::~DemonBoy()
{

}

void DemonBoy::addToScene(Scene* scene, Vector3 startPosition)
{
	scene->addEntity(body);
	body->setPosition(startPosition);
	collisionCollider->getEntity()->setPosition(startPosition);
	effectCollider->getEntity()->setPosition(startPosition);
}

void DemonBoy::removeFromScene(Scene* scene)
{
	scene->removeEntity(body);
}

void DemonBoy::reactToHit(float damage)
{
	curseLevel += damage;
}

void DemonBoy::reactToWarm(float ember)
{
	emberLevel += ember;
}

void DemonBoy::move()
{
	steer();
	collisionCollider->getEntity()->setPositionY(0.75f);
	effectCollider->getEntity()->setPosition(collisionCollider->getEntity()->getPosition());
	effectCollider->getEntity()->setPositionY(1.25f);
}

void DemonBoy::animate()
{
	body->setYaw(facing.getYaw());
	body->setPosition(collisionCollider->getEntity()->getPosition());
	body->setPositionY(1.5f);

	updateShaderLocals(body);
}
