Artifact -> stationary (maybe rarely moving?) object that offers a resource exchange
2 varieties: context-free & context sensitive (w/r/t placement in world map)

2 services: Navigational & Resource Exchange
    Navigational
	Teleporter (placed in pairs, with decently long shortest path between them)
	Pointer
	Divination shrine (locate exact node of certain Artifact or NPC)
		Either closest or within small radius
	Compass alignment (set what your compass-torch homes towards)
    Xchange
	Curse reducer (no neighbor)
	Item shrine   (context: no neighbor of same type)
	    Ember price
	    Take-one-leave-one
	Spear upgrade
	Extra ember
	    Not an xchange, but will provide some level of comfort to player, rare, needs level w/ no NPCs
    Both:
	Locked door - place btwn touching 3 neighbored levels
	Compass upgrade?

Consumables:
    Extra ember (rare, dumb, but provides comfort?)
    Door key
    Curse sheild (curse does not increase with each hit, ember lost reduced)
    Curse reducer (less than curse cure)
    Passive inducer (set NPC state to passive) (one for friendly, aggresive, and retreating)
	(breaks if you interact)
    Teleporter (use 1, place, use 2 return, erased thereafter)
    Save salts
    Potion - temporarily lowers curse value. (its drugs)
    Salve - permanently heals minor curese amount

Compass mechanic:
	When it's out and pointed, changes intensity when it's pointed towards the next closest door to the thinking you're tracking down


Also:
    -Artifact count = X% of levels
    -Common NPC thing: have static mesh as head/face piece (like a mask, or helm)
    -Artifacts super geometric? (most everything else is organic)
    -Campfire: call for aid? (Assumes all NPCs may provide assistance)
    