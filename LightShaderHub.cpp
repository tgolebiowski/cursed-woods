#include "LightShaderHub.h"

LightShaderHub* LightShaderHub::shaderHubInstance = NULL;

LightShaderHub::LightShaderHub(Vector2 screenSize)
{
	lightDirection = Vector3(1.0, 0.66, -0.66);
	lightDirection.Normalize();
	treetopsChannelSampleCycle = 0.0f;

	sceneryShader = (Material*)CoreServices::getInstance()->getResourceManager()
		->getGlobalPool()->getResource(Resource::RESOURCE_MATERIAL, "SceneryShader");
	sceneryShader->getShaderBinding(0)->getLocalParamByName("GlobalLightAngle")
		->setVector3(-lightDirection);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("screenCenterX")->setNumber(screenSize.x/2.0);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("screenCenterY")->setNumber(screenSize.y/2.0);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("lightColor")->setColor(ColorTable::emberYellow);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("shadowColor")->setColor(ColorTable::curseShadow);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("fogColor")->setColor(ColorTable::turquiose1);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("depthMin")->setNumber(0.015f);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("depthMax")->setNumber(0.047f);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("worldSize")->setNumber(100.0f);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("treetopsChannelSample")
		->setNumber(treetopsChannelSampleCycle);
	
	Image* canopyShadowImage = new Image(512, 512);
	Perlin treeTopsPerlin = Perlin(4, 60.0f, 1.0f, 55);
	const float fullLightCutoff = 0.7f;
	const float fullShadowCutoff = 0.5f;
	for (int i = 0; i < 512; i++)
	{
		for (int j = 0; j < 512; j++)
		{
			float perlinValueR = treeTopsPerlin.Get3D(((float)i) / 512.0f, ((float)j) / 512.0f, 0.0f);
			perlinValueR += 0.5f;
			if (perlinValueR > fullLightCutoff) perlinValueR = 1.0f;
			else if (perlinValueR < fullShadowCutoff) perlinValueR = 0.0f;
			else perlinValueR = (perlinValueR - fullShadowCutoff) / (fullLightCutoff - fullShadowCutoff);

			float perlinValueG = treeTopsPerlin.Get3D(((float)i) / 512.0f, ((float)j) / 512.0f, 0.003f);
			perlinValueG += 0.5f;
			if (perlinValueG > fullLightCutoff) perlinValueG = 1.0f;
			else if (perlinValueG < fullShadowCutoff) perlinValueG = 0.0f;
			else perlinValueG = (perlinValueG - fullShadowCutoff) / (fullLightCutoff - fullShadowCutoff);

			canopyShadowImage->setPixel(i, j, Color(perlinValueR, perlinValueG, 0.0f, 1.0f));
		}
	}
	canopyShadowMap = new SceneImage(canopyShadowImage);
	sceneryShader->getShaderBinding(0)->addTexture("canopyShadowMap", canopyShadowMap->getTexture());

	staticShadowMap = NULL;


	LightShaderHub::shaderHubInstance = this;
	/*occlusionRenderScene = new Scene(Scene::SCENE_2D);
	occlusionRenderScene->getDefaultCamera()->setOrthoMode(true);
	occlusionRenderScene->getDefaultCamera()->setOrthoSize(1024, 1024);
*/
}

LightShaderHub::~LightShaderHub()
{

}

void LightShaderHub::setStaticShadowMap(Image* image)
{
	Image newStaticShadowMap = Image(image);
	Vector2 sampleDirection = Vector2(-lightDirection.x, -lightDirection.z);
	int sampleLength = lightDirection.y * 90.0;
	for (int i = 0; i < newStaticShadowMap.getHeight(); i++)
	{
		for (int j = 0; j < newStaticShadowMap.getWidth(); j++)
		{
			Color current = image->getPixel(j, i);

			float sampleTotal = 0.0f;
			float factorTotal = 0.0f;
			for (int k = 0; k < sampleLength; k++)
			{
				int sampleX = j + k * sampleDirection.x;
				int sampleY = i + k * sampleDirection.y;
				float factor = (1.0 + (1.0 - ((float)k / sampleLength)));

				if (sampleX >= 0 && sampleX < image->getWidth() && 
					sampleY >= 0 && sampleY < image->getHeight())
				{
					sampleTotal += image->getPixel(sampleX, sampleY).r * factor;
				}
				else
				{
					sampleTotal += 1.0f * factor;
				}

				factorTotal += factor;
			}

			float sample = sampleTotal / factorTotal;
			current.r *= sample;
			current.g *= sample;
			current.b *= sample;
			newStaticShadowMap.setPixel(j, i, current);
		}
	}

	if (staticShadowMap != NULL) { delete staticShadowMap; }
	staticShadowMap = new SceneImage(&newStaticShadowMap);

	static bool quickSkip = true;
	if (quickSkip)
	{
		quickSkip = false;
		sceneryShader->getShaderBinding(0)->addTexture("staticShadowMap", staticShadowMap->getTexture());
	}
	else 
	{ 
		sceneryShader->getShaderBinding(0)->getTexture("staticShadowMap")
			->setTextureData(staticShadowMap->getTexture()->getTextureData());
	}
}

void LightShaderHub::update(Vector3 currentFocus)
{
	//2PI / (30 FPS * # of Seconds)
	float const sampleUpdateValue = (2.0 * 3.1415926) / (30.0 * 2.0);

	treetopsChannelSampleCycle += sampleUpdateValue;
	if (treetopsChannelSampleCycle > (3.1415926 * 2.0)) treetopsChannelSampleCycle -= (2.0 * 3.1415926);
	sceneryShader->getShaderBinding(0)->getLocalParamByName("treetopsChannelSample")
		->setNumber((std::sinf(treetopsChannelSampleCycle) + 1.0)/2.0);
}

void LightShaderHub::createStaticOcclusionMap(LevelLayoutStruct* levelInfo, LevelStateInfo* stateInfo, float planeSize)
{
	const int lightMapSize = 1024;
	Image* lightMap = new Image(lightMapSize, lightMapSize);
	lightMap->fill(Color(1.0, 1.0, 1.0, 1.0));

	auto paintCircle = [lightMapSize, lightMap, planeSize](float x, float z, float radius)
	{
		float pixelCenterX = x / planeSize;
		pixelCenterX += 0.5f;
		pixelCenterX *= lightMapSize;
		float pixelCenterY = z / planeSize;
		pixelCenterY += 0.5;
		pixelCenterY *= lightMapSize;

		const float radiusCONST = 1.3f;
		const float cutoffCONST = 1.25f;
		const float grayMinCONST = 0.9f;

		int startPixelX = (((x - radius * radiusCONST) + (planeSize / 2.0)) / planeSize) * lightMapSize;
		int endPixelX = startPixelX + (((radius * radiusCONST * 2) / planeSize) * lightMapSize);
		int startPixelY = (((z - radius * radiusCONST) + (planeSize / 2.0)) / planeSize) * lightMapSize;
		int endPixelY = startPixelY + (((radius * radiusCONST * 2) / planeSize) * lightMapSize);

		startPixelX = std::fmaxf(std::fminf(lightMapSize, startPixelX), 0.0);
		endPixelX = std::fmaxf(std::fminf(lightMapSize, endPixelX), 0.0);
		startPixelY = std::fmaxf(std::fminf(lightMapSize, startPixelY), 0.0);
		endPixelY = std::fmaxf(std::fminf(lightMapSize, endPixelY), 0.0);

		for (int i = startPixelX; i < endPixelX; i++)
		{
			for (int j = startPixelY; j < endPixelY; j++)
			{
				float iDist = i - pixelCenterX;
				float jDist = j - pixelCenterY;
				float dist = sqrt(iDist * iDist + jDist * jDist);
				int distCutoff = ((radius * cutoffCONST / planeSize) * lightMapSize);
				int grayMin = ((radius * grayMinCONST / planeSize) * lightMapSize);

				if (dist < distCutoff)
				{
					if (dist < grayMin)
					{
						lightMap->setPixel(i, j, Color(0.0, 0.0, 0.0, 1.0));
					}
					else
					{
						float grayNess = ((float)dist - (float)grayMin) /
							((float)distCutoff - (float)grayMin);

						float current = lightMap->getPixel(i, j).r;
						if (current > grayNess)
						{
							lightMap->setPixel(i, j, Color(grayNess, grayNess, grayNess, 1.0f));
						}
					}
				}
			}
		}
	};

	auto paintRect = [lightMapSize, lightMap, planeSize, paintCircle]
		(float x, float z, float length, float depth, float angle)
	{
		float rads = angle * (3.1415926 / 180.0);
		Vector2 parallel = Vector2(std::cosf(rads), std::sinf(rads));
		//Vector2 normal = Vector2(-parallel.y, parallel.x);

		int pasteCount = (length / planeSize) * lightMapSize * 0.5;

		for (int i = 0; i < pasteCount; i++)
		{
			paintCircle(x + (parallel.x * length * ((float)i / (float)pasteCount) * 0.5),
				z + (parallel.y * length * ((float)i / (float)pasteCount) * 0.5), depth);
			paintCircle(x + (parallel.x * length * ((float)i / (float)pasteCount) * -0.5),
				z + (parallel.y * length * ((float)i / (float)pasteCount) * -0.5), depth);
		}
	};

	for (int i = 0; i < levelInfo->siteCount; i++)
	{
		LevelLayoutStruct::SiteInfo site = levelInfo->siteInfo[i];
		for (int j = 0; j < site.colliderCount; j++)
		{
			LevelLayoutStruct::ColliderObjInfo colliderInfo = site.collidersInSite[j];
			if (colliderInfo.colliderType == ScenePrimitive::TYPE_CYLINDER)
			{
				paintCircle(colliderInfo.positionX, -colliderInfo.positionZ, colliderInfo.value2);
			}
			else if (colliderInfo.colliderType == ScenePrimitive::TYPE_BOX)
			{
				paintRect(colliderInfo.positionX, -colliderInfo.positionZ, 
					colliderInfo.value1, colliderInfo.value3, colliderInfo.angle);
			}
		}
	}
	
	if (stateInfo->artifactType != LevelStateInfo::ArtifactTypes::None &&
		stateInfo->artifactType != LevelStateInfo::ArtifactTypes::Torch &&
		stateInfo->artifactType != LevelStateInfo::ArtifactTypes::Start)
	{
		Artifact* artifact = NULL;
		Vector2 artifactPosition = Vector2(stateInfo->artifactPositionX, stateInfo->artifactPositionZ);
		if (stateInfo->artifactType == LevelStateInfo::ArtifactTypes::Item)
			artifact = new ItemPickup(artifactPosition, NULL);
		else if (stateInfo->artifactType == LevelStateInfo::ArtifactTypes::CurseCure)
			artifact = new CurseCurer(artifactPosition, NULL);
		else if (stateInfo->artifactType == LevelStateInfo::ArtifactTypes::Divination)
			artifact = new DivinationShrine(artifactPosition, NULL);
		else if (stateInfo->artifactType == LevelStateInfo::ArtifactTypes::Aura)
			artifact = new AuraShrine(artifactPosition, NULL);
		else if (stateInfo->artifactType == LevelStateInfo::ArtifactTypes::Pointer)
			artifact = new Pointer(artifactPosition, NULL);

		for (int j = 0; j < artifact->colliders.size(); j++)
		{
			if (artifact->colliders.at(j)->getPrimitiveType() == ScenePrimitive::TYPE_CYLINDER)
			{
				paintCircle(
					artifact->colliders.at(j)->getPosition().x,
					-artifact->colliders.at(j)->getPosition().z,
					artifact->colliders.at(j)->getPrimitiveParameter2()
					);
			}
			else if (artifact->colliders.at(j)->getPrimitiveType() == ScenePrimitive::TYPE_BOX)
			{
				paintRect(
					artifact->colliders.at(j)->getPosition().x,
					-artifact->colliders.at(j)->getPosition().z,
					artifact->colliders.at(j)->getPrimitiveParameter1(),
					artifact->colliders.at(j)->getPrimitiveParameter3(),
					artifact->colliders.at(j)->getYaw()
					);
			}
		}

		delete artifact;
	}

	const String lightMapSaveFolder = "Data/WorldSave/OcclusionMaps/";
	String saveString = lightMapSaveFolder + String::NumberToString(levelInfo->identifier, 0) + ".png";
	lightMap->savePNG(saveString);
}