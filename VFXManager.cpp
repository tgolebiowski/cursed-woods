#include "VFXManager.h"

VFXManager* VEffect::vfxManager = NULL;
Scene* VEffect::scene = NULL;

//VEffects
VEffect::VEffect(int length)
{
	this->scene = scene;
	this->timeLeft = length;
	killFlag = false;

	vfxManager->addEffect(this);
}

VEffect::~VEffect()
{

}

void VEffect::update()
{
	timeLeft--;
	if(timeLeft <= 0) killFlag = true;
}

//Flame Effect
FlameEffect::FlameEffect(Vector3 center) : VEffect(3)
{
	this->center = center;
	this->delayNewFlaneMesh = 0;

	flameCurve = BezierCurve();
	flameCurve.addControlPoint2dWithHandles(0, 0, 0, 0, 0.75, 0.5);
	flameCurve.addControlPoint2dWithHandles(0, 0.33f, 0, 1, 0, 0);

	burningToggle = true;
}

FlameEffect::~FlameEffect()
{
	for(int i = 0; i < flameList.size(); i++)
	{
		scene->removeEntity(flameList.at(i)->mesh);
		delete flameList.at(i)->mesh;
		delete flameList.at(i);
	}
	flameList.clear();
}

void FlameEffect::update()
{
	if(!burningToggle) return;

	timeLeft = 3;
	if(delayNewFlaneMesh > 0) { delayNewFlaneMesh--; }
	else
	{
		FlameMesh* newFlame = new FlameMesh(rand() % 360, (((float)(rand() % 100))/100.0f) * 1.2f);
		newFlame->mesh->setPosition(center);
		newFlame->mesh->setScale(0.0,0.0,0.0);
		flameList.push_back(newFlame);
		scene->addEntity(newFlame->mesh);

		delayNewFlaneMesh = (rand() % 2) + 1;
	}

	for(int i = 0; i < flameList.size(); i++)
	{
		FlameMesh* flame = flameList.at(i);
		flame->currentT += 1.0f / 60.0f;

		if(flame->currentT > 1.0f)
		{
			scene->removeEntity(flame->mesh);
			delete flame->mesh;
			delete flame;
			flameList.erase(flameList.begin() + i);
			i--;
		}
		else
		{
			Vector3 bezPoint = flameCurve.getPointAt(flame->currentT);
			float scaling = bezPoint.x * 1.4;
	
			Quaternion rotationQuat;
			rotationQuat.createFromAxisAngle(0,1,0, flame->angle);
			Vector3 rotatedPosition = rotationQuat.applyTo(Vector3(bezPoint.x * flame->distance, 0.0f, 0.0f));

			flame->mesh->setPosition(center + rotatedPosition + Vector3(0, bezPoint.y * 2.3f, 0.0f));
			flame->mesh->setScale(scaling, scaling, scaling);
		}

	}

	VEffect::update();
}

void FlameEffect::toggle()
{
	burningToggle = !burningToggle;

	if(!burningToggle)
	{
		for(int i = 0; i < flameList.size(); i++)
		{
			scene->removeEntity(flameList.at(i)->mesh);
		}
	}
	else
	{
		for(int i = 0; i < flameList.size(); i++)
		{
			scene->addEntity(flameList.at(i)->mesh);
		}
	}
}

//Warming Effect
WarmingEffect::WarmingEffect(Vector3 center, float strength) : VEffect(15)
{
	this->center = center;

	curve = BezierCurve();
	curve.addControlPoint2dWithHandles(0,0,0,0,1,0);
	curve.addControlPoint2dWithHandles(1,0,1,1,0,0);

	int count = 3;
	if(strength > 0.2f) count = 5;

	for(int i = 0 ; i < count; i++)
	{
		Quaternion rotQuat;
		rotQuat.createFromAxisAngle(0,1,0, rand() % 360);
		float heightOffset = (((float)(rand() % 100)) / 100.0f) * 0.4f;
		float distance = (((float)(rand() % 100))/100.0f) * 0.65f;
		heightOffsets.push_back(heightOffset);

		Vector3 p = center + rotQuat.applyTo(Vector3(distance, heightOffset, 0));
		Vector3 p2 = p + Vector3(0,0.5f, 0.0f);

		SceneLine* line = new SceneLine(p, p2);
		line->setColor(247.0/255.0, 206.0/255.0, 68.0/255.0, 1.0f);
		scene->addEntity(line);
		lines.push_back(line);
	}
}

WarmingEffect::~WarmingEffect()
{
	for(int i = 0; i < lines.size(); i++)
	{
		scene->removeEntity(lines[i]);
		delete lines[i];
	}
}

void WarmingEffect::update()
{
	VEffect::update();

	float t = (1.0f - ((float)timeLeft / 15.0f));
	float newY = curve.getPointAt(t).y;

	for(int i = 0; i < 3; i++)
	{
		lines[i]->setPositionY(center.y + (newY * 3.0f) + heightOffsets[i]);
		lines[i]->setScaleY(1.0f - newY);
	}
}

//Curse-Add effect
CurseAddEffect::CurseAddEffect(Vector3 start) : VEffect(30)
{
	this->start = start;

	riseCurve = BezierCurve();
	riseCurve.addControlPoint2dWithHandles(0.5, 0.33, 0, 0, 0.5, 0.33);
	riseCurve.addControlPoint2dWithHandles(0.5, 0.5, 0.5, 1.0, 0.5, 0.5);

	//first bubble
	ScenePrimitive* bubble = new ScenePrimitive(ScenePrimitive::TYPE_ICOSPHERE, 0.2, 1);
	bubble->setPosition(start);
	bubble->setColor(curseColor);
	scene->addEntity(bubble);

	CurseBubbleStatus stats = CurseBubbleStatus();
	stats.angle = ((float)(rand() % 100) / 100.0) * (2.0 * 3.1415926);
	stats.tick = 0;
	stats.attachedCloud = bubble;

	info.push_back(stats);
}

CurseAddEffect::~CurseAddEffect(void)
{
	for (int i = 0; i < info.size(); i++)
	{
		scene->removeEntity(info.at(i).attachedCloud);
	}
	info.clear();
}

void CurseAddEffect::update()
{
	VEffect::update();

	if (this->timeLeft % 3 == 0 && this->timeLeft > 20)
	{
		ScenePrimitive* bubble = new ScenePrimitive(ScenePrimitive::TYPE_ICOSPHERE, 0.2, 1);
		bubble->setPosition(start);
		bubble->setColor(curseColor);
		scene->addEntity(bubble);

		CurseBubbleStatus stats = CurseBubbleStatus();
		stats.angle = ((float)(rand() % 100) / 100.0) * (2.0 * 3.1415926);
		stats.tick = 0;
		stats.attachedCloud = bubble;

		info.push_back(stats);
	}

	for (int i = 0; i < info.size(); i++)
	{
		info.at(i).tick++;

		if (info.at(i).tick > 20)
		{
			scene->removeEntity(info.at(i).attachedCloud);
			info.erase(info.begin() + i);
			i--;
		}
		else
		{
			float t = ((float)info.at(i).tick) / 20.0;
			float offset = riseCurve.getPointAt(t).x;
			float height = t * 0.75;

			Vector3 relativeP = Vector3(std::cos(info.at(i).angle) * offset, height, std::sin(info.at(i).angle) * offset);
			info.at(i).attachedCloud->setPosition(start + relativeP);
		}
	}
}

//point laser effect
PointLaserEffect::PointLaserEffect(Vector3 start, Vector3 direction) : VEffect(15)
{
	const float length = 30.0f;
	direction.Normalize();
	direction.setLength(length);

	laser = new SceneLine(start, start + direction);
	scene->addEntity(laser);
}

PointLaserEffect::~PointLaserEffect()
{
	scene->removeEntity(laser);
	delete laser;
}

void PointLaserEffect::update()
{
	const float maxLineSize = 11.0f;
	float t = ((float)timeLeft) / 15.0f;
	laser->setLineWidth(1.0f + maxLineSize * t);

	Color c = laser->getCombinedColor();
	laser->setColor(c.r, c.g, c.b, t * t);

	VEffect::update();
}

//VFXManager stuff
VFXManager::VFXManager(Scene* scene)
{
	this->scene = scene;

	VEffect::setStatics(this, scene);
}

VFXManager::~VFXManager(void)
{
	for(int i = 0; i < effects.size(); i++)
	{
		delete effects.at(i);
	}
}

void VFXManager::deleteEffect(VEffect* effect)
{
	for (int i = 0; i < effects.size(); i++)
	{
		if (effects.at(i) == effect)
		{
			delete effects.at(i);
			effects.erase(effects.begin() + i);
			break;
		}
	}
}

void VFXManager::addEffect(VEffect* effect)
{
	effects.push_back(effect);
}

void VFXManager::update()
{
	for(int i = 0; i < effects.size(); i++)
	{
		effects.at(i)->update();

		if(effects.at(i)->killFlag == true)
		{
			delete effects.at(i);
			effects.erase(effects.begin() + i);
			i--;
		}
	}
}