#include "NPC.h"


NPC::NPC(Player* player, PhysicsScene* scene, std::vector<NPC*>* npcList)
{
	this->player = player;
	this->scene = scene;
	this->npcList = npcList;
}

NPC::~NPC(void)
{
	delete collisionCollider;
	delete effectCollider;
	delete brainCore;
}

void NPC::update()
{
	brainCore->run();
	move();
	animate();
}

void NPC::steer()
{
	Vector3 movedir = currentTargetPoint - collisionCollider->getEntity()->getPosition();
	movedir.y = 0.0f;
	movedir.Normalize();

	float distanceToTarget = (currentTargetPoint - collisionCollider->getEntity()->getPosition()).length();
	float cutoffDist = moveInfo.calculatedSpeed * 10;

	if (distanceToTarget < cutoffDist) movedir = Vector3(0);

	moveInfo.update(Vector2(movedir.x, movedir.z));

	Vector3 velocity = moveInfo.calculatedDirection;
	velocity.setLength(moveInfo.calculatedSpeed);

	if (CoreServices::getInstance()->getCore()->getInput()->getKeyState(Polycode::KEY_o))
	{
		int kjdsdsd = 555;
	}

	facingInfo.update(Vector2(movedir.x, -movedir.z), Vector2(facing.x, facing.z));
	facing.rotate(facingInfo.calculatedTurnSpeed);
		
	collisionCollider->getEntity()->setPosition(collisionCollider->getEntity()->getPosition() + velocity);
}

void NPC::registerMeshWithShader(SceneMesh* mesh)
{
	mesh->setMaterialByName("SceneryShader");
	mesh->getLocalShaderOptions()->addParam(ProgramParam::PARAM_MATRIX, "transformMatrix")
		->setMatrix4(mesh->getConcatenatedMatrix());
	Quaternion q = mesh->getConcatenatedQuat();
	mesh->getLocalShaderOptions()->addParam(ProgramParam::PARAM_COLOR, "rotationQuat")
		->setColor(Color(q.w, q.x, q.y, q.z));
}

void NPC::updateShaderLocals(SceneMesh* mesh)
{
	mesh->getLocalShaderOptions()->getLocalParamByName("transformMatrix")
		->setMatrix4(mesh->getConcatenatedMatrix());
	Quaternion q = mesh->getConcatenatedQuat();
	mesh->getLocalShaderOptions()->getLocalParamByName("rotationQuat")->setColor(Color(q.w, q.x, q.y, q.z));
}

float NPC::getCurseVal()
{
	float curveStartY = 0.0f;
	float curveHandle1X = 0.0f;
	float curveHandle1Y = 0.25f;

	float curveEndY = 1.0f;
	float curveHandle2X = 1.0f - 0.33f;
	float curveHandle2Y = 1.0f;

	BezierCurve curseCoeffCurve = BezierCurve();
	curseCoeffCurve.addControlPoint2dWithHandles(0.0f, curveStartY, 0.0f, curveStartY, curveHandle1X, curveHandle1Y);
	curseCoeffCurve.addControlPoint2dWithHandles(curveHandle2X, curveHandle2Y, 1.0f, curveEndY, 1.0f, curveEndY);

	float normalizedCurseValue = curseLevel / 1000.0f;
	return curseCoeffCurve.getYValueAtX(normalizedCurseValue);
}

float NPC::getEmberVal()
{
	float curveStartY = 0.0f;
	float curveHandle1X = 0.0f;
	float curveHandle1Y = 0.25f;

	float curveEndY = 1.0f;
	float curveHandle2X = 1.0f - 0.33f;
	float curveHandle2Y = 1.0f;

	BezierCurve curseCoeffCurve = BezierCurve();
	curseCoeffCurve.addControlPoint2dWithHandles(0.0f, curveStartY, 0.0f, curveStartY, curveHandle1X, curveHandle1Y);
	curseCoeffCurve.addControlPoint2dWithHandles(curveHandle2X, curveHandle2Y, 1.0f, curveEndY, 1.0f, curveEndY);

	float normalizedCurseValue = emberLevel / 1000.0f;
	return curseCoeffCurve.getYValueAtX(normalizedCurseValue);
}

bool NPC::isBeingThreatened()
{
	if (player->playerState->currentEquip != PlayerState::Equipment::Spear) return false;

	float spearExtension = player->playerState->spearOffset.length();
	Vector3 spearDirection = Vector3(player->playerState->spearOffset);
	spearDirection.y = 0.0f;
	spearDirection.Normalize();
	spearDirection.setLength(spearExtension);

	Vector3 directionToThisEnemy = collisionCollider->getEntity()->getPosition() - player->playerState->position;
	directionToThisEnemy.y = 0.0f;
	directionToThisEnemy.Normalize();

	float angleBetween = spearDirection.angleBetween(directionToThisEnemy);
	angleBetween = std::abs(angleBetween);

	if(angleBetween < (3.1415926f / 6.0f) && spearExtension > 0.25f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool NPC::isBeingAttacked()
{
	if (player->playerState->currentEquip != PlayerState::Equipment::Spear) return false;

	float length = player->playerState->spearOffset.length();
	Vector3 spearDirection = player->playerState->spearOffset;
	spearDirection.y = 0.0f;
	spearDirection.Normalize();
	spearDirection.setLength(length);

	Vector3 directionToThisEnemy = collisionCollider->getEntity()->getPosition() - player->playerState->position;
	directionToThisEnemy.y = 0.0f;
	directionToThisEnemy.Normalize();

	float angleBetween = spearDirection.angleBetween(directionToThisEnemy);
	angleBetween = std::abs(angleBetween);

	Vector3 spearSwingDirection = player->playerState->spearMovementThisFrame;
	spearSwingDirection.y = 0.0f;
	spearSwingDirection.Normalize();

	float dot = spearSwingDirection.dot(directionToThisEnemy);

	if(angleBetween < ( 3.1415926f / 4.0) && dot > 0) return true;

	return false;
}

bool NPC::isBeingWarmed()
{
	if (player->playerState->currentEquip != PlayerState::Equipment::Ember &&
		player->playerState->emberExtended) return false;

	CollisionResult warmResult = scene->testCollision(collisionCollider->getEntity(), 
		player->emberSphere->getEntity());
	return warmResult.collided;
}

bool NPC::isEmberBeingPointed()
{
	if (player->playerState->currentEquip != PlayerState::Equipment::Ember &&
		player->playerState->emberExtended) return false;

	Vector3 playerToNPC = player->playerState->position - collisionCollider->getEntity()->getPosition();
	Vector3 emberVec = player->playerState->position - player->emberSphere->getEntity()->getPosition();
	float angle = playerToNPC.angleBetween(emberVec);
	angle = std::abs(angle);
	if(angle < (3.1415926f / 6.0f)) return true;
	return false;
}
