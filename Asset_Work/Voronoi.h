#pragma once
#include <Polycode.h>
#include <unordered_map>

using namespace Polycode;

struct Triangle;
struct VoronoiNode;
struct Site;

struct Edge
{
	Vector2 point1;
	Vector2 point2;

	Triangle* tri1;
	Triangle* tri2;

	Edge(Vector2 point1, Vector2 point2)
	{
		this->point1 = point1;
		this->point2 = point2;
		tri1 = NULL;
		tri2 = NULL;
	};
};

struct Triangle
{
	Edge* edges[3];
};

struct DelaunayTriangle
{
	Vector2 p1;
	Vector2 p2;
	Vector2 p3;

	///index 0 = p1->p2, index 1 = p2->p3, index 2 = p3->p1
	DelaunayTriangle* neighbors[3];

	void* userData;

	Triangle* baseTri;

	VoronoiNode* node;

	Site* sites[3];

	DelaunayTriangle()
	{
		neighbors[0] = NULL;
		neighbors[1] = NULL;
		neighbors[2] = NULL;
		sites[0] = NULL;
		sites[1] = NULL;
		sites[2] = NULL;
		baseTri = NULL;
		node = NULL;
	};
};

struct Circle
{
	Vector2 center;
	float radius;

	Circle()
	{

	};

	Circle(Triangle* triangle)
	{
		Vector2 vert1 = triangle->edges[0]->point1;
		Vector2 vert2 = triangle->edges[0]->point2;
		Vector2 vert3;

		if(triangle->edges[1]->point1 != vert1 && triangle->edges[1]->point1 != vert2)
		{
			vert3 = triangle->edges[1]->point1;
		}
		else
		{
			vert3 = triangle->edges[1]->point2;
		}

		Vector2 p0 = vert1;
		Vector2 p1 = vert2;
		Vector2 p2 = vert3;

		Vector2 midPoint01 = (p0 + p1) / 2.0;
		Vector2 midPoint12 = (p1 + p2) / 2.0;

		float slope1 = (p0.y - p1.y) / (p0.x - p1.x);
		float slope2 = (p1.y - p2.y) / (p1.x - p2.x);

		float slope1Inverse = (-1.0f / slope1);
		float slope2Inverse = (-1.0f / slope2);

		float b1 = midPoint01.y - slope1Inverse * midPoint01.x;
		float b2 = midPoint12.y - slope2Inverse * midPoint12.x;

		float intersectX = (b2 - b1) / (slope1Inverse - slope2Inverse);
		float intersectY = slope1Inverse * intersectX + b1;

		center = Vector2(intersectX, intersectY);
		radius = (center - p0).length();
	};
};

struct VoronoiNode
{
	Vector2 location;
	VoronoiNode* neighbors[3];
	float radius;

	DelaunayTriangle* tri;

	VoronoiNode(Vector2 location)
	{
		this->location = location;
		neighbors[0] = NULL;
		neighbors[1] = NULL;
		neighbors[2] = NULL;

		tri = NULL;
	}
};

struct Site
{
	Vector2 position;
	std::vector<VoronoiNode*> verticies;
	std::vector<DelaunayTriangle*> touchingTris;

	void* userData;

	Site()
	{
		userData = NULL;
	};
};

class Voronoi
{
public:
	Voronoi(void);
	~Voronoi(void);

	bool generateDelaunayTriangulation();
	void generateVoronoiGraph();
	int pointInTri(DelaunayTriangle* tri, Vector2 point);
	void renderDelaunayMesh(Scene* scene, std::vector<SceneLine*>* lines, Vector2 offset = Vector2(0,0));
	void renderVoronoiDiagram(Scene* scene, std::vector<SceneLine*>* lines, Vector2 offset = Vector2(0,0));
	bool flip(Triangle* triangle1, Triangle* triangle2, Edge* edgeInCommon);
	DelaunayTriangle* getTriangleHousingPoint(Vector2 point, DelaunayTriangle* startTri = NULL);
	Site* getNearestNeighbor(Vector2 point, DelaunayTriangle* startTri = NULL);
	void floodFill(std::vector<Site*>* floodedSites, float cutOffDistance, Site* startSite);

	DelaunayTriangle* centerMostTriangle;

	std::vector<Vector2>* getPointList() { return &pointList; };
	std::vector<Triangle*>* getTriangleList() { return &triangleList; };
	std::vector<VoronoiNode*>* getVoronoiGraph() { return &voronoiGraph; };
	std::vector<DelaunayTriangle*>* getDelaunayMesh() { return &delaunayGraph; };
	std::vector<Site*>* getSiteList() { return &siteList; };

	std::unordered_map<std::string, Site*> siteMap;

	float avgX;
	float avgY;

private:

	Vector2 startPoint1;
	Vector2 startPoint2;
	Vector2 startPoint3;

	std::vector<VoronoiNode*> voronoiGraph;
	std::vector<Vector2> pointList;
	std::vector<Triangle*> triangleList;
	std::vector<DelaunayTriangle*> delaunayGraph;
	std::vector<Edge*> edgeList;
	std::vector<Site*> siteList;

	float minX;
	float minY;
	float maxX;
	float maxY;
	
	void insertPoint(Triangle* parentTri, Vector2 newPoint, Edge** newEdges, Edge** oldEdges);
	bool pointInTri(Triangle* triangle, Vector2 point);
};