#include "ColorTable.h"

Polycode::Color ColorTable::brown3 = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::purple_gray = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::brown2 = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::yellow_gray = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::darkGray_Blue = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::lightGray_littlePurple = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::lightCursePurple = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::skin = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::emberOrange = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::midCursePurple = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::emberYellow = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::curseShadow = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::turquiose1 = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::brownRed = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::blue1 = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::green1 = Polycode::Color(0, 0, 0, 0);
Polycode::Color ColorTable::gray_charcoal = Polycode::Color(0, 0, 0, 0);