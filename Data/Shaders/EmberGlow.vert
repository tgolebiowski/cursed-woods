#version 150

flat out vec4 vertexColor;

void main() {

	vec3 normal = gl_NormalMatrix * gl_Normal;
	gl_Position = ftransform();
	vec4 pos = gl_ModelViewMatrix * gl_Vertex;
	vec4 rawpos = gl_Vertex;
    vertexColor = gl_Color;
}