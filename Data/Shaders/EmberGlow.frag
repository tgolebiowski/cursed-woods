#version 150
flat in vec4 vertexColor;

void main()
{
    vec4 color = vertexColor;
	gl_FragColor = color;
    gl_FragDepth = 5.0f;
}