#version 150
//global values
uniform vec4 fogColor;
uniform vec4 lightColor;
uniform vec4 shadowColor;
uniform vec3 GlobalLightAngle;
uniform sampler2D staticShadowMap;
uniform sampler2D canopyShadowMap;
uniform float depthMin;
uniform float depthMax;
uniform float worldSize;
uniform float screenCenterX;
uniform float screenCenterY;
uniform float treetopsChannelSample;

//instance dependent
uniform mat4 transformMatrix;
uniform vec4 rotationQuat;

//passed from vertex
noperspective in vec4 vertColor;
noperspective in vec3 position;
noperspective in float lightValue;
noperspective in vec3 sampleDir;

void main()
{
	//setup
	//vec4 color = vertColor;
	gl_FragColor = vertColor;

	vec3 fragPosition = position;
	vec2 xzPos = fragPosition.xz;
	xzPos.x = (xzPos.x / worldSize) + 0.5;
	xzPos.y = (xzPos.y / worldSize) + 0.5;

	vec2 sampleDirection = vec2(GlobalLightAngle.x, GlobalLightAngle.z);
	sampleDirection = normalize(sampleDirection);
	float sampleDistance = ((fragPosition.y + fragPosition.y) * cos(GlobalLightAngle.y)) / worldSize;

	//end Fogstuff, begin ambientOcclusion
	float occlusion = 0.0;
	const float maxAmbientOccEffect = 0.5f;
	const float occHeightCuttoff = 1.5f;
	if(fragPosition.y == 0.0)
	{
		occlusion = 1.0 - texture2D(staticShadowMap, xzPos).r;

		if(occlusion > 0.0)
		{
			float shadowColorWeight = occlusion * maxAmbientOccEffect;
			float currentColorWeight = 1.0f - shadowColorWeight;
			gl_FragColor.r = (shadowColor.r * shadowColorWeight) + (gl_FragColor.r * currentColorWeight);
			gl_FragColor.g = (shadowColor.g * shadowColorWeight) + (gl_FragColor.g * currentColorWeight);
			gl_FragColor.b = (shadowColor.b * shadowColorWeight) + (gl_FragColor.b * currentColorWeight);

			float intensity = 0.3 * gl_FragColor.r + 0.59 * gl_FragColor.g + 0.11 * gl_FragColor.b;
			float k = occlusion * maxAmbientOccEffect; // * 0.20;
			float kInv = 1.0 - k;
			gl_FragColor.r = intensity * k + gl_FragColor.r * kInv;
			gl_FragColor.g = intensity * k + gl_FragColor.g * kInv;
			gl_FragColor.b = intensity * k + gl_FragColor.b * kInv;
		}
	}
	else if(fragPosition.y < occHeightCuttoff) {
		occlusion = 1.0 - texture2D(staticShadowMap, xzPos + sampleDirection * sampleDistance).r;

		if(occlusion > 0.0)
		{
			float heightFactor = (1.0 - (fragPosition.y / occHeightCuttoff));
			float shadowColorWeight = occlusion * maxAmbientOccEffect * heightFactor;
			float currentColorWeight = 1.0f - shadowColorWeight;
			gl_FragColor.r = (shadowColor.r * shadowColorWeight) + (gl_FragColor.r * currentColorWeight);
			gl_FragColor.g = (shadowColor.g * shadowColorWeight) + (gl_FragColor.g * currentColorWeight);
			gl_FragColor.b = (shadowColor.b * shadowColorWeight) + (gl_FragColor.b * currentColorWeight);

			float intensity = 0.3 * gl_FragColor.r + 0.59 * gl_FragColor.g + 0.11 * gl_FragColor.b;
			float k = occlusion * maxAmbientOccEffect * heightFactor; // * 0.20;
			float kInv = 1.0 - k;
			gl_FragColor.r = intensity * k + gl_FragColor.r * kInv;
			gl_FragColor.g = intensity * k + gl_FragColor.g * kInv;
			gl_FragColor.b = intensity * k + gl_FragColor.b * kInv;
		}
	}

	//ending ambient occlusion stuff, begin tree top shadows
	const float treeLightCuttoff = 0.1;
	if(lightValue >= treeLightCuttoff)
	{
		float adjustedChannelSample = (treetopsChannelSample + ((xzPos.x + xzPos.y)/2.0))/2.0;
		//
		if(fragPosition.y == 0.0)
		{
			float lightPeekingValue = (texture2D(canopyShadowMap, xzPos).r * adjustedChannelSample) +
			                          (texture2D(canopyShadowMap, xzPos).g * (1.0 - adjustedChannelSample));
			lightPeekingValue *= ((lightValue - treeLightCuttoff) / (1.0 - treeLightCuttoff));

			if(occlusion > 0.0 && occlusion < 0.2)
				lightPeekingValue *= (1.0 - (occlusion/ 0.2));
			else if(occlusion >= 0.2)
				lightPeekingValue = 0.0;

			if(lightPeekingValue > 0.0) gl_FragColor = mix(gl_FragColor, lightColor, lightPeekingValue * 0.5);
			
		}
		else if(fragPosition.y < 8.0)
		{
			float lightPeekingValue = texture2D(canopyShadowMap, xzPos + sampleDirection * sampleDistance).r * adjustedChannelSample +
				                      texture2D(canopyShadowMap, xzPos + sampleDirection * sampleDistance).g * (1.0 - adjustedChannelSample);
			lightPeekingValue *= ((lightValue - treeLightCuttoff) / (1.0 - treeLightCuttoff));

			if(occlusion > 0.0 && occlusion < 0.2) 
				lightPeekingValue *= (1.0 - (occlusion/ 0.2));
			else if(occlusion >= 0.2) 
				lightPeekingValue = 0.0;

			if(lightPeekingValue > 0.0) 
				gl_FragColor = mix(gl_FragColor, lightColor, lightPeekingValue * 0.5);
		}
	}

	//fog stuff
	float depthfogFactor = gl_FragCoord.z / gl_FragCoord.w;
	depthfogFactor -= depthMin;
	depthfogFactor /= (depthMax - depthMin);

	//Normalized screen position from 1.0 to -1.0 with center as 0,0
	float offCenterX = (gl_FragCoord.x/screenCenterX) - 1.0;
	float offCenterY = (gl_FragCoord.y/screenCenterY) - 1.0;
	float screenDistance = sqrt(offCenterX * offCenterX + offCenterY * offCenterY);
	//0.708150 = 1 / sqrt(2)
	float screenCenterFogFactor = screenDistance * 0.708150;

	float netFogFactor = depthfogFactor * screenCenterFogFactor;
	//netFogFactor = clamp(netFogFactor, 0.0, 1.0);
	//netFogFactor = pow(netFogFactor, 1.6);

	gl_FragColor = mix(gl_FragColor, fogColor, netFogFactor);

	const float beginAlphaCutoff = 8.0;
	const float endAlphaCutoff = 10.0;
	if(position.y > beginAlphaCutoff)
	{
		if(position.y < endAlphaCutoff)
		{
			gl_FragColor.a = (endAlphaCutoff - position.y) / (endAlphaCutoff - beginAlphaCutoff);
			gl_FragColor.a = gl_FragColor.a * gl_FragColor.a * gl_FragColor.a;
		}
		else if (position.y >= endAlphaCutoff)
		{
			gl_FragColor.a = 0.0;
		}
	}
}