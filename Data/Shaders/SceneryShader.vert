#version 150
//global values
uniform vec4 fogColor;
uniform vec4 lightColor;
uniform vec4 shadowColor;
uniform vec3 GlobalLightAngle;
uniform sampler2D staticShadowMap;
uniform sampler2D canopyShadowMap;
uniform float screenCenterX;
uniform float screenCenterY;
uniform float worldSize;
uniform float depthMin;
uniform float depthMax;
uniform float treetopsChannelSample;

//instance dependent
uniform mat4 transformMatrix;
uniform vec4 rotationQuat;

//stuff to pass to fragment shader
noperspective out vec4 vertColor;
noperspective out vec3 position;
noperspective out float lightValue;
noperspective out vec3 sampleDir;

vec3 transformPoint(vec4 v) {

	return (transformMatrix * v).xyz;
}

vec3 rotateVec(vec3 v)
{
	vec3 t = 2 * cross(rotationQuat.xyz, v);
	return v + rotationQuat.w * t + cross(rotationQuat.xyz, t);
}

void main() {
	gl_Position = ftransform();

	position = transformPoint(gl_Vertex);

    vertColor = gl_Color;
	vec3 normal = rotateVec(gl_Normal);

	sampleDir = rotateVec(GlobalLightAngle);
	sampleDir.y = -cos(sampleDir.y);
	sampleDir.z *= -1.0;

	//normal = normalize(normal);
	
	float baseLightValue = dot(GlobalLightAngle, normal);
	lightValue = baseLightValue;

	vec4 color = vertColor;
	if(baseLightValue >= 0.0) //This is for lit stuff
	{
		float lightColorWeight = (baseLightValue / 2.0);
		float baseColorWeight = 1.0 - lightColorWeight;
		color.r = (lightColor.r * lightColorWeight) + (color.r * baseColorWeight);
		color.g = (lightColor.g * lightColorWeight) + (color.g * baseColorWeight);
		color.b = (lightColor.b * lightColorWeight) + (color.b * baseColorWeight);
	}
	else //This is for stuff in shadows
	{
		baseLightValue = -baseLightValue;
		float shadowColorWeight = (baseLightValue / 2.0);
		float baseColorWeight = 1.0 - shadowColorWeight;
		color.r = (shadowColor.r * shadowColorWeight) + (color.r * baseColorWeight);
		color.g = (shadowColor.g * shadowColorWeight) + (color.g * baseColorWeight);
		color.b = (shadowColor.b * shadowColorWeight) + (color.b * baseColorWeight);

		float intensity = 0.3f * color.r + 0.59 * color.g + 0.11 * color.b;
		float k = baseLightValue * baseLightValue * 0.20;
		float kInv = 1.0 - k;
		color.r = intensity * k + color.r * kInv;
		color.g = intensity * k + color.g * kInv;
		color.b = intensity * k + color.b * kInv;
	}
	vertColor = color;
}