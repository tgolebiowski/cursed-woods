#pragma once
#include <Polycode.h>
#ifndef GUIMANAGER
#define GUIMANAGER
#include "Player.h"
#include "GameLogicHub.h"
#include "WorldMap.h"
#include "GuiScreen.h"
#include "Input.h"

using namespace Polycode;

class GuiManager
{
public:
	GuiManager(Core* core, Scene* scene, Input* input, GameLogicHub* gameLogicHub, Vector2 screensize);
	~GuiManager(void);

	void update();
	void setBlackScreened(bool state) { blackScreened = state; };

	bool hasScreenUp;

	SceneImage* debugImage;

private:
	Core* core;
	Scene* guiScene;
	Player* player;
	GameLogicHub* gameLogicHub;
	Input* input;

	Scene* guiScreenVirtualScene;
	SceneRenderTexture* guiScreenRenderTexture;

	ScenePrimitive* debugInfoSpot;
	SceneImage* guiScreenImage;

	//Debug info labels
	SceneLabel* renderCountLabel;
	SceneLabel* physicsEntityCountLabel;
	SceneLabel* fpsLabel;

	ScenePrimitive* pinkTransitionDot;
	ScenePrimitive* blackScreen;
	bool blackScreened;

	GuiScreen* currentScreen;
	EmberCurseScreen* emberCurseScreen;
	MapScreen* mapScreen;
};
#endif