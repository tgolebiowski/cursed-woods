#include "Level.h"
CollisionScene* Level::scene = NULL;

LevelSite::~LevelSite()
{
	free(low_LOD_Meshes);
	free(high_LOD_Meshes);

	for (int i = 0; i < colliderCount; i++)
		delete colliders[i];
}

int Level::lastLargeMeshRenderCount = 0;

Level::Level()
{
	artifact = NULL;
	lastCenterSite = NULL;

	aabb.max = Vector3(-50, 5, -50);
	aabb.min = Vector3(50, -5, 50);
	bBox.set(100, 10, 100);
}

Level::~Level(void)
{
	scene->removeEntity(groundMesh);

	for (int i = 0; i < activeColliders.size(); i++)
	{
		scene->removeEntity(activeColliders.at(i)->getEntity());
		scene->removeCollision(activeColliders.at(i)->getEntity());
	}

	for (int i = 0; i < levelSites.size(); i++)
	{
		LevelSite* site = levelSites[i];
		delete site;
	}

	for(int i = 0; i < transitionColliders.size(); i++)
		delete transitionColliders[i];

	if (artifact != NULL) { artifact->unload(); delete artifact; }

	delete groundMesh;
	delete levelMap;
	delete groundLightMap;
}

void Level::level_update(Vector3 focus)
{
	Site* siteLookup = levelMap->getNearestSite(focus.x, focus.z);
	LevelSite* centerSite = (LevelSite*)siteLookup->userData;

	if (centerSite == lastCenterSite)
		goto skipcolliderchecks;

	if (lastCenterSite != NULL)
	{
		LevelSite::NeighborGroupList lastCenterGroupList = lastCenterSite->colliders_Neighbor_List;
		LevelSite::NeighborGroupList newCenterGroupList = centerSite->colliders_Neighbor_List;

		bool inLastCenterGroup[32];
		for (int i = 0; i < newCenterGroupList.count; i++)
			inLastCenterGroup[i] = false;

		for (int i = 0; i < lastCenterGroupList.count; i++)
		{
			int searchIndex = lastCenterGroupList.indexes[i];

			//check if this index from the old neighbor list is in the new neighbor list
			bool repeatFound = false;
			for (int j = 0; j < newCenterGroupList.count; j++)
			{
				if (searchIndex == newCenterGroupList.indexes[j])
				{
					inLastCenterGroup[j] = true;
					repeatFound = true;
					break;
				}
			}

			// if this index was in the old neighbor list, but not in the new one, its colliders musbt be removed
			if (!repeatFound) 
			{
				for (int j = 0; j < levelSites[searchIndex]->colliderCount; j++)
				{
					ScenePrimitive* collider = levelSites[searchIndex]->colliders[j];
					scene->removeCollision(collider);
					scene->removeEntity(collider);
					for (int k = 0; k < activeColliders.size(); k++)
						if (activeColliders[k]->getEntity() == collider)
						{
							activeColliders.erase(activeColliders.begin() + k);
							break;
						}
				}
			}
		}

		//if an index in the new neighbor list was not flagged as being in the old neighbor list, it needs to be
		//added
		for (int i = 0; i < newCenterGroupList.count; i++)
		{
			//tho skip it if it was the last center site
			if (inLastCenterGroup[i] != false || newCenterGroupList.indexes[i] == lastCenterSite->index) continue;

			LevelSite* site = levelSites[newCenterGroupList.indexes[i]];

			for (int j = 0; j < site->colliderCount; j++)
			{
				ScenePrimitive* collider = site->colliders[j];
				activeColliders.push_back(scene->addCollisionChild(collider, collider->getPrimitiveType()));
			}
		}
	}
	else
	{
		for (int i = 0; i < centerSite->colliders_Neighbor_List.count; i++)
		{
			LevelSite* collisionActiveNeighbor = levelSites[centerSite->colliders_Neighbor_List.indexes[i]];

			for (int j = 0; j < collisionActiveNeighbor->colliderCount; j++)
			{
				ScenePrimitive* collider = collisionActiveNeighbor->colliders[j];
				activeColliders.push_back(scene->addCollisionChild(collider, collider->getPrimitiveType()));
			}
		}
	}

	lastCenterSite = centerSite;

skipcolliderchecks:

	if (artifact != NULL){ artifact->update(); }
}

void Level::Render()
{
	Renderer* renderer = CoreServices::getInstance()->getRenderer();

	renderer->enableAlphaTest(true);
	renderer->enableBackfaceCulling(true);
	renderer->enableDepthTest(true);
	renderer->enableDepthWrite(true);
	renderer->setTexture(NULL);
	renderer->setBlendingMode(Renderer::BLEND_MODE_NORMAL);
	renderer->clearShader();
	
	//Material* sceneryMaterial = (Material*)CoreServices::getInstance()->getResourceManager()->getGlobalPool()
		//->getResource(Resource::RESOURCE_MATERIAL, "SceneryShader");
	//Shader* sceneryShader = sceneryMaterial->getShader(0);

	//renderer->applyMaterial(sceneryMaterial, lastCenterSite->low_LOD_Meshes[0].localShaderOptions, 0, false);

	lastLargeMeshRenderCount = 0;

	for (int i = 0; i < lastCenterSite->low_LOD_Neighbor_List.count; i++)
	{
		LevelSite* site = levelSites[lastCenterSite->low_LOD_Neighbor_List.indexes[i]];

		LostWoods_Mesh* meshes = site->low_LOD_Meshes;
		for (int j = 0; j < site->lowLODCount; j++)
		{
			renderer->pushMatrix();
			renderer->multModelviewMatrix(meshes[j].transformMatrix);
			renderer->pushVertexColor();
			renderer->multiplyVertexColor(meshes[j].color);

			//renderer->setRendererShaderParams(sceneryShader, meshes[i].localShaderOptions);
			renderer->drawVertexBuffer(meshes[j].buffer, true);

			renderer->popMatrix();
			renderer->popVertexColor();

			lastLargeMeshRenderCount++;
		}
	}

	renderer->clearShader();
}

void Level::unload()
{
	scene->removeEntity(groundMesh);

	for (int i = 0; i < activeColliders.size(); i++)
	{
		scene->removeCollision(activeColliders.at(i)->getEntity());
		scene->removeEntity(activeColliders.at(i)->getEntity());
	}
	activeColliders.clear();

	for(int i = 0; i < transitionColliders.size(); i++)
	{
		scene->removeCollision(transitionColliders.at(i));
		scene->removeEntity(transitionColliders.at(i));
	}

	if (artifact != NULL) { artifact->unload(); }
	lastCenterSite = NULL;
}

void Level::loadin(Vector3 startFocus)
{
	if (artifact != NULL) { artifact->loadin(); }

	scene->addEntity(groundMesh);
	level_update(startFocus);

	for(int i = 0; i < transitionColliders.size(); i++)
	{
		scene->addCollisionChild(transitionColliders.at(i));
	}
}

void LevelSite::addMesh(LevelLayoutStruct::SceneryObjInfo objInfo, LevelPart part, bool isHighLOD)
{
	LostWoods_Mesh* newMesh;
	if (isHighLOD)
	{
		newMesh = &high_LOD_Meshes[highLODCount];
		highLODCount++;
	}
	else
	{
		newMesh = &low_LOD_Meshes[lowLODCount];
		lowLODCount++;
	}

	Quaternion rotationQuat;
	rotationQuat.createFromAxisAngle(0.0, -1.0, 0.0, -objInfo.angle);

	newMesh->baseMesh = &part.mesh;
	newMesh->buffer = part.meshBuffer;
	newMesh->color = part.meshColor;

	Matrix4 transformMatrix;
	transformMatrix.identity();
	Matrix4 scaleMatrix;
	scaleMatrix.identity();
	Matrix4 rotationMatrix = rotationQuat.createMatrix();
	transformMatrix.setPosition(objInfo.x, 0.0f, objInfo.z);
	Vector3 scaling;

	if (objInfo.useScaleXAsTarget)
	{
		float widthScalingNeeded = objInfo.scaleX / (part.width * part.scaling);
		scaling.x = (part.scaling * widthScalingNeeded);
	}
	else
	{
		scaling.x = (part.scaling * objInfo.scaleX);
	}

	if (objInfo.useScaleZAsTarget)
	{
		float depthScalingNeeded = objInfo.scaleZ / (part.depth * part.scaling);
		scaling.z = (part.scaling * depthScalingNeeded);
	}
	else
	{
		scaling.z = (part.scaling * objInfo.scaleZ);
	}
	
	scaling.y = part.scaling;
	scaleMatrix.setScale(scaling);
	newMesh->transformMatrix = scaleMatrix * rotationMatrix * transformMatrix;

	Material* sceneryMaterial = (Material*)CoreServices::getInstance()->getResourceManager()->getGlobalPool()
		->getResource(Resource::RESOURCE_MATERIAL, "SceneryShader");
	newMesh->localShaderOptions = sceneryMaterial->getShader(0)->createBinding();
	newMesh->localShaderOptions->addParam(ProgramParam::PARAM_MATRIX, "transformMatrix")
		->setMatrix4(newMesh->transformMatrix);

	Quaternion q = Quaternion();
	newMesh->localShaderOptions->addParam(ProgramParam::PARAM_COLOR, "rotationQuat")
		->setColor(Color(q.w, q.x, q.y, q.z));
}
