#include <Polycode.h>
#ifndef AISTRUCTS_H
#define AISTRUCTS_H

using namespace Polycode;
struct WorldState
{
	Vector3 position;
	Vector3 playerPosition;
	float thisEmber;
	float thisCurse;
	float playerEmber;
	float playerCurse;

	WorldState();
	WorldState(Vector3 thisP, Vector3 p, float thisEmber,
		float thisCurse, float playerEmber, float playerCurse);
	float getDiff(WorldState otherState);
};
#endif