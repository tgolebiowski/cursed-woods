#pragma once
#include <Polycode.h>
#ifndef LIGHTSHADERHUB
#define LIGHTSHADERHUB
#include "ColorTable.h"
#include "GameCam.h"
#include "SaveStructs.h"
#include "Artifact.h"

using namespace Polycode;

class LightShaderHub
{
public:
	LightShaderHub(Vector2 screenSize);
	~LightShaderHub();

	static LightShaderHub* getInstance() { return shaderHubInstance; }
	void setStaticShadowMap(Image* image);
	void update(Vector3 update);
	void createStaticOcclusionMap(LevelLayoutStruct* levelInfo, LevelStateInfo* stateInfo, float groundPlaneSize);

private:
	Material* sceneryShader;
	SceneImage* staticShadowMap;
	SceneImage* canopyShadowMap;

	Vector3 lightDirection;
	float treetopsChannelSampleCycle;
	static LightShaderHub* shaderHubInstance;

	/*Scene* occlusionRenderScene;
	SceneImage* currentStaticOcclusionMap;
	SceneImage* canopyOcclusionMap;
	SceneImage* kinematicOcclusionMap;*/
};
#endif

