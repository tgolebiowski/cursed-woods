#include <Polycode.h>
#ifndef COLORTABLE
#define COLORTABLE

struct ColorTable
{
	static Polycode::Color brown2;
	static Polycode::Color brown3;
	static Polycode::Color brownRed;

	static Polycode::Color purple_gray;
	static Polycode::Color yellow_gray;
	static Polycode::Color darkGray_Blue;
	static Polycode::Color lightGray_littlePurple;
	static Polycode::Color gray_charcoal;

	static Polycode::Color lightCursePurple;
	static Polycode::Color midCursePurple;
	static Polycode::Color curseShadow;

	static Polycode::Color skin;
	static Polycode::Color emberOrange;
	static Polycode::Color emberYellow;

	static Polycode::Color turquiose1;

	static Polycode::Color blue1;

	static Polycode::Color green1;

	Polycode::Image pallette;

	void initTable()
	{
		Polycode::Image palletteLookup = Polycode::Image("Data/PalletteLookup.png");
		this->pallette = palletteLookup;

		brown2 = palletteLookup.getPixel(12, 12);
		brown3 = palletteLookup.getPixel(12, 10);
		brownRed = palletteLookup.getPixel(10, 8);

		purple_gray = palletteLookup.getPixel(14, 14);
		yellow_gray = palletteLookup.getPixel(4, 14);
		lightGray_littlePurple = palletteLookup.getPixel(6, 14);
		gray_charcoal = palletteLookup.getPixel(8, 14);
		darkGray_Blue = palletteLookup.getPixel(10, 14);

		lightCursePurple = palletteLookup.getPixel(14, 12);
		midCursePurple = palletteLookup.getPixel(14, 10);
		curseShadow = palletteLookup.getPixel(8, 10);

		emberOrange = palletteLookup.getPixel(0, 0);
		emberYellow = palletteLookup.getPixel(0, 2);

		skin = palletteLookup.getPixel(8, 8);

		turquiose1 = palletteLookup.getPixel(14, 4);

		blue1 = palletteLookup.getPixel(8, 6);

		green1 = palletteLookup.getPixel(14, 0);
	}

	Polycode::Color getColor(int x, int y)
	{
		return Polycode::Color(1.0, 1.0, 1.0, 1.0);
	}
};

#endif