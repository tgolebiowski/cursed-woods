#include <Polycode.h>
#include <Polycode3DPhysics.h>
#ifndef VISUALS_TEST_APP_H
#define VISUALS_TEST_APP_H
#include <PolycodeView.h>
#include "..\LightShaderHub.h"
#include "..\Level.h"
#include "..\WorldGeneration.h"
#include "..\ColorTable.h"
#include "..\LevelSaverLoader.h"

using namespace Polycode;
class VisualTests_App
{
public:
	VisualTests_App(PolycodeView* view);
	~VisualTests_App();

	bool Update();

private:
	Core* core;
	CollisionScene* scene;

	ColorTable colorTable;
	LevelSaverLoader* saverloader;
	LightShaderHub* shaderHub;

	WorldGeneration levelGenerator;
	Level* testLevel;

	Vector2 cameraPosition;
	const Vector2 screensize = Vector2(1300, 700);
	const Vector3 cameraOffset = Vector3(0.0, std::sin(120.0f) * 30.0, -std::cos(120.0f) * 30.0);
	const float factor = 0.01 * 2.0;
	const Vector2 cameraSize = Vector2(screensize.x * factor, screensize.y * factor);
};
#endif