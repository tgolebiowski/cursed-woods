#include "VisualTests_App.h"
#include <time.h>


VisualTests_App::VisualTests_App(PolycodeView *view)
{
	core = new Win32Core(view, screensize.x, screensize.y, false, false, 0, 0, 30);
	CoreServices::getInstance()->getResourceManager()->addArchive("default.pak");
	CoreServices::getInstance()->getResourceManager()->addArchive("hdr.pak");
	CoreServices::getInstance()->getResourceManager()->addDirResource("default", false);
	CoreServices::getInstance()->getResourceManager()->addDirResource("hdr", true);
	CoreServices::getInstance()->getResourceManager()->addDirResource("Data", true);

	ColorTable table;
	table.initTable();

	time_t seed = time(NULL);
	srand(seed);
	Logger::log("Using Seed: %d\n", &seed);

	scene = new CollisionScene(Vector3(200));
	scene->getDefaultCamera()->setOrthoSize(cameraSize.x, cameraSize.y);
	scene->getDefaultCamera()->setPosition(cameraOffset);
	scene->getDefaultCamera()->lookAt(Vector3(0));

	shaderHub = new LightShaderHub(screensize);
	saverloader = new LevelSaverLoader(NULL, NULL);

	Level::scene = scene;
	levelGenerator = WorldGeneration();
	testLevel = levelGenerator.generateVisualTestLevel(saverloader);
	scene->addEntity(testLevel);

	//Artifact::setGlobalVars(scene, NULL, NULL, NULL, NULL, 0);
	//testLevel->loadin(Vector3(0));
}


VisualTests_App::~VisualTests_App()
{
}

bool VisualTests_App::Update()
{
	return core->updateAndRender();
}
