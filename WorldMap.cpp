#include "WorldMap.h"

WorldMap::WorldMap()
{
	current = 0;
}

void WorldMap::makeConnection(MapNode* parent, MapNode* child)
{
	parent->neighbors.push_back(child);
	child->neighbors.push_back(parent);

	NodeConnection connection;
	connection.parent = parent;
	connection.child = child;
	connections.push_back(connection);
}

WorldMap::MapNode* WorldMap::addNode(Site* site)
{
	MapNode* newNode = new MapNode();
	newNode->identifier = nodes.size();
	newNode->position = site->p;
	newNode->baseSite = site;
	site->userData = (void*)newNode;
	nodes.push_back(newNode);
	return newNode;
}