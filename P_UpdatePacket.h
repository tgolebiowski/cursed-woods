#include <Polycode.h>
#ifndef PUPDATEPACKET
#define PUPDATEPACKET

using namespace Polycode;

struct P_UpdatePacket
{
	Vector2 moveInput;
	Vector2 equipmentInput;
	bool equipmentActionState;
	bool equipmentActionWasPressed;
	bool dashWasPressed;
	bool switchEquipmentPressed;
};
#endif