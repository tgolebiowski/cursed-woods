#include "Core.h"
#include <math.h>
#include <time.h>

LostWoods::LostWoods(PolycodeView *view) {
	core = new Win32Core(view, screensize.x, screensize.y, false, false, 0, 0, 30);
	CoreServices::getInstance()->getResourceManager()->addArchive("default.pak");
	CoreServices::getInstance()->getResourceManager()->addArchive("hdr.pak");
	CoreServices::getInstance()->getResourceManager()->addDirResource("default", false);
	CoreServices::getInstance()->getResourceManager()->addDirResource("hdr", true);
	CoreServices::getInstance()->getResourceManager()->addDirResource("Data", true);

	srand(time(NULL));

	ColorTable table;
	table.initTable();

	input = new Input(core->getInput());
	gameScene = new CollisionScene(Vector3(200));
	gameCore = new GameLogicHub(gameScene, screensize, input);

	guiScene = new Scene(Scene::SCENE_2D_TOPLEFT);
	guiManager = new GuiManager(core, guiScene, input, gameCore, screensize);

	debugTools = DebugTools(core, gameCore->player, gameCore, gameCore->gameCam);
}
LostWoods::~LostWoods() {
    
}

bool LostWoods::Update() {
	input->update();

	gameCore->update();

	if (gameCore->transitioning) guiManager->setBlackScreened(true);
	else guiManager->setBlackScreened(false);

	guiManager->update();

	debugTools.update();

	bool updateReturn = core->Update();

	gameCore->player->animate();

	core->Render();

	return updateReturn;
}