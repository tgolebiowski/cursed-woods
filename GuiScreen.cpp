#include "GuiScreen.h"

GuiScreen::GuiScreen(Scene* guiScene, Input* input)
{
	this->guiScene = guiScene;
	this->input = input;
	closeFlag = false;
}

EmberCurseScreen::EmberCurseScreen(Scene* guiScene, Input* input, Player* player) : 
GuiScreen(guiScene, input)
{
	this->player = player;

	skinColor = ColorTable::skin;
	curseColor = ColorTable::lightCursePurple;

	Image* armTexture = new Image(1024, 1024, Image::IMAGE_RGBA);
	Perlin merlin = Perlin(4, 16.0f, 1.0f, 15);

	float curseCoeff = 0.33f;
	float bottomCutoff = curseCoeff - 0.1f;
	bottomCutoff = std::max(0.0, (double)bottomCutoff);
	float topCutoff = curseCoeff + 0.1f;
	topCutoff = std::min(1.0, (double)topCutoff);

	for (int i = 0; i < 1024; i++)
	{
		for (int j = 0; j < 1024; j++)
		{
			float pixelNoiseValue = (merlin.Get2D(((float)i) / 1024.0f, ((float)j) / 1024.0f) + 1.0) / 2.0;
			float pixelBlendVal = 0.0f;

			Color blendColor = curseColor;
			if (pixelNoiseValue >= bottomCutoff && pixelNoiseValue <= topCutoff)
			{
				//pixelBlendVal = (pixelNoiseValue - bottomCutoff) / (topCutoff - bottomCutoff);
				pixelBlendVal = 1.0;
			}
			else if (pixelNoiseValue < bottomCutoff)
			{
				pixelBlendVal = 1.0f;
				blendColor = ColorTable::midCursePurple;
			}
			else if (pixelNoiseValue > topCutoff)
			{
				pixelBlendVal = 0.0f;
			}

			Color targetColor = skinColor.blendColor(blendColor, Color::BLEND_NORMAL, pixelBlendVal);
			armTexture->setPixel(i, j, targetColor);
		}
	}

	armMesh = new SceneMesh("Data/MenuArm.mesh");
	armMesh->setPosition(1.2, -0.2f, 2.5);
	armMesh->setRoll(25.0f);
	armMesh->setPitch(-15.0f);
	armMesh->setYaw(180.0f);
	armMesh->loadTextureFromImage(armTexture);

	emberGlow = new ScenePrimitive(ScenePrimitive::TYPE_ICOSPHERE, 1.75, 1.0);
	emberGlow->setColor(ColorTable::emberOrange.r, ColorTable::emberOrange.g, ColorTable::emberOrange.b, 0.75f);
	emberGlow->setPosition(0.0, -0.75, 2.75);
	emberGlow->depthWrite = true;
	emberGlow->alphaTest = true;
	emberGlow->setMaterialByName("EmberGlow");

	emberChainMesh = new ScenePrimitive(ScenePrimitive::TYPE_BOX, 0.25, 0.25, 0.25);
	emberChainMesh->setColor(ColorTable::yellow_gray);
	emberChainMesh->setPosition(0.0, -0.75, 2.5);
	emberChainMesh->depthTest = true;
}

void EmberCurseScreen::update()
{
	if (input->pauseWasPressed)
	{
		closeFlag = true;
	}

	emberGlow->setYaw(emberGlow->getYaw() + 5.0f);
}

void EmberCurseScreen::loadin()
{
	closeFlag = false;
	guiScene->addEntity(armMesh);
	guiScene->addEntity(emberChainMesh);
	guiScene->addEntity(emberGlow);
}

void EmberCurseScreen::unload()
{
	guiScene->removeEntity(armMesh);
	guiScene->removeEntity(emberChainMesh);
	guiScene->removeEntity(emberGlow);
}

MapScreen::MapScreen(Scene* guiScene, Input* input, WorldMap* worldMap) :GuiScreen(guiScene, input)
{
	this->worldMap = worldMap;
	this->mapTextureRenderScene = new Scene(Scene::SCENE_2D, true);
	this->mapTextureRenderScene->getDefaultCamera()->setOrthoMode(true);
	this->mapTextureRenderScene->getDefaultCamera()->setOrthoSize(512, 512);
	
	this->mapTextureRenderTexture = new SceneRenderTexture(this->mapTextureRenderScene,
		this->mapTextureRenderScene->getDefaultCamera(), 512, 512);
//this->mapTextureRenderTexture->

	scaling = 1.3f;

	backdrop = new ScenePrimitive(ScenePrimitive::TYPE_VPLANE, 512, 512);
	backdrop->setColor(0.33, 0.33, 0.33, 1.0);
	mapTextureRenderScene->addEntity(backdrop);

	marker = new ScenePrimitive(ScenePrimitive::TYPE_CIRCLE, 14, 14, 8);
	marker->setColor(ColorTable::emberYellow);
	marker->setPositionZ(1.1f);
	mapTextureRenderScene->addEntity(marker);

	for (int i = 0; i < worldMap->connections.size(); i++)
	{
		WorldMap::NodeConnection connection = worldMap->connections.at(i);
		Vector2 p1 = connection.child->position;
		Vector2 p2 = connection.parent->position;
		SceneLine* line = new SceneLine(Vector3(p1.x / scaling, -p1.y / scaling, 0.0f),
			Vector3(p2.x / scaling, -p2.y / scaling, 1.0f));
		connections.push_back(line);
		mapTextureRenderScene->addEntity(line);
	}
	for (int i = 0; i < worldMap->nodes.size(); i++)
	{
		WorldMap::MapNode* node = worldMap->nodes.at(i);
		Vector2 p = node->position;
		ScenePrimitive* dot = new ScenePrimitive(ScenePrimitive::TYPE_CIRCLE, 8, 8, 8);
		dot->setPosition(p.x / scaling, -p.y / scaling, 1.0f);
		nodes.push_back(dot);
		mapTextureRenderScene->addEntity(dot);
	}

	this->mapTextureRenderTexture->Render();

	this->mapMesh = new SceneMesh("Data/Menu_Map.mesh");
	this->mapMesh->setPosition(0, 0, 2.75f);
	this->mapMesh->setTexture(this->mapTextureRenderTexture->getTargetTexture());
	this->mapMesh->setYaw(90.0f);
}

void MapScreen::update()
{
	static Vector2 p = Vector2(0, 0);
	Vector2 currentWorldLevelPosition = worldMap->nodes[worldMap->current]->position;
	if (p.x != currentWorldLevelPosition.x && p.y != currentWorldLevelPosition.y)
	{
		marker->setPosition(currentWorldLevelPosition.x / scaling, -currentWorldLevelPosition.y / scaling);
		p = currentWorldLevelPosition;
		this->mapTextureRenderTexture->Render();
		mapMesh->setTexture(this->mapTextureRenderTexture->getTargetTexture());
	}

	if (input->selectWasPressed)
	{
		closeFlag = true;
	}
}

void MapScreen::loadin()
{
	closeFlag = false;
	guiScene->addEntity(this->mapMesh);
}

void MapScreen::unload()
{
	guiScene->removeEntity(this->mapMesh);
}