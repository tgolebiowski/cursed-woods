#pragma once
#include <Polycode.h>
#ifndef TRIANGULATOR_H
#define TRIANGULATOR_H

using namespace Polycode;

struct Triangle;
class Triangulator;

struct Site
{
	Vector2 p;

	std::vector<Triangle*> incidentTris;
	std::vector<Site*> neighbors;

	void* userData;

	Site(Vector2 p)
	{
		userData = NULL;
		this->p = p;
	}

	~Site()
	{

	}
};

struct Edge
{
	Site* site1;
	Site* site2;

	Triangle* tri1;
	Triangle* tri2;

	Edge(Site* site1, Site* site2)
	{
		this->site1 = site1;
		this->site2 = site2;
		tri1 = NULL;
		tri2 = NULL;
	}
};

struct Triangle
{
	Site* sites[3];
	//edges[0] : sites[0] & sites[1] /// edges[1] : sites[1] & sites[2] /// edges[2] : sites[2] & sites[0]
	Edge* edges[3];
	Triangle* neighbors[3];

	void* userData;
	void* voronoiHolder;
};

struct Circle
{
	Vector2 center;
	float radius;

	Circle()
	{

	};

	Circle(Triangle* tri);
};

struct VoronoiNode
{
	VoronoiNode* neighbors[3];
	Vector2 position;

	VoronoiNode()
	{
		neighbors[0] = NULL;
		neighbors[1] = NULL;
		neighbors[2] = NULL;
	}

	VoronoiNode(Triangle* baseTri)
	{
		position = Circle(baseTri).center;

		neighbors[0] = NULL;
		neighbors[1] = NULL;
		neighbors[2] = NULL;
	}
};

struct VoronoiGraph
{
	int nodeCount;
	VoronoiNode* nodes;

	VoronoiGraph(Triangulator* triangulator);
	~VoronoiGraph();
};

class Triangulator
{
public:
	Triangulator(std::vector<Site*>* siteList);
	~Triangulator();

	void insert(Site* newSite);
	void floodFill(Site* startSite, float cutoff, std::vector<Site*>* outList);
	//void floodFillWithMap(Site* startSite, float cutoff, std::vector<Site*>* outList);
	Site* getNearestSite(float x, float y);
	Triangle* getTri(float x, float y, Triangle* startTri = NULL);

	std::vector<Edge*> edges;
	std::vector<Triangle*> tris;
	std::vector<Site*> sites;

private:
	void flip(Triangle* tri1, Triangle* tri2, Edge* edgeInCommon);
	int pointInTri(Triangle* tri, float x, float y);
	void insertSite(Triangle* enclosingTri, Site* newSite);

	//validation method
	static bool checkEdgeCorrespondence(Triangle* tri);

	static int generateHashForSite(Site* site);

	Triangle* centerMostTri;
};
#endif