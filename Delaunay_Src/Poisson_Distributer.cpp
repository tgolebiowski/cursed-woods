#include "Poisson_Distributer.h"
#include <random>
#include <time.h>

Poisson_Distributer::Poisson_Distributer(void)
{
}


Poisson_Distributer::~Poisson_Distributer(void)
{
}

void Poisson_Distributer::generate_poisson(float width, float height, float min_dist, int new_points)
{
	samplePoints.clear();

	float cellsize = min_dist/sqrt(2.0);

	//generate grid
	gridWidth = (int)(width/cellsize) + 1;
	gridHeight = (int)(height/cellsize) + 1;
	
	grid = new Vector2*[gridWidth];
	for(int i = 0; i < gridWidth; i++)
	{
		grid[i] = new Vector2[gridHeight];
	}

	srand(time(NULL));

	float firstX = rand() % gridWidth;
	float firstY = rand() % gridHeight;

	processList.push_back(Vector2(firstX, firstY));

	int maxPoints = gridWidth * gridHeight;

	while(!processList.empty() && samplePoints.size() < (gridWidth * gridHeight))
	{
		Vector2 point = processList.back();
		processList.pop_back();

		for(int i = 0; i < new_points; i++)
		{
			Vector2 newPoint = generate_point_around(point, min_dist);

			if(newPoint.x > 0.0f && newPoint.x < width && newPoint.y > 0.0f && newPoint.y < height)
			{
				if(!inNeighborhood(newPoint, min_dist, cellsize))
				{
					//update containers
					processList.push_back(newPoint);
					samplePoints.push_back(newPoint);

					Vector2 gridSpot = imageToGrid(newPoint, cellsize);
					grid[(int)gridSpot.x][(int)gridSpot.y] = newPoint;
				}
			}
		}
	}
}

Vector2 Poisson_Distributer::generate_point_around(Vector2 point, float min_dist)
{
	float rand1 = ((float)rand()/(RAND_MAX + 1));
	float rand2 = ((float)rand()/(RAND_MAX + 1));

	float randRadius = min_dist * (rand1 + 1);
	float randAngle = 2 * PI * rand2;

	float newX = point.x + randRadius * cos(randAngle);
	float newY = point.y + randRadius * sin(randAngle);

	return Vector2(newX, newY);
}

bool Poisson_Distributer::inNeighborhood(Vector2 point, float min_dist, float cellSize)
{
	Vector2 gridPoint = imageToGrid(point, cellSize);

	int area_X_Max = gridPoint.x + 2;
	int area_Y_Max = gridPoint.y + 2;
	int area_X_Min = gridPoint.x - 2;
	int area_Y_Min = gridPoint.y - 2;

	if(area_X_Max > gridWidth - 1) area_X_Max = gridWidth - 1;
	if(area_Y_Max > gridHeight - 1) area_Y_Max = gridHeight - 1;
	if(area_X_Min < 0) area_X_Min = 0;
	if(area_Y_Min < 0) area_Y_Min = 0;

	for(int i = area_X_Min; i <= area_X_Max; i++)
	{
		for(int j = area_Y_Min; j <= area_Y_Max; j++)
		{
			Vector2 otherPoint = grid[i][j];

			if(otherPoint.x != 0 && otherPoint.y != 0)
			{
				float dist = (otherPoint - point).length();

				if(dist < min_dist) return true;
			}
		}
	}

	return false;
}

Vector2 Poisson_Distributer::imageToGrid(Vector2 point, float cellSize)
{
	int gridX = (int)(point.x/cellSize);
	int gridY = (int)(point.y/cellSize);

	return Vector2(gridX, gridY);
}

void Poisson_Distributer::reset()
{
	samplePoints.clear();
}