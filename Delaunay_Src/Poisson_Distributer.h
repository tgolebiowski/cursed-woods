#pragma once
#include <Polycode.h>

using namespace Polycode;

class Poisson_Distributer
{
public:
	Poisson_Distributer(void);
	~Poisson_Distributer(void);

	void generate_poisson(float width, float height, float min_dist, int new_points_count);
	void reset();
	std::vector<Vector2>* getSamplePoints() { return &samplePoints; };

private:
	std::vector<Vector2> points;

	Vector2 generate_point_around(Vector2 point, float min_dist);
	Vector2 imageToGrid(Vector2 point, float cellSize);
	bool inNeighborhood(Vector2 point, float min_dist, float cellsize);	

	std::vector<Vector2> processList;
	std::vector<Vector2> samplePoints;

	Vector2** grid;
	int gridWidth;
	int gridHeight;
};

