#include "Input.h"


Input::Input(CoreInput* input)
{
	this->input = input;
}


Input::~Input(void)
{
}

void Input::update()
{
	if(input->getNumJoysticks() > 0)
	{
		leftStick = Vector2(input->getJoystickAxisValue(0,0), input->getJoystickAxisValue(0,1));
		rightStick = Vector2(input->getJoystickAxisValue(0,4), input->getJoystickAxisValue(0,3));

		if(leftStick.length() < 0.6f) leftStick = Vector2(0,0);
		if(rightStick.length() < 0.6f) rightStick = Vector2(0,0);

		bool rbLastState = rbIsDown;
		rbIsDown = input->getJoystickButtonState(0, 5);
		rbWasPressed = (!rbLastState && rbIsDown);

		bool lbLastState = lbIsDown;
		lbIsDown = input->getJoystickButtonState(0,4);
		lbWasPressed = (!lbLastState && lbIsDown);

		bool xState = input->getJoystickButtonState(0, 2);
		if(!xIsDown && xState) 
		{
			xWasPressed = true;
		}
		else xWasPressed = false;
		xIsDown = xState;

		bool pauseState = input->getJoystickButtonState(0, 7);
		pauseWasPressed = false;
		if (!pauseLastState && pauseState) pauseWasPressed = true;
		pauseLastState = pauseState;

		bool selectState = input->getJoystickButtonState(0, 6);
		selectWasPressed = false;
		if (!selectLastState && selectState) selectWasPressed = true;
		selectLastState = selectState;
	}
	else
	{
		leftStick = Vector2(0,0);
		rightStick = Vector2(0,0);
		rbIsDown = false;
		rbWasPressed = false;
		lbIsDown = false;
		lbWasPressed = false;
		xIsDown = false;
		xWasPressed = false;
		pauseWasPressed = false;
		selectWasPressed = false;
	}
}

P_UpdatePacket Input::createPInputPacket()
{
	P_UpdatePacket packet = P_UpdatePacket();

	packet.dashWasPressed = lbWasPressed;
	packet.equipmentActionState = rbIsDown;
	packet.equipmentActionWasPressed = rbWasPressed;
	packet.moveInput = -leftStick;
	packet.equipmentInput = -rightStick;
	packet.switchEquipmentPressed = xWasPressed;

	return packet;
}