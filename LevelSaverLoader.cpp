#include "LevelSaverLoader.h"

LevelSaverLoader::LevelSaverLoader(Player* player, VFXManager* vfxManager)
{
	this->player = player;
	this->vfxManager = vfxManager;

	loadLevelParts();
}

LevelSaverLoader::~LevelSaverLoader(void)
{

}

bool LevelSaverLoader::hasWorldBeenGenerated()
{
	std::ifstream in(worldWADFileName.c_str(), std::fstream::in | std::fstream::binary);
	bool worldWADExists = in.good();
	in.close();

	return worldWADExists;
}

void LevelSaverLoader::loadWorldWAD(WorldMap* worldMap, bool* done)
{
	std::ifstream in(worldWADFileName.c_str(), std::fstream::in | std::fstream::binary);
	///assert(in.good());

	in.read(reinterpret_cast<char*>(&worldWAD), sizeof(WorldMapWAD));
	in.close();

	worldMap->current = worldWAD.playerInIndex;
	for (int i = 0; i < worldMap->nodes.size(); i++)
	{
		delete worldMap->nodes.at(i);
	}
	worldMap->nodes.clear();
	worldMap->connections.clear();

	for (int i = 0; i < worldWAD.nodeCount; i++)
	{
		WorldMap::MapNode* node = new WorldMap::MapNode();
		worldMap->nodes.push_back(node);
		node->identifier;
		node->position = Vector2(worldWAD.mapNodes[i].x, worldWAD.mapNodes[i].z);
	}

	for (int i = 0; i < worldWAD.nodeCount; i++)
	{
		WorldMap::MapNode* node = worldMap->nodes.at(i);
		for (int j = 0; j < worldWAD.mapNodes[i].neighborCount; j++)
		{
			node->neighbors.push_back(worldMap->nodes.at(worldWAD.mapNodes[i].neighborsByIndex[j]));
		}
	}

	for (int i = 0; i < worldWAD.connectionCount; i++)
	{
		WorldMap::NodeConnection connection;
		connection.child = worldMap->nodes[worldWAD.connections[i].childIndex];
		connection.parent = worldMap->nodes[worldWAD.connections[i].parentIndex];
		worldMap->connections.push_back(connection);
	}

	this->map = worldMap;
}

void LevelSaverLoader::saveWorldWADtoFile()
{ 
	std::ofstream out(worldWADFileName.c_str(),std::fstream::out | std::fstream::binary);
	assert(out);
	out.write(reinterpret_cast<char*>(&worldWAD), sizeof(WorldMapWAD));
	out.close();
}

void LevelSaverLoader::levelStructsToLevel(LevelLayoutStruct* layout, LevelStateInfo* state, 
		LevelStateInfo* stateInfoArray, int arraySize, Level* allocatedSpace, bool* done)
{
	allocatedSpace->identifier = layout->identifier;

	String lightMapFilePath = "Data/WorldSave/OcclusionMaps/" + String::NumberToString(allocatedSpace->identifier, 0) 
		+ ".png";
	allocatedSpace->groundLightMap = new Image(lightMapFilePath);

	float planeSize = 100.0;
	SceneMesh* baseGround = new ScenePrimitive(ScenePrimitive::TYPE_PLANE, planeSize, planeSize);
	baseGround->setColor(ColorTable::green1);
	allocatedSpace->groundMesh = baseGround;
	allocatedSpace->groundMesh->alphaTest = true;
	allocatedSpace->groundMesh->depthWrite = true;
	allocatedSpace->groundMesh->setMaterialByName("SceneryShader");
	allocatedSpace->groundMesh->getLocalShaderOptions()
		->addParam(ProgramParam::PARAM_MATRIX, "transformMatrix")
		->setMatrix4(allocatedSpace->groundMesh->getConcatenatedMatrix());
	Quaternion q = allocatedSpace->groundMesh->getRotationQuat();
	allocatedSpace->groundMesh->getLocalShaderOptions()
		->addParam(ProgramParam::PARAM_COLOR, "rotationQuat")
		->setColor(Color(q.w, q.x, q.y, q.z));

	std::vector<Vector2> navmeshPoints;
	std::vector<Site*> levelMapSites;

	for(int i = 0; i < layout->siteCount; i++)
	{
		Site* baseSite = new Site(Vector2(layout->sites[i].x, layout->sites[i].y));
		levelMapSites.push_back(baseSite);
		LevelSite* site = new LevelSite(i, baseSite);
		baseSite->userData = (void*)site;
		allocatedSpace->levelSites.push_back(site);
		site->center = Vector2(layout->sites[i].x, layout->sites[i].y);

		for (int j = 0; j < layout->siteInfo[i].objCount; j++)
		{
			LevelLayoutStruct::SceneryObjInfo objInfo = layout->siteInfo[i].objsInSite[j];
			LevelPart part = allParts.at(objInfo.fromList)->at(objInfo.indexInList);

			if (objInfo.x == state->artifactPositionX && objInfo.z == state->artifactPositionZ) continue;

			site->addMesh(objInfo, part, false);
		}

		for (int j = 0; j < layout->siteInfo[i].colliderCount; j++)
		{
			LevelLayoutStruct::ColliderObjInfo colliderInfo = layout->siteInfo[i].collidersInSite[j];
			if (colliderInfo.positionX == state->artifactPositionX && 
				colliderInfo.positionZ == state->artifactPositionZ) continue;

			ScenePrimitive* collider = new ScenePrimitive(colliderInfo.colliderType,
				colliderInfo.value1, colliderInfo.value2, colliderInfo.value3);
			site->colliders[site->colliderCount] = collider;
			collider->setPositionY(collider->getHeight() / 2.0f);
			collider->setPositionX(colliderInfo.positionX);
			collider->setPositionZ(colliderInfo.positionZ);
			collider->setYaw(colliderInfo.angle);
			collider->visible = false;
			site->colliderCount++;
			navmeshPoints.push_back(Vector2(colliderInfo.positionX, colliderInfo.positionZ));
		}
	}

	if (state->artifactType != LevelStateInfo::ArtifactTypes::None &&
		state->artifactType != LevelStateInfo::ArtifactTypes::Start)
	{
		Artifact* artifact = NULL;
		Vector2 artifactPosition = Vector2(state->artifactPositionX, state->artifactPositionZ);
		if (state->artifactType == LevelStateInfo::ArtifactTypes::Item)
			artifact = new ItemPickup(artifactPosition, state);
		else if (state->artifactType == LevelStateInfo::ArtifactTypes::CurseCure)
			artifact = new CurseCurer(artifactPosition, state);
		else if (state->artifactType == LevelStateInfo::ArtifactTypes::Divination)
			artifact = new DivinationShrine(artifactPosition, state);
		else if (state->artifactType == LevelStateInfo::ArtifactTypes::Aura)
			artifact = new AuraShrine(artifactPosition, state);
		else if (state->artifactType == LevelStateInfo::ArtifactTypes::Pointer)
			artifact = new Pointer(artifactPosition, state);
		else if (state->artifactType == LevelStateInfo::ArtifactTypes::Torch)
			artifact = new Torch(artifactPosition, state);

		allocatedSpace->artifact = artifact;
		navmeshPoints.push_back(artifactPosition);
	}

	allocatedSpace->levelMap = new Triangulator(&levelMapSites);

	//set up neighbor lists in sites during load to make "scene graph mgmt" easy during actual run time
	for (int i = 0; i < allocatedSpace->levelSites.size(); i++)
	{
		const float Collider_Distance_Cutoff = 15.0f;
		const float Low_LOD_Cutoff = 23.0f;
		LevelSite* site = allocatedSpace->levelSites[i];
		Site* baseSite = site->baseSite;

		std::vector<Site*> colliderOutList;
		allocatedSpace->levelMap->floodFill(baseSite, Collider_Distance_Cutoff, &colliderOutList);
		std::vector<Site*> lowLODOutlist;
		allocatedSpace->levelMap->floodFill(baseSite, Low_LOD_Cutoff, &lowLODOutlist);

		site->low_LOD_Neighbor_List.count = lowLODOutlist.size();
		for (int j = 0; j < lowLODOutlist.size(); j++)
			site->low_LOD_Neighbor_List.indexes[j] = ((LevelSite*)lowLODOutlist[j]->userData)->index;

		site->colliders_Neighbor_List.count = colliderOutList.size();
		for (int j = 0; j < colliderOutList.size(); j++)
			site->colliders_Neighbor_List.indexes[j] = ((LevelSite*)colliderOutList[j]->userData)->index;
	}

	std::vector<Vector2> border;
	for (int i = 0; i < layout->borderPointCount; i++)
		border.push_back(Vector2(layout->borderPoints[i].x, layout->borderPoints[i].y));

	allocatedSpace->navMesh = NavMesh();
	allocatedSpace->navMesh.init(navmeshPoints, border);

	for(int i = 0; i < layout->connectedCount; i++)
	{
		ScenePrimitive* transistioner = new ScenePrimitive(ScenePrimitive::TYPE_BOX, layout->transitioners[i].xLength, 1.0f, 0.5f);
		transistioner->setPosition(layout->transitioners[i].x, 0.5f, layout->transitioners[i].z);
		transistioner->setYaw(layout->transitioners[i].angle);
		transistioner->visible = false;
		allocatedSpace->transitionColliders.push_back(transistioner);
		allocatedSpace->playerStarts.push_back(Vector2(layout->transitioners[i].playerX, layout->transitioners[i].playerZ));
		allocatedSpace->neighboringLevels.push_back(layout->transitioners[i].connectedTo);
	}

	*done = true;
}

void LevelSaverLoader::saveLevelLayoutToFile(int identifier, LevelLayoutStruct* saveInfo, bool* done)
{
	String fileName = levelLayoutPrefix + String::NumberToString(identifier, 0) + ".lvl";
	std::ofstream out(fileName.c_str(),std::fstream::out | std::fstream::binary);
	assert(out);
	out.write(reinterpret_cast<char*>(saveInfo), sizeof(LevelLayoutStruct));
	out.close();
	*done = true;
}

void LevelSaverLoader::loadLevelLayoutFromFile(int identifier, LevelLayoutStruct* loadInfo, bool* done)
{
	String fileName = levelLayoutPrefix + String::NumberToString(identifier, 0) + ".lvl";
	std::ifstream in(fileName.c_str(), std::fstream::in | std::fstream::binary);
	assert(in);
	in.read(reinterpret_cast<char*>(loadInfo), sizeof(LevelLayoutStruct));
	in.close();
	*done = true;
}

void LevelSaverLoader::saveLevelStateToFile(int identifier, LevelStateInfo* stateInfo, bool* done)
{
	String fileName = levelStatePrefix + String::NumberToString(identifier, 0) + ".lvlst";
	std::ofstream out(fileName.c_str(), std::fstream::out | std::fstream::binary);
	assert(out);
	out.write(reinterpret_cast<char*>(stateInfo), sizeof(LevelStateInfo));
	out.close();
}

void LevelSaverLoader::loadLevelStateFromFile(int identifier, LevelStateInfo* stateInfo, bool* done)
{
	String fileName = levelStatePrefix + String::NumberToString(identifier, 0) + ".lvlst";
	std::ifstream in(fileName.c_str(), std::fstream::in | std::fstream::binary);
	///assert(in.good());

	in.read(reinterpret_cast<char*>(stateInfo), sizeof(LevelStateInfo));
	in.close();
}

void LevelSaverLoader::loadLevelParts()
{
	LevelPart treePart = LevelPart("Data/WorldParts/Big/Tree1.mesh", ColorTable::brown3, 1.0f);
	mundaneParts.push_back(treePart);

	LevelPart wallPart1 = LevelPart("Data/WorldParts/Big/Wall2.mesh",
		ColorTable::lightGray_littlePurple, 1.0f);
	borderParts.push_back(wallPart1);

	LevelPart grassPatch = LevelPart("Data/WorldParts/Detail/Grass_Patch.mesh", ColorTable::green1, 0.1f);
	detailParts.push_back(grassPatch);

	allParts.push_back(&mundaneParts);
	allParts.push_back(&borderParts);
	allParts.push_back(&detailParts);
}

void LevelSaverLoader::updateThread()
{
	while (loadLevelRequests.size() != 0)
	{
		LoadLevelRequest request = loadLevelRequests.front();

		bool donePointer = false;
		LevelLayoutStruct* layout = new LevelLayoutStruct();
		loadLevelLayoutFromFile(request.levelID, layout, &donePointer);

		donePointer = false;
		levelStructsToLevel(layout, request.stateInfo, request.stateInfoArray, request.arraySize, 
			request.levelPointer, &donePointer);
		loadLevelRequests.erase(loadLevelRequests.begin());
		request.levelPointer->stateInfo = request.stateInfo;
		*request.donePointer = true;
		delete layout;
	}

	while (deleteLevelRequests.size() != 0)
	{
		DeleteLevelRequest request = deleteLevelRequests.front();

		bool donePointer = false;
		saveLevelStateToFile(request.levelPointer->identifier, request.state, &donePointer);
		delete request.levelPointer;

		deleteLevelRequests.erase(deleteLevelRequests.begin());
	}
}