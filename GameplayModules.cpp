#include "GameplayModules.h"

MovementInfo::MovementInfo(float maxSpeed, int velMemLength, int accelStepMax)
{
	accelPow = 0.8f;
	this->accelStepMax = accelStepMax;

	this->velocityMemoryLength = velMemLength;
	this->maxSpeed = maxSpeed;

	calculatedDirection = Vector3(0, 0, 1);
	calculatedSpeed = 0.0f;
	accelStepState = 0;
}

void MovementInfo::update(Vector2 inputDirection)
{
	float dot = 0.0f;
	if (velocityMemory.size() != 0)
		dot = velocityMemory.back().dot(Vector3(inputDirection.x, 0.0f, inputDirection.y));

	//Movement Acceleration Stuff
	const float directionInputDeadZone = 0.4f;
	if (accelStepState < accelStepMax && (inputDirection.length() >= directionInputDeadZone || dot >= 0.0f))
		accelStepState++;
	else if (accelStepState > 0 && (inputDirection.length() < directionInputDeadZone || dot <= -0.1f))
		accelStepState--;

	float normalizedAccel = ((float)accelStepState) / ((float)accelStepMax);
	calculatedSpeed = std::powf(normalizedAccel, accelPow) * maxSpeed;

	//Movement direction stuff
	Vector3 moveInput = Vector3(inputDirection.x, 0.0f, inputDirection.y);
	moveInput.Normalize();
	if (inputDirection.length() < directionInputDeadZone)
		moveInput = Vector3(0.0f);

	if (moveInput.length() == 0.0f && accelStepState == 0) velocityMemory.clear();

	velocityMemory.push_front(moveInput);
	if (velocityMemory.size() > velocityMemoryLength)
		velocityMemory.pop_back();
		
	Vector3 movementTotals = Vector3(0.0f);
	float totalTotal = 0.0f;
	for (int i = 0; i < velocityMemory.size(); i++)
	{
		float weight = ((float)(i + 1));
		movementTotals += velocityMemory.at(i) * weight;
		totalTotal += weight;
	}
	calculatedDirection = movementTotals / totalTotal;
}

FacingInfo::FacingInfo(float turnSpeed, int turnStepMax)
{
	turnSpeedPow = 1.5f;
	this->turnSpeed = turnSpeed;
	this->turnStepMax = turnStepMax;

	turnStepState = 0;	
}

void FacingInfo::update(Vector2 input, Vector2 current)
{
	Vector2 facingInput = input;
	facingInput.Normalize();

	const float minAngleToStartTurn = 3.1415926 / 16.0f;
	float angle = Vector2(current.x, current.y).angle(facingInput);
	float cross = Vector2(current.x, current.y).crossProduct(facingInput);
	if (cross > 0.0) cross = 1.0f;
	else cross = -1.0f;

	if (std::abs(angle) > minAngleToStartTurn)
	{
		if (std::abs(turnStepState) < turnStepMax) turnStepState += (int)cross;
	}
	else
	{
		turnStepState = 0;
	}

	float rotationSpeedNormalized = ((float)std::abs(turnStepState)) / ((float)turnStepMax);
	rotationSpeedNormalized = std::powf(rotationSpeedNormalized, turnSpeedPow);
	float rotationThisFrame = turnSpeed * rotationSpeedNormalized * cross;
	if (std::abs(rotationThisFrame) > std::abs((angle * (180.0f / 3.1415926))))
	{
		rotationThisFrame = angle;
		turnStepState = 0;
	}
	calculatedTurnSpeed = rotationThisFrame;
}