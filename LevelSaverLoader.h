#pragma once
#include <Polycode.h>
#ifndef LEVELSAVERLOADER
#define LEVELSAVERLOADER
#include "SaveStructs.h"
#include "Level.h"
#include "Artifact.h"
#include "LevelPartStruct.h"
#include "WorldMap.h"
#include <fstream>
#include <string>

using namespace Polycode;

class Player;
class VFXManager;

struct LoadLevelRequest
{
	int levelID;
	LevelStateInfo* stateInfo;
	LevelStateInfo* stateInfoArray;
	int arraySize;
	Level* levelPointer;
	bool* donePointer;

	LoadLevelRequest(bool* donePointer, int levelRequested, Level* levelPointer, 
		LevelStateInfo* stateInfo, LevelStateInfo* stateInfoArray, int arraySize)
	{
		this->donePointer = donePointer;
		this->levelID = levelRequested;
		this->levelPointer = levelPointer;
		this->stateInfo = stateInfo;
		this->stateInfoArray = stateInfoArray;
		this->arraySize = arraySize;
	}
};

struct DeleteLevelRequest
{
	Level* levelPointer;
	LevelStateInfo* state;

	DeleteLevelRequest(Level* levelPointer, LevelStateInfo* state)
	{
		this->levelPointer = levelPointer;
		this->state = state;
	}
};

class LevelSaverLoader : public Threaded
{
public:
	LevelSaverLoader(Player* player, VFXManager* vfxManager);
	~LevelSaverLoader(void);

	WorldMapWAD worldWAD;

	void updateThread();
	std::vector<LoadLevelRequest> loadLevelRequests;
	std::vector<DeleteLevelRequest> deleteLevelRequests;

	void saveLevelLayoutToFile(int indentifier, LevelLayoutStruct* saveInfo, bool* done);
	void loadLevelLayoutFromFile(int indentifier, LevelLayoutStruct* loadInfo, bool* done);
	void levelStructsToLevel(LevelLayoutStruct* layoutInfo, LevelStateInfo* stateInfo, 
		LevelStateInfo* stateInfoArray, int arraySize, Level* allocatedSpace, bool* done);

	void saveLevelStateToFile(int identifier, LevelStateInfo* stateInfo, bool* done);
	void loadLevelStateFromFile(int identifier, LevelStateInfo* stateInfo, bool* done);
	//void overwriteLevelStateFile(int identifier, LevelStateInfo* stateInfo, bool* done);

	bool hasWorldBeenGenerated();
	void loadWorldWAD(WorldMap* map, bool* done);
	void saveWorldWADtoFile();

	void loadLevelParts();

private:
	Player* player;
	VFXManager* vfxManager;
	WorldMap* map;

	const String worldWADFileName = "Data/WorldSave/World.WAD";
	const String levelLayoutPrefix = "Data/WorldSave/";
	const String levelStatePrefix = "Data/WorldSave/StateSave/";
	const String levelShadingImages = "Data/WorldSave/LightMaps/";
	
	std::vector<LevelPart> mundaneParts;
	std::vector<LevelPart> borderParts;
	std::vector<LevelPart> detailParts;
	std::vector<std::vector<LevelPart>*> allParts;
};
#endif