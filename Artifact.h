#pragma once
#ifndef ARTIFACT
#define ARTIFACT
#include <Polycode.h>
#include <Polycode3DPhysics.h>
#include "Player.h"
#include "VFXManager.h"
#include "ColorTable.h"
#include "WorldMap.h"
#include "SaveStructs.h"
#include "VFXManager.h"

using namespace Polycode;

class Artifact
{
public:
	Artifact(LevelStateInfo* state);
	~Artifact();

	virtual void update() = 0;
	virtual void loadin();
	virtual void unload();

	static void setGlobalVars(CollisionScene* scene, WorldMap* map, Player* player, VFXManager* vfxManager,
		LevelStateInfo* levelStatesArray, int statesArraySize);

	bool hasMenuView;
	String textInfo;

	std::vector<SceneMesh*> meshes;
	std::vector<ScenePrimitive*> colliders;

protected:
	static WorldMap* map;
	static VFXManager* vfxManager;
	static Player* player;
	static CollisionScene* scene;
	static LevelStateInfo* levelStatesArray;
	static int stateArrayLength;

	LevelStateInfo* state;

	void bindMeshesToSceneryShader();
};

class CurseCurer : public Artifact
{
public:
	CurseCurer(Vector2 centerPoint, LevelStateInfo* state);
	~CurseCurer();

	virtual void update();
};

class AuraShrine : public Artifact
{
public:
	AuraShrine(Vector2 centerPoint, LevelStateInfo* state);
	~AuraShrine();

	virtual void update();
};

class CompassAlignment : public Artifact
{
public:
	CompassAlignment(Vector2 centerPoint, LevelStateInfo* state);
	~CompassAlignment();

	virtual void update();
};

class Pointer : public Artifact
{
public:
	Pointer(Vector2 centerPoint, LevelStateInfo* state);
	~Pointer();

	virtual void update();
private:
	WorldMap* map;
	LevelStateInfo* stateInfoArray;
	VFXManager* vfxManager;
	int arraySize;
	float storedEmber;
};

class Ember : public Artifact
{
public:
	Ember(Vector2 centerPoint, LevelStateInfo* state);
	~Ember();

	virtual void update();
};

class DivinationShrine : public Artifact
{
public:
	DivinationShrine(Vector2 centerPoint, LevelStateInfo* state);
	~DivinationShrine();

	virtual void update();
};

class ItemPickup : public Artifact
{
public:
	ItemPickup(Vector2 centerPoint, LevelStateInfo* state);
	~ItemPickup();

	virtual void update();
};

class Torch : public Artifact
{
public:
	Torch(Vector2 centerPoint, LevelStateInfo* state);
	~Torch();

	virtual void update();
private:
	SceneMesh* tempMesh;
	float storedEmber;
};
#endif