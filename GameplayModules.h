#include <Polycode.h>
#ifndef GAMEPLAY_MODULES
#define GAMEPLAY_MODULES

using namespace Polycode;

struct Facing
{
	float x, z;

	Facing()
	{
		x = 0.0f;
		z = 1.0f;
	}

	void rotate(float angle)
	{
		float rads = angle * (3.1415926 / 180.0f);
		float sin = std::sinf(rads);
		float cos = std::cosf(rads);

		float newX = x * cos - z * sin;
		float newZ = x * sin + z * cos;
		x = newX;
		z = newZ;
	}

	float getYaw()
	{
		return Vector2(0, -1).angle(Vector2(x, z)) * (180.0f / 3.1415926f);
	}
};

class MovementInfo
{
public:
	Vector3 calculatedDirection;
	float calculatedSpeed;

	MovementInfo()
	{

	};

	MovementInfo(float maxSpeed, int velMemLength, int accelStepMax);
	void update(Vector2 inputDirection);

private:
	float accelPow;
	int accelStepMax;

	int accelStepState;
	float maxSpeed;

	int velocityMemoryLength;
	std::deque<Vector3> velocityMemory;
};

class FacingInfo
{
public:
	FacingInfo()
	{

	};
	FacingInfo(float turnSpeed, int turnStepMax);
	void update(Vector2 input, Vector2 current);

	float calculatedTurnSpeed;

private:
	float turnSpeedPow;
	float turnSpeed;
	int turnStepMax;
	int turnStepState;
};

#endif