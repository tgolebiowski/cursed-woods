#pragma once
#include <Polycode.h>
#include <Polycode3DPhysics.h>
#ifndef ENEMYMANAGER
#define ENEMYMANAGER
#include "Player.h"
#include "NPC.h"
#include "Level.h"
#include "VFXManager.h"
#include "DemonBoy.h"

using namespace Polycode;

class NPCManager
{
public:
	NPCManager(Player* player, VFXManager* vfxManager);
	~NPCManager(void);

	void update();

	CollisionScene* scene;
	Level* level;
	std::vector<NPC*> npcList;
	Player* player;

private:
	VFXManager* vfxManager;
};
#endif
