#pragma once
#include <Polycode.h>
#include <Polycode3DPhysics.h>
#ifndef NPC_H
#define NPC_H
#include "Player.h"
#include "GameplayModules.h"
#include "AI_Brain.h"

using namespace Polycode;

enum NPCTYPES
{
	DemonBae
};

class NPC
{
public:
	NPC(Player* player, PhysicsScene* scene, std::vector<NPC*>* npcList);
	~NPC(void);

	//Universal Enemy Components
	int type;
	CollisionEntity* collisionCollider;
	CollisionEntity* effectCollider;

	float getCurseVal();
	float getEmberVal();

	virtual void addToScene(Scene* scene, Vector3 startPosition) = 0;
	virtual void removeFromScene(Scene* scene) = 0;

	virtual void update();

	virtual void reactToHit(float damage) = 0;
	virtual void reactToWarm(float ember) = 0;

	void setCurrentTargetPoint(Vector3 newTarget) { this->currentTargetPoint = newTarget; }

	//Some basic AI stuff all enemies should have access to
	//regardless of if they use it or not
	bool isBeingThreatened();
	bool isBeingAttacked();
	bool isBeingWarmed();
	bool isEmberBeingPointed();
 
protected:
	Player* player;
	PhysicsScene* scene;
	std::vector<NPC*>* npcList;
	NPC_Brain* brainCore;

	void steer();
	void registerMeshWithShader(SceneMesh* mesh);
	void updateShaderLocals(SceneMesh* mesh);

	virtual void move() = 0;
	virtual void animate() = 0;

	//gameplay fields
	float curseLevel;
	float emberLevel;

	//General movement stuff
	MovementInfo moveInfo;
	FacingInfo facingInfo;
	Facing facing;
	Vector3 currentTargetPoint;
};
#endif
