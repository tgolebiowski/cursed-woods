#pragma once
#include <Polycode.h>
#include <Polycode3DPhysics.h>
#ifndef LEVEL
#define LEVEL
#include "Delaunay_Src\Triangulator.h"
#include "NavMesh.h"
#include "LevelPartStruct.h"
#include "Artifact.h"
#include "MeshBatch.h"
#include "SaveStructs.h"

using namespace Polycode;

struct LostWoods_Mesh
{
	VertexBuffer* buffer;
	Mesh* baseMesh;
	ShaderBinding* localShaderOptions;
	Matrix4 transformMatrix;
	Color color;
};

struct LevelSite
{
	struct NeighborGroupList
	{
		int count;
		int indexes[32];
	};

	int index;
	int highLODCount;
	int lowLODCount;
	int colliderCount;
	Vector2 center;
	ScenePrimitive* colliders [32];
	LostWoods_Mesh* low_LOD_Meshes;
	LostWoods_Mesh* high_LOD_Meshes;
	///NOTE: this has its own index at list.indexes[0];
	NeighborGroupList low_LOD_Neighbor_List;
	///NOTE: this has its own index at list.indexes[0];
	NeighborGroupList high_LOD_Neighbor_List;
	///NOTE: this has its own index at list.indexes[0];
	NeighborGroupList colliders_Neighbor_List;
	Site* baseSite;
	
	LevelSite(int index, Site* site)
	{
		this->baseSite = site;
		this->index = index;
		highLODCount = 0;
		lowLODCount = 0;
		colliderCount = 0;

		low_LOD_Meshes = (LostWoods_Mesh*)malloc(sizeof(LostWoods_Mesh) * low_LOD_Mesh_MaxCount);
		high_LOD_Meshes = (LostWoods_Mesh*)malloc(sizeof(LostWoods_Mesh) * high_LOD_Mesh_MaxCount);

		high_LOD_Neighbor_List.indexes[0] = index;
		low_LOD_Neighbor_List.indexes[0] = index;
		colliders_Neighbor_List.indexes[0] = index;
	};

	~LevelSite();

	void addMesh(LevelLayoutStruct::SceneryObjInfo info, LevelPart part, bool isHighLOD);

private:
		static const int low_LOD_Mesh_MaxCount = 16;
		static const int high_LOD_Mesh_MaxCount = 64;
};

struct Level : public Polycode::Entity
{
	Level();
	~Level(void);

	std::vector<ScenePrimitive*>* getTransitions() { return &transitionColliders; };
	std::vector<CollisionEntity*>* getColliders() { return &activeColliders; };

	void level_update(Vector3 focus);
	virtual void Render();
	void unload();
	void loadin(Vector3 startFocus);

	static CollisionScene* scene;

	///The level's scene graph manager
	Triangulator* levelMap;

	int identifier;

	static int lastLargeMeshRenderCount;
	Artifact* artifact;
	NavMesh navMesh;
	Image* groundLightMap;
	LevelStateInfo* stateInfo;
	SceneMesh* groundMesh;
	std::vector<LevelSite*> levelSites;
	std::vector<ScenePrimitive*> transitionColliders;

	std::vector<int> neighboringLevels;
	std::vector<Vector2> playerStarts;
private:
	std::vector<CollisionEntity*> activeColliders;
	LevelSite* lastCenterSite;
};
#endif