#pragma once
#include <Polycode.h>
#include "Delaunay_Src\Triangulator.h"
#include "micropather.h"
#ifndef NAVMESH_H
#define NAVMESH_H

using namespace Polycode;
using namespace micropather;

class NavMesh : public Graph
{
public:
	NavMesh(void);
	~NavMesh(void);

	void init(std::vector<Vector2> colliderPoints, std::vector<Vector2> borderPoints);
	void createPath(std::vector<Vector3>* outPath, Vector3 worldSpaceStart, Vector3 worldSpaceEnd);

	virtual float LeastCostEstimate( void* stateStart, void* stateEnd ); 
	virtual void AdjacentCost( void* state, MP_VECTOR< micropather::StateCost > *adjacent );
	virtual void PrintStateInfo( void* state );

	std::vector<SceneLine*> debugLines;
private:
	VoronoiGraph* map;
	Triangulator* siteFinder;
	MicroPather* pather;
};
#endif
