#include <Polycode.h>
#ifndef LEVELSAVEINFO
#define LEVELSAVEINFO

using namespace Polycode;

struct BasicLevelInfo
{
	int identifier;
	int neighborCount;
	char neighborsByIndex[8];
	float x, z;
};

struct WorldMapWAD
{
	struct LevelConnectionInfo
	{
		char parentIndex;
		char childIndex;
	};

	char playerInIndex, nodeCount, connectionCount;
	float playerX, playerZ;
	BasicLevelInfo mapNodes[50];
	LevelConnectionInfo connections[120];
};

struct PlayerSaveStruct
{
	char levelsRevealed[50];
	int levelIndex;
	int items[16];
	float ember;
	float curse;
	float positionX;
	float positionZ;
};

struct LevelStateInfo
{
	enum ArtifactTypes
	{
		Torch, Start, CurseCure, Item, Divination, Pointer, CompassAlignment, Aura, Ember, None
	};

	enum NPCTypes
	{
		Gold_Figure, MaskedMonkey, Harpy, Bat_Person, Hood_Horns, Masked_Tall
	};

	bool artifactFlag;
	int npcCount;
	int artifactType;
	int NPCTypes[16];
	float artifactSavedValue1;
	float artifactPositionX;
	float artifactPositionZ;
	float npcStartX[16];
	float npcStartZ[16];
	float npcCurseValue[16];

	LevelStateInfo()
	{
		npcCount = 0;
		artifactFlag = false;
		artifactType = ArtifactTypes::None;
		artifactPositionX = 0.0f;
		artifactPositionZ = 0.0f;
		artifactSavedValue1 = 0.0f;
	}
};

struct LevelLayoutStruct
{
	struct FloatPair
	{
		float x;
		float y;

		FloatPair()
		{
			x = 0.0f;
			y = 0.0f;
		};
	};

	struct SceneryObjInfo
	{	
		char fromList;
		char indexInList;
		bool useScaleXAsTarget;
		bool useScaleZAsTarget;
		float x;
		float z;
		float scaleX;
		float scaleZ;
		float angle;

		SceneryObjInfo()
		{
			useScaleXAsTarget = false;
			useScaleZAsTarget = false;
			scaleX = 1.0f;
			scaleZ = 1.0f;
		}
	};

	struct ColliderObjInfo
	{
		int colliderType;
		float angle;
		float positionX;
		float positionZ;
		float value1;
		float value2;
		float value3;
	};

	struct SiteInfo
	{
		int objCount;
		int colliderCount;
		SceneryObjInfo objsInSite[32];
		ColliderObjInfo collidersInSite[32];

		SiteInfo()
		{
			objCount = 0;
			colliderCount = 0;
		}
	};

	struct TransitionInfo
	{
		bool isLocked;
		int connectedTo;
		float xLength;
		float x;
		float z;
		float angle;
		float playerX;
		float playerZ;
	};

	int identifier;
	int siteCount;
	int connectedCount;
	int borderPointCount;
	FloatPair sites [300];
	FloatPair borderPoints[32];
	SiteInfo siteInfo [300];
	TransitionInfo transitioners[16];

	LevelLayoutStruct()
	{
		siteCount = 0;
		connectedCount = 0;
		borderPointCount = 0;
	}
};
#endif