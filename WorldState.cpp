#include "WorldState.h"

WorldState::WorldState(Vector3 thisP, Vector3 p, float thisEmber,float thisCurse, 
	float playerEmber, float playerCurse)
{
	position = thisP;
	playerPosition = p;
	this->thisEmber = thisEmber;
	this->thisCurse = thisCurse;
	this->playerCurse = playerCurse;
	this->playerEmber = playerEmber;
}

WorldState::WorldState()
{
	position = Vector3(0);
	playerPosition = Vector3(0);
	this->thisEmber = 0.0f;
	this->thisCurse = 0.0f;
	this->playerCurse = 0.0f;
	this->playerEmber = 0.0f;
}

float WorldState::getDiff(WorldState otherState)
{
	return (this->position - otherState.position).length();
}