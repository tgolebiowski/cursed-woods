#pragma once
#include <Polycode.h>
#ifndef AI_ACTIONS_H
#define AI_ACTIONS_H
#include "WorldState.h"
#include "NPC.h"
#include "Player.h"
#include "NavMesh.h"

using namespace Polycode;
class Action
{
public:
	Action();
	~Action();

	bool doneFlag;
	virtual void interrupt() = 0;
	virtual bool hasPrereq(WorldState* state) = 0;
	///Return cost of doing the action and set outState to what the
	virtual float describeChangeInWorldState(WorldState* outState, WorldState* targetState) = 0;
	virtual void run(WorldState* current, WorldState* target) = 0;
};

class PathTo : public Action
{
public:
	PathTo(NPC* npc, NavMesh* navMesh, float changeWayPointDist);
	~PathTo();

	virtual void interrupt() { path.clear(); }
	virtual bool hasPrereq(WorldState* state) { return true; }
	virtual float describeChangeInWorldState(WorldState* outState, WorldState* targetState);
	virtual void run(WorldState* current, WorldState* target);

private:
	String moveAnimationName;
	NPC* npc;
	NavMesh* navMesh;
	float changeWayPointDist;

	std::vector<Vector3> path;
	void createPath(Vector3 npcStart, Vector3 targetPOS);
};
#endif