#include "GameCam.h"


GameCam::GameCam(Scene* scene, Vector3 viewingVec, Vector2 screenSize)
{
	this->camera = scene->getDefaultCamera();
	const float factor = 0.01 * 2.0;
	this->camera->setOrthoSize(screenSize.x * factor, screenSize.y * factor);
	this->camera->setOrthoMode(true);

	this->viewingVec = viewingVec;
}


GameCam::~GameCam(void)
{
}

void GameCam::update(Vector3 focus)
{
	camera->setPosition(focus + viewingVec);
	camera->lookAt(focus);
}