#include "WorldGeneration.h"


WorldGeneration::WorldGeneration()
{

}


WorldGeneration::~WorldGeneration()
{
}

void WorldGeneration::generateLevel(LevelGenInfo genInfo, LevelLayoutStruct* levelInfo, LevelStateInfo* stateInfo)
{
	levelInfo->identifier = genInfo.identifier;

	Poisson_Distributer meshPositionDist = Poisson_Distributer();
	meshPositionDist.generate_poisson(100.0, 100.0, genInfo.objectDensity, 15);
	Vector2 offset = Vector2(100.0 / 2.0, 100.0 / 2.0);
	std::vector<Vector2>* meshPositions = meshPositionDist.getSamplePoints();
	for (int i = 0; i < meshPositions->size(); i++)
		meshPositions->at(i) = meshPositions->at(i) - offset;

	Poisson_Distributer organizationSiteDist = Poisson_Distributer();
	organizationSiteDist.generate_poisson(150.0, 150.0, 8.0f, 15);
	offset = Vector2(150.0 / 2.0, 150.0 / 2.0);

	std::vector<Site*> siteList;
	for (int i = 0; i < organizationSiteDist.getSamplePoints()->size(); i++)
	{
		Vector2 p = organizationSiteDist.getSamplePoints()->at(i) - offset;
		//Vector2 p = organizationSiteDist.getSamplePoints()->at(i);
		siteList.push_back(new Site(p));
	}

	Triangulator scenegraphSiteMap = Triangulator(&siteList);
	siteList.clear();

	int defaultObjType = 0;

	bool skipArtifact = genInfo.gameObj_type == "NONE" ||
		genInfo.gameObj_type == "TORCH" ||
		genInfo.gameObj_type == "START";
	int skipIndex = -1;
	float shortestDistanceToBestSpot = 10000.0f;

	if (skipArtifact) goto skippingArtifactInsert;

	for (int i = 0; i < meshPositions->size(); i++)
	{
		if (meshPositions->at(i).length() < shortestDistanceToBestSpot)
		{
			shortestDistanceToBestSpot = meshPositions->at(i).length();
			skipIndex = i;
		}
	}

skippingArtifactInsert:

	stateInfo->artifactSavedValue1 = 0.0f;
	stateInfo->npcCount = 0;

	if (skipIndex != -1) {
		Vector2 loc = meshPositions->at(skipIndex);
		int objType;
		if (genInfo.gameObj_type == "Pickup Shrine")
			objType = LevelStateInfo::ArtifactTypes::Item;
		else if (genInfo.gameObj_type == "Curse Curer")
			objType = LevelStateInfo::ArtifactTypes::CurseCure;
		else if (genInfo.gameObj_type == "Divination Shrine")
			objType = LevelStateInfo::ArtifactTypes::Divination;
		else if (genInfo.gameObj_type == "Aura Shrine")
			objType = LevelStateInfo::ArtifactTypes::Aura;
		else if (genInfo.gameObj_type == "Pointer")
			objType = LevelStateInfo::ArtifactTypes::Pointer;
		else if (genInfo.gameObj_type == "NONE")
			objType = LevelStateInfo::ArtifactTypes::None;
		else if (genInfo.gameObj_type == "START")
			objType = LevelStateInfo::ArtifactTypes::Torch;
		else if (genInfo.gameObj_type == "TORCH")
			objType = LevelStateInfo::ArtifactTypes::Start;

		stateInfo->artifactPositionX = loc.x;
		stateInfo->artifactPositionZ = loc.y;
		stateInfo->artifactType = objType;
		stateInfo->artifactFlag = false;
	}
   

	for (int i = 0; i < meshPositions->size(); i++)
	{
		if (i == skipIndex) continue;

		LevelLayoutStruct::SceneryObjInfo objInfo = LevelLayoutStruct::SceneryObjInfo();
		objInfo.x = meshPositions->at(i).x;
		objInfo.z = meshPositions->at(i).y;
		objInfo.scaleX = 1.0f;
		objInfo.scaleZ = 1.0f;
		objInfo.angle = rand() % 360;
		objInfo.indexInList = defaultObjType;
		objInfo.fromList = 0;
		LevelLayoutStruct::ColliderObjInfo colliderInfo = LevelLayoutStruct::ColliderObjInfo();
		colliderInfo.positionX = meshPositions->at(i).x;
		colliderInfo.positionZ = meshPositions->at(i).y;
		colliderInfo.angle = objInfo.angle;
		colliderInfo.colliderType = ScenePrimitive::TYPE_CYLINDER;
		colliderInfo.value1 = 1.0f;
		colliderInfo.value2 = 0.75f;
		colliderInfo.value3 = 7.0f;

		Site* nearestSite = scenegraphSiteMap.getNearestSite(objInfo.x, objInfo.z);
		if (nearestSite->userData == NULL)
		{
			levelInfo->siteInfo[levelInfo->siteCount] = LevelLayoutStruct::SiteInfo();
			levelInfo->sites[levelInfo->siteCount].x = nearestSite->p.x;
			levelInfo->sites[levelInfo->siteCount].y = nearestSite->p.y;
			nearestSite->userData = (void*)&levelInfo->siteInfo[levelInfo->siteCount];
			levelInfo->siteCount++;
		}
		LevelLayoutStruct::SiteInfo* infoTarget = (LevelLayoutStruct::SiteInfo*)nearestSite->userData;

		infoTarget->objsInSite[infoTarget->objCount] = objInfo;
		infoTarget->objCount++;
		infoTarget->collidersInSite[infoTarget->colliderCount] = colliderInfo;
		infoTarget->colliderCount++;
	}

	std::vector<Vector2> border;
	for (int i = 0; i < genInfo.borderPoints.size(); i++)
	{
		border.push_back(genInfo.borderPoints.at(i));
		levelInfo->borderPoints[i].x = genInfo.borderPoints.at(i).x;
		levelInfo->borderPoints[i].y = genInfo.borderPoints.at(i).y;
	}
	levelInfo->borderPointCount = border.size();

	struct IntPair
	{
		int int1;
		int int2;
	};

	std::vector<IntPair> transitionBorders;
	levelInfo->connectedCount = genInfo.transitionDirections.size();
	for(int i = 0; i < genInfo.transitionDirections.size(); i++)
	{
		Vector2 transitionInsertDirection = genInfo.transitionDirections.at(i);
		int index1 = -1;
		int index2 = -1;
		for(int j = 0; j < border.size(); j++)
		{
			index1 = j;
			if(j == border.size() - 1) index2 = 0; else index2 = j + 1;

			Vector2 border1 = border.at(index1);
			Vector2 border2 = border.at(index2);
			float angle1 = transitionInsertDirection.angle(border1);
			float angle2 = transitionInsertDirection.angle(border2);

			float dot1 = transitionInsertDirection.dot(border1);
			float dot2 = transitionInsertDirection.dot(border2);
		
			if(dot1 > 0.0f && dot2 > 0.0f)
			{
				if((angle1 > 0.0 && angle2 < 0.0f) || (angle1 < 0.0f && angle2 > 0.0f))
				{
					//WINNER!
					Vector2 p = Vector2((border1.x + border2.x)/2.0f, (border1.y + border2.y)/2.0f);
					Vector2 borderVec = border2 - border1;
					borderVec.Normalize();
					float xLength = (border1 - border2).length();
					float baseAngle = borderVec.angle(Vector2(1,0));
					float angle = (baseAngle * (180.0f/3.141592653f));
					
					Vector2 v = border2 - border1;
					v.Normalize();
					v = Vector2(-v.y * 5.0f, v.x * 5.0f);
					Vector2 playerStart = p + v;

					IntPair indexPair = IntPair();
					indexPair.int1 = index1;
					indexPair.int2 = index2;
					transitionBorders.push_back(indexPair);

					LevelLayoutStruct::TransitionInfo transistInfo = LevelLayoutStruct::TransitionInfo();
					transistInfo.x = p.x;
					transistInfo.z = p.y;
					transistInfo.xLength = xLength;
					transistInfo.angle = angle;
					transistInfo.connectedTo = genInfo.transitionTo.at(i);
					transistInfo.playerX = playerStart.x;
					transistInfo.playerZ = playerStart.y;
					levelInfo->transitioners[i] = transistInfo;

					break;
				}
			}
		}
	}


	int index1 = -1;
	int index2 = -1;
	for(int i = 0; i < border.size(); i++)
	{
		index1 = i;
		if(i == border.size() - 1) index2 = 0; else index2 = i + 1;

		///Quik check if this section of border is for a transition area
		bool skipFlag = false;
		for(int j = 0; j < transitionBorders.size(); j++)
		{
			IntPair pair = transitionBorders.at(j);
			if(index1 == pair.int1 && index2 == pair.int2) { skipFlag = true; break; }
		}
		if(skipFlag) continue;
		///End transition checking

		Vector2 border1 = border.at(index1);
		Vector2 border2 = border.at(index2);

		//Border collider adding
		Vector2 p = Vector2((border1.x + border2.x) / 2.0f, (border1.y + border2.y) / 2.0f);
		Vector2 borderVec = border2 - border1;
		borderVec.Normalize();
		float xLength = (border1 - border2).length();
		float baseAngle = borderVec.angle(Vector2(1, 0));
		float angle = (baseAngle * (180.0f / 3.141592653f));

		Site* nearestSite = NULL;
		nearestSite = scenegraphSiteMap.getNearestSite(p.x, p.y);
		if (nearestSite->userData == NULL)
		{
			levelInfo->siteInfo[levelInfo->siteCount] = LevelLayoutStruct::SiteInfo();
			nearestSite->userData = (void*)&levelInfo->siteInfo[levelInfo->siteCount];
			levelInfo->sites[levelInfo->siteCount].x = nearestSite->p.x;
			levelInfo->sites[levelInfo->siteCount].y = nearestSite->p.y;
			levelInfo->siteCount++;
		}
		LevelLayoutStruct::SiteInfo* infoTarget = (LevelLayoutStruct::SiteInfo*)nearestSite->userData;

		LevelLayoutStruct::ColliderObjInfo colliderInfo = LevelLayoutStruct::ColliderObjInfo();
		colliderInfo.colliderType = ScenePrimitive::TYPE_BOX;
		colliderInfo.angle = angle;
		colliderInfo.positionX = p.x;
		colliderInfo.positionZ = p.y;
		colliderInfo.value1 = xLength;
		colliderInfo.value2 = 1.5f;
		colliderInfo.value3 = 0.75f;
		infoTarget->collidersInSite[infoTarget->colliderCount] = colliderInfo;
		infoTarget->colliderCount++;

		LevelLayoutStruct::SceneryObjInfo objInfo = LevelLayoutStruct::SceneryObjInfo();
		objInfo.fromList = 1;
		objInfo.indexInList = 0;
		objInfo.angle = angle;
		objInfo.useScaleXAsTarget = true;
		objInfo.useScaleZAsTarget = false;
		objInfo.scaleZ = 1.0f;
		objInfo.scaleX = xLength;
		objInfo.x = p.x;
		objInfo.z = p.y;
		infoTarget->objsInSite[infoTarget->objCount] = objInfo;
		infoTarget->objCount++;

	}

	//high LOD stuff
	//newdist.getSamplePoints()->clear();
	//newdist.generate_poisson(100.0, 100.0, 1.0f, 15);

	//for (int i = 0; i < newdist.getSamplePoints()->size(); i++)
	//{
	//	Vector2 p = newdist.getSamplePoints()->at(i) - Vector2(100.0 / 2.0, 100.0 / 2.0);

	//	LevelLayoutStruct::SceneryObjInfo objInfo;
	//	objInfo.fromList = 2;
	//	objInfo.indexInList = 0;
	//	objInfo.angle = ((float)(rand() % 360));
	//	objInfo.scaleX = 1.0f;
	//	objInfo.scaleZ = 1.0f;
	//	objInfo.x = p.x;
	//	objInfo.z = p.y;

	//	Site* nearestSite = siteMap.getNearestSite(objInfo.x, objInfo.z);
	//	if (nearestSite->userData == NULL)
	//	{
	//		levelInfo->siteInfo[levelInfo->siteCount] = LevelLayoutStruct::SiteInfo();
	//		levelInfo->sites[levelInfo->siteCount].x = nearestSite->p.x;
	//		levelInfo->sites[levelInfo->siteCount].y = nearestSite->p.y;
	//		nearestSite->userData = (void*)&levelInfo->siteInfo[levelInfo->siteCount];
	//		levelInfo->siteCount++;
	//	}
	//	LevelLayoutStruct::SiteInfo* infoTarget = (LevelLayoutStruct::SiteInfo*)nearestSite->userData;

	//	infoTarget->objsInSite[infoTarget->objCount] = objInfo;
	//	infoTarget->objCount++;
	//}
}

void WorldGeneration::generateBigDebugWorld(WorldMap* map, LevelSaverLoader* loader)
{
	std::vector<LevelGenInfo> levelGenInfoList;
	generateWorldMap(map, &levelGenInfoList);

	loader->worldWAD.nodeCount = map->nodes.size();

	for (int i = 0; i < map->nodes.size(); i++)
	{
		LevelGenInfo info = levelGenInfoList.at(i);
		info.identifier = map->nodes.at(i)->identifier;

		loader->worldWAD.mapNodes[i].identifier = info.identifier;
		loader->worldWAD.mapNodes[i].x = map->nodes.at(i)->position.x;
		loader->worldWAD.mapNodes[i].z = map->nodes.at(i)->position.y;
		loader->worldWAD.mapNodes[i].neighborCount = info.transitionTo.size();

		for (int j = 0; j < info.transitionTo.size(); j++)
		{
			loader->worldWAD.mapNodes[i].neighborsByIndex[j] = info.transitionTo.at(j);
		}

		for (int j = 0; j < map->nodes.at(i)->neighbors.size(); j++)
		{
			WorldMap::MapNode* neighbor = map->nodes.at(i)->neighbors[j];
			Vector2 neighborDirection = map->nodes.at(i)->neighbors[j]->position - map->nodes.at(i)->position;
			neighborDirection.Normalize();
		}

		LevelLayoutStruct* layoutInfo = new LevelLayoutStruct();
		LevelStateInfo* stateInfo = new LevelStateInfo();
		generateLevel(info, layoutInfo, stateInfo);

		bool done = false;
		loader->saveLevelLayoutToFile(info.identifier, layoutInfo, &done);
		loader->saveLevelStateToFile(info.identifier, stateInfo, &done);

		LightShaderHub::getInstance()->createStaticOcclusionMap(layoutInfo, stateInfo, 100.0f);
		delete layoutInfo;
		delete stateInfo;
	}

	loader->worldWAD.connectionCount = map->connections.size();
	for (int i = 0; i < map->connections.size(); i++)
	{
		loader->worldWAD.connections[i].childIndex = map->connections.at(i).child->identifier;
		loader->worldWAD.connections[i].parentIndex = map->connections.at(i).parent->identifier;
	}

	loader->worldWAD.playerInIndex = 0;
	loader->worldWAD.playerX = 0.0f;
	loader->worldWAD.playerZ = 0.0f;

	loader->saveWorldWADtoFile();
}

void WorldGeneration::generateWorldMap(WorldMap* map, std::vector<LevelGenInfo>* levelGenInfoList)
{
	//std::vector<WorldMap::MapNode*> leaves;

	//roughLayout(map, &leaves);
	roughLayout(map);
	//doLeafConnections(map, &leaves);
	generateGenerationInfo(map, levelGenInfoList);

	delete triangulator;
}

void WorldGeneration::roughLayout(WorldMap* map)
{
	Poisson_Distributer dist = Poisson_Distributer();
	dist.generate_poisson(500.0f, 500.0f, 60.0f, 15);
	std::vector<Vector2> points;
	Vector2 offset = Vector2(500.0f / 2.0f, 500.0f / 2.0f);

	std::vector<Site*> siteList;
	for (int i = 0; i < dist.getSamplePoints()->size(); i++)
	{
		if ((dist.getSamplePoints()->at(i) - offset).length() > 250.0f) continue;
		else siteList.push_back(new Site(dist.getSamplePoints()->at(i) - offset));
	}

	for (int i = 0; i < 26; i++)
	{
		float angle = ((float)i) * ((3.14159 * 2.0) / 26.0);
		float x = std::cos(angle) * 300.0f;
		float y = std::sin(angle) * 300.0f;
		siteList.push_back(new Site(Vector2(x, y)));
	}

	triangulator = new Triangulator(&siteList);

	for (int i = 0; i < triangulator->sites.size(); i++)
	{
		Site* site = triangulator->sites[i];
		if (site->p.length() >= 250.0f) continue;

		WorldMap::MapNode* node = NULL;
		if (site->userData == NULL)	node = map->addNode(site);
		else node = (WorldMap::MapNode*)site->userData;

		for (int j = 0; j < site->neighbors.size(); j++)
		{
			Site* neighbor = site->neighbors.at(j);
			//if its outside the border circle
			if (neighbor->p.length() > 250.0f) continue;

			WorldMap::MapNode* n_node = NULL;
			if (neighbor->userData == NULL) n_node = map->addNode(neighbor);
			else n_node = (WorldMap::MapNode*)neighbor->userData;

			//if n_node isn't already a neighbor
			if (std::find(node->neighbors.begin(), node->neighbors.end(), n_node) != node->neighbors.end())
				continue;

			//relative neighbor constraint, no shared neighbor that is closer to each node than they
			//are to each other
			float distance = (site->p - neighbor->p).length();
			bool closerNeighborExists = false;
			for (int k = 0; k < site->neighbors.size(); k++)
			{
				if (std::find(neighbor->neighbors.begin(), neighbor->neighbors.end(),
					site->neighbors.at(k)) != neighbor->neighbors.end())
				{
					float siteToOther = (site->p - site->neighbors.at(k)->p).length();
					float neighborToOther = (neighbor->p - site->neighbors.at(k)->p).length();
					if (siteToOther < distance && neighborToOther < distance)
					{
						closerNeighborExists = true;
						break;
					}
				}
			}

			if (!closerNeighborExists && node != n_node) map->makeConnection(node, n_node);
		}
	}

	for (int i = 0; i < map->connections.size(); i++)
	{
		WorldMap::NodeConnection connection = map->connections.at(i);
		if (connection.child->neighbors.size() >= 3 && connection.parent->neighbors.size() > 3)
		{
			WorldMap::MapNode* child = connection.child;
			WorldMap::MapNode* parent = connection.parent;

			for (int j = 0; j < child->neighbors.size(); j++)
			{
				if (child->neighbors.at(j) == parent)
				{
					child->neighbors.erase(child->neighbors.begin() + j);
					break;
				}
			}

			for (int j = 0; j < parent->neighbors.size(); j++)
			{
				if (parent->neighbors.at(j) == child)
				{
					parent->neighbors.erase(parent->neighbors.begin() + j);
					break;
				}
			}

			map->connections.erase(map->connections.begin() + i);
			i--;
		}
	}
}

void WorldGeneration::generateGenerationInfo(WorldMap* map, std::vector<LevelGenInfo>* levelGenInfoList)
{
	vector<Polycode::String> npcNames =
	{
		String("Masked Monkey"),
		String("Harpy"),
		String("Masked Warrior"),
		String("Hood and Horns"),
		String("Bat Demon"),
		String("Gold figure"),
		String("NONE")
	};

	int farthestSouthIndex = -1;
	float southCutoff = 10000.0f;
	for (int i = 0; i < map->nodes.size(); i++)
	{
		WorldMap::MapNode* node = map->nodes.at(i);
		LevelGenInfo info = LevelGenInfo();
		
		info.identifier = i;
		info.averageCurse = 0.5f;
		info.npc_type = npcNames.at(rand() % npcNames.size());

		//border stuff
		std::vector<Vector2> unorganizedBorder;
		for (int j = 0; j < node->baseSite->incidentTris.size(); j++)
		{
			Vector2 borderPoint = Circle(node->baseSite->incidentTris[j]).center - node->position;
			unorganizedBorder.push_back(borderPoint);
			if (unorganizedBorder.back().length() > 45.0f)
			{
				float length = unorganizedBorder.back().length();
				unorganizedBorder.back().x *= 45.0f / length;
				unorganizedBorder.back().y *= 45.0f / length;
			}
		}
		do
		{
			Vector2 compare = Vector2(1, 0);
			int bestIndex = -1;
			float bestAngle = 2.0 * 3.14159;

			for (int j = 0; j < unorganizedBorder.size(); j++)
			{
				Vector2 compare2 = unorganizedBorder.at(j);
				compare2.Normalize();

				float angle = compare.angle(compare2);
				if (angle < 0.0) angle += (2.0 * 3.14159);

				if (angle < bestAngle)
				{
					bestIndex = j;
					bestAngle = angle;
				}
			}

			info.borderPoints.push_back(unorganizedBorder.at(bestIndex));
			unorganizedBorder.erase(unorganizedBorder.begin() + bestIndex);
		} while (unorganizedBorder.size() != 0);

		//divide borders so they are less than max length
		float borderLengthCutoff = 20.0f;
		for (int j = 0; j < info.borderPoints.size(); j++)
		{
			int index1 = j;
			int index2 = j + 1;
			if (j == info.borderPoints.size() - 1) index2 = 0;

			float distance = (info.borderPoints.at(index1) - info.borderPoints.at(index2)).length();

			if (distance < borderLengthCutoff) continue;

			int newSegments = distance / borderLengthCutoff;
			newSegments += 1;
			Vector2 b1Tob2 = info.borderPoints.at(index2) - info.borderPoints.at(index1);
			b1Tob2.Normalize();
			float segmentsLength = distance / ((float)newSegments);
			for (int k = 1; k < newSegments; k++)
			{
				Vector2 newBorderPoint = info.borderPoints.at(index1) + 
					(b1Tob2 * ((float)(k)) * segmentsLength);

				info.borderPoints.insert(info.borderPoints.begin() + index1 + k, newBorderPoint);
			}
		}

		if (node->position.y < southCutoff)
		{
			southCutoff = node->position.y;
			farthestSouthIndex = i;
		}
		levelGenInfoList->push_back(info);
	}


	levelGenInfoList->at(farthestSouthIndex).gameObj_type = "START";
	levelGenInfoList->at(farthestSouthIndex).npc_type = "NONE";

	for (int i = 0; i < map->nodes.size(); i++)
	{
		for (int j = 0; j < map->nodes.at(i)->neighbors.size(); j++)
		{
			Vector2 direction = map->nodes.at(i)->neighbors[j]->position - map->nodes.at(i)->position;
			direction.Normalize();
			direction.x *= -1.0f;
			direction.y *= -1.0f;
			levelGenInfoList->at(i).transitionDirections.push_back(direction);
			levelGenInfoList->at(i).transitionTo.push_back(map->nodes.at(i)->neighbors[j]->identifier);
		}

		//not doing locked doors right now
		//3 Conditions for adding a locked door
		//1. must have 3 neighbors
		//2. must not already have a locked door
		//3. neighbor must have 3 neighbors
		if (map->nodes.at(i)->neighbors.size() != 3) continue;

		/*bool alreadyHasLockedDoor = false;
		for (int j = 0; j < 3; j++)
		{
			if (map->nodes.at(i)->lockedDoors[j] != -1)
			{
				alreadyHasLockedDoor = true;
				break;
			}
		}
		if (alreadyHasLockedDoor)continue;*/

		//WorldMap::MapNode* validNeighbor = NULL;
		//for (int j = 0; j < 3; j++)
		//{
		//	if (map->nodes.at(i)->neighbors[j] != NULL)
		//	{
		//		if (map->nodes.at(i)->neighbors[j]->neighbors.size() == 3)
		//		{
		//			validNeighbor = map->nodes.at(i)->neighbors[j];
		//		}
		//	}
		//}
		//if (validNeighbor == NULL) continue;

		////if conditions met, 50% chance for adding a door
		//int rando = rand() % 100;
		//if (rando > 50) continue;

		//int startToN_index = -1;
		//int nToStart_index = -1;
		//for (int j = 0; j < 3; j++)
		//{
		//	if (map->nodes.at(i)->neighbors[j] == validNeighbor) startToN_index = j;
		//	if (validNeighbor->neighbors[j] == map->nodes.at(i)) nToStart_index = j;
		//}

		//int neighborIndex = -1;
		//for (int j = 0; j < map->nodes.size(); j++)
		//{
		//	if (map->nodes.at(j) == validNeighbor) neighborIndex = j;
		//}

		//map->nodes.at(i)->lockedDoors[startToN_index] = neighborIndex;
		//validNeighbor->lockedDoors[nToStart_index] = i;
	}

	struct ContextFreeArtifactInfo
	{
		String name;
		int neighborSearchLength;
		float percentageOfTotalLevels;
		
		ContextFreeArtifactInfo(String name, int searchLength, float percent)
		{
			this->name = name;
			this->neighborSearchLength = searchLength;
			this->percentageOfTotalLevels = percent;
		}

		bool doSearch(WorldMap::MapNode* startNode, std::vector<LevelGenInfo>* genInfoList)
		{
			for (int i = 0; i < startNode->neighbors.size(); i++)
			{
				if (recursiveSearch(startNode, startNode->neighbors.at(i), 
					neighborSearchLength, 0, name, genInfoList)) 
					return false;
			}
			return true;
		}
		
		bool recursiveSearch(WorldMap::MapNode* parentNode, WorldMap::MapNode* calledNode, 
			int maxCheck, int increment, String searchItem, std::vector<LevelGenInfo>* genInfoList)
		{
			if (maxCheck <= increment) return false;
			
			if (genInfoList->at(calledNode->identifier).gameObj_type == searchItem)
			{
				return true;
			}
			
			bool foundInChildren = false;
			for (int i = 0; i < calledNode->neighbors.size(); i++)
			{
				if (calledNode->neighbors.at(i) == parentNode) continue;
				
				bool searchReturn = recursiveSearch(calledNode, calledNode->neighbors.at(i), 
					maxCheck, increment + 1, searchItem, genInfoList);
				if (searchReturn) { foundInChildren = true; break; }
			}
			
			return foundInChildren;
		}
	};
	
	std::vector<ContextFreeArtifactInfo> artifactInfo;
	artifactInfo.push_back(ContextFreeArtifactInfo("Pickup Shrine", 1, 0.15));
	artifactInfo.push_back(ContextFreeArtifactInfo("Pointer", 2, 0.15));
	artifactInfo.push_back(ContextFreeArtifactInfo("Curse Curer", 2, 0.15));
	artifactInfo.push_back(ContextFreeArtifactInfo("Divination Shrine", 3, 0.05));
	artifactInfo.push_back(ContextFreeArtifactInfo("Aura Shrine", 3, 0.05));
	//artifactInfo.push_back(ContextFreeArtifactInfo("NONE", 0, 0.45));

	//Add artifacts to levels
	for (int i = 0; i < artifactInfo.size(); i++)
	{
		int artifactCount = map->nodes.size() * artifactInfo.at(i).percentageOfTotalLevels;
		
		for (int j = 0; j < artifactCount; j++)
		{
			bool validSiteFound = false;
			do{
				int randSiteIndex = rand() % map->nodes.size();
				if (levelGenInfoList->at(randSiteIndex).gameObj_type != "NONE") continue;
				
				bool validSite = artifactInfo.at(i).doSearch(map->nodes.at(randSiteIndex), levelGenInfoList);
				if (validSite)
				{
					levelGenInfoList->at(randSiteIndex).gameObj_type = artifactInfo.at(i).name;
					validSiteFound = true;
				}
			} while (!validSiteFound);
		}
	}

}

Level* WorldGeneration::generateVisualTestLevel(LevelSaverLoader* saverloader)
{
	Level* returnLevel = new Level();

	WorldMap testMap = WorldMap();
	std::vector<LevelGenInfo> levelGenInfoList;
	generateWorldMap(&testMap, &levelGenInfoList);

	LevelGenInfo info = levelGenInfoList.at(0);
	info.identifier = testMap.nodes.at(0)->identifier;

	LevelLayoutStruct* layout = new LevelLayoutStruct();
	LevelStateInfo* state = new LevelStateInfo();
	generateLevel(info, layout, state);

	LightShaderHub::getInstance()->createStaticOcclusionMap(layout, state, 100.0f);

	bool donePointer = false;
	saverloader->levelStructsToLevel(layout, state, NULL, 0, returnLevel, &donePointer);

	delete layout;
	delete state;

	return returnLevel;
}

