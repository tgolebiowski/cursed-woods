#include "PolycodeView.h"
#include "Polycode.h"
#include <Polycode3DPhysics.h>
#include "Input.h"
#include "GuiManager.h"
#include "GameLogicHub.h"
#include "DebugTools.h"
#include "ColorTable.h"

using namespace Polycode;

class LostWoods {
public:
    LostWoods(PolycodeView *view);
    ~LostWoods();
    
    bool Update();
    
private:
    Core *core;
	CollisionScene* gameScene;
	Scene* guiScene;
	Input* input;

	GameLogicHub* gameCore;
	GuiManager* guiManager;

	DebugTools debugTools;

	const Vector2 screensize = Vector2(1200, 700);
};