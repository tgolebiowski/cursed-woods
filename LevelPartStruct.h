#include <Polycode.h>
#ifndef PARTSTRUCT
#define PARTSTRUCT

using namespace Polycode;

///This is a struct that is created during load as a "base template" for the part in the level
///It does not get changed
struct LevelPart
{
	VertexBuffer* meshBuffer;
	float scaling;
	float height;
	float width;
	float depth;
	Color meshColor;
	Mesh mesh;

	LevelPart(String filePath, Color meshColor, float scaling) : mesh(Mesh::TRI_MESH)
	{
		this->scaling = scaling;
		this->meshColor = meshColor;
		this->mesh = Mesh(filePath);

		this->meshBuffer = CoreServices::getInstance()->getRenderer()->createVertexBufferForMesh(&mesh);

		struct floatTriple
		{
			float x, y, z;
		};

		floatTriple min;
		floatTriple max;

		for (int i = 0; i < mesh.getVertexCount(); i++)
		{
			Vector3 p = mesh.getVertexPositionAtIndex(i);
			if (i == 0)
			{
				min.x = p.x;
				min.y = p.y;
				min.z = p.z;
				max = min;
				continue;
			}

			if (min.x > p.x) { min.x = p.x; }
			if (min.y > p.y) { min.y = p.y; }
			if (min.z > p.z) { min.z = p.z; }
			if (max.x < p.x) { max.x = p.x; }
			if (max.y < p.y) { max.y = p.y; }
			if (max.z < p.z) { max.z = p.z; }
		}

		width = max.x - min.x;
		height = max.y - min.y;
		depth = max.z - min.z;
	}
};
#endif