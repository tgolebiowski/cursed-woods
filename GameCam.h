#pragma once
#include <Polycode.h>

using namespace Polycode;

class GameCam
{
public:
	GameCam(Scene* scene, Vector3 viewingVec, Vector2 screensize);
	~GameCam(void);

	void update(Vector3 focus);

	Camera* camera;
private:

	Vector3 viewingVec;
};

