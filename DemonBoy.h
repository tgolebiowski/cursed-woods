#pragma once
#include <Polycode.h>
#ifndef DEMON_BOY_H
#define DEMON_BOY_H
#include "NPC.h"
#include "Actions.h"
#include "Goals.h"

using namespace Polycode;
class DemonBoy : public NPC
{
public:
	DemonBoy(Player* player, PhysicsScene* scene, std::vector<NPC*>* npcList, NavMesh* navMesh);
	~DemonBoy();

	virtual void addToScene(Scene* scene, Vector3 startPosition);
	virtual void removeFromScene(Scene* scene);

	virtual void reactToHit(float damage);
	virtual void reactToWarm(float ember);

protected:
	virtual void move();
	virtual void animate();

private:
	SceneMesh* body;
};
#endif