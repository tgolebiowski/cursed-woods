#pragma once
#include <Polycode.h>
#include <Polycode3DPhysics.h>
#ifndef PLAYER
#define PLAYER
#include "PlayerState.h"
#include "PlayerAnimationState.h"
#include "P_UpdatePacket.h"
#include "ColorTable.h"
#include "WorldMap.h"

using namespace Polycode;

struct MovementValues
{
	enum MoveState{ Step, Run, Dash };

	//consts
	int rotationStartup;
	int accelerationTotal;
	float deccel;
	float runSpeed;
	float stepSpeed;
	float dashSpeed;
	int dashLength;
	int dashCooldown;
	int swingLength;
	int swingStartup;
	int equipmentTransitionLength;
	float equipmentInputMin;

	//active values
	int movementState;
	int accelerationIndex;
	int accelDirectionLastTic;
	int dashLeft;
	int swingState;
	int equipmentSwitchIndex;
	float velocity;
	std::vector<Vector3> directionQueue;
	Vector3 dashVec;

	MovementValues()
	{
		//consts
		rotationStartup = 12;
		accelerationTotal = 6;
		deccel = 0.014f;
		runSpeed = 0.22f;
		stepSpeed = 0.08f;
		dashSpeed = 0.44f;
		dashLength = 15;
		dashCooldown = 22;
		swingLength = 14;
		swingStartup = 7;
		equipmentTransitionLength = 30;
		equipmentInputMin = 0.33f;

		//active values
		movementState = MoveState::Run;
		accelerationIndex = -1;
		accelDirectionLastTic = 0;
		dashLeft = 0;
		swingState = -1;
		equipmentSwitchIndex = -1;
		velocity = 0.0f;
		dashVec = Vector3(0, 0, 0);

		for (int i = 0; i < 10; i++)
		{
			directionQueue.push_back(Vector3(0, 0, 0));
		}
	}
};

class Player
{
public:
	Player(WorldMap* worldMap);
	~Player(void);

	void addToScene(CollisionScene* scene);
	void removeFromScene(CollisionScene* scene);

	//void update(P_UpdatePacket packet);
	void update(P_UpdatePacket packet);
	void animate();

	//In game logic getter
	float getEmberCoeff();
	float getCurseCoeff();

	//In Game playerState mutators
	void damage(float damage);
	float spendEmber();
	void drainEmeber(float drainVal);
	void setCompassPointVector2(Vector2 compassPoint);

	PlayerState* playerState;
	PlayerAnimationInfo* animState;
	CollisionEntity* collisionSphere;
	CollisionEntity* spearTip;
	CollisionEntity* emberSphere;
	SceneMesh* spearMesh;

	SceneMesh* body;
	Skeleton* legsSkeley;
	Skeleton* armsSkeley;

	Vector3 lastSpearPosition;

private:
	PhysicsScene* scene;
	WorldMap* worldMap;

	ScenePrimitive* playerColliderShape;
	ScenePrimitive* spearColliderShape;
	ScenePrimitive* emberColliderShape;

	SceneMesh* bootLeft;
	SceneMesh* bootRight;
	SceneMesh* emberGlow1;
	SceneMesh* emberGlow2;
	std::vector<SceneMesh*> allPlayerMeshes;

	void adjustSpear(P_UpdatePacket packet);
	void adjustPosition(P_UpdatePacket packet);
	void adjustEmber(P_UpdatePacket packet);

	void updateMovement(P_UpdatePacket packet);
	void updateEquipment(P_UpdatePacket packet);
	void animateLegs(Vector3 facing);
	void animateBody();

	int hitTimer;
	MovementValues movementValues;
	Vector2 compassLightDirection;
};
#endif
