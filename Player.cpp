#include "Player.h"


Player::Player(WorldMap* worldMap)
{
	this->worldMap = worldMap;

	body = new SceneMesh("Data/Player/Body.mesh");
	SceneMesh* head = new SceneMesh("Data/Player/Head.mesh");
	SceneMesh* hair = new SceneMesh("Data/Player/Hair.mesh");
	SceneMesh* arms = new SceneMesh("Data/Player/Arms.mesh");
	SceneMesh* legs = new SceneMesh("Data/Player/Legs.mesh");
	bootLeft = new SceneMesh("Data/Player/BootLeft.mesh");
	bootRight = new SceneMesh("Data/Player/BootRight.mesh");

	allPlayerMeshes.push_back(body);
	allPlayerMeshes.push_back(head);
	allPlayerMeshes.push_back(hair);
	allPlayerMeshes.push_back(arms);
	allPlayerMeshes.push_back(legs);
	allPlayerMeshes.push_back(bootLeft);
	allPlayerMeshes.push_back(bootRight);

	arms->loadSkeleton("Data/Player/Player.skeleton");
	legs->loadSkeleton("Data/Player/Player.skeleton");
	legsSkeley = legs->getSkeleton();
	armsSkeley = arms->getSkeleton();

	Vector3 lcbp = legsSkeley->getBoneByName("LeftCalf")->getFullRestMatrix().getPosition();
	Vector3 blbb = bootLeft->getLocalBoundingBox();
	bootLeft->setAnchorPoint(Vector3((lcbp.x / blbb.x) * 0.68, -lcbp.y / (blbb.y / 2.0), 0.0)); //lcbp.z/blbb.z

	Vector3 rcbp = legsSkeley->getBoneByName("RightCalf")->getFullRestMatrix().getPosition();
	Vector3 brbb = bootRight->getLocalBoundingBox();
	bootRight->setAnchorPoint(Vector3((rcbp.x / brbb.x) * 1.75, -(rcbp.y / (brbb.y / 2.0)) * 0.75, 0.0));

	legsSkeley->addAnimation("Idle", "Data/Player/Legs/Idle.anim");
	legsSkeley->addAnimation("Run_F_Pass1", "Data/Player/Legs/Run_F_Pass1.anim");
	legsSkeley->addAnimation("Run_F_Pass2", "Data/Player/Legs/Run_F_Pass2.anim");
	legsSkeley->addAnimation("Run_F_Stride1", "Data/Player/Legs/Run_F_Stride1.anim");
	legsSkeley->addAnimation("Run_F_Stride2", "Data/Player/Legs/Run_F_Stride2.anim");
	legsSkeley->setBaseAnimationByName("Idle");

	armsSkeley->addAnimation("SpearIdle", "Data/Player/Arms_Spear/Spear_Idle.anim");
	armsSkeley->addAnimation("SpearPoint", "Data/Player/Arms_Spear/Spear_Point.anim");
	armsSkeley->addAnimation("SpearBeginSwing", "Data/Player/Arms_Spear/Spear_BeginSwing.anim");
	armsSkeley->addAnimation("SpearEndSwing", "Data/Player/Arms_Spear/Spear_EndSwing.anim");
	armsSkeley->addAnimation("EmberPoint", "Data/Player/Arms_Ember/Ember_Extend.anim");
	armsSkeley->addAnimation("EmberIdle", "Data/Player/Arms_Ember/Ember_Idle.anim");
	armsSkeley->setBaseAnimationByName("SpearIdle");

	spearMesh = new ScenePrimitive(ScenePrimitive::TYPE_CYLINDER, 3.0, 0.05, 4);
	spearMesh->setColor(ColorTable::brownRed);
	spearMesh->colorAffectsChildren = false;
	SceneMesh* spearTipMesh = new SceneMesh("Data/Player/SpearTip.mesh");
	spearTipMesh->setPositionY(1.5f);
	spearTipMesh->setScale(Vector3(0.45f));
	spearTipMesh->setColor(ColorTable::blue1);
	spearTipMesh->colorAffectsChildren = false;
	spearMesh->addChild(spearTipMesh);

	allPlayerMeshes.push_back(spearMesh);
	allPlayerMeshes.push_back(spearTipMesh);

	body->colorAffectsChildren = false;
	head->colorAffectsChildren = false;
	legs->colorAffectsChildren = false;
	arms->colorAffectsChildren = false;
	head->addChild(hair);
	legs->addChild(bootLeft);
	legs->addChild(bootRight);
	arms->addChild(spearMesh);
	body->addChild(head);
	body->addChild(arms);
	body->addChild(legs);
	body->setYaw(-45.0);
	body->setScale(Vector3(0.5));

	body->setColor(ColorTable::lightGray_littlePurple);
	arms->setColor(ColorTable::skin);
	legs->setColor(ColorTable::skin);
	head->setColor(ColorTable::skin);
	hair->setColor(ColorTable::brown3);
	bootLeft->setColor(ColorTable::brown2);
	bootRight->setColor(ColorTable::brown2);

	playerColliderShape = new ScenePrimitive(ScenePrimitive::TYPE_CYLINDER, 2.0, 0.5, 8.0);
	playerColliderShape->setColor(1,1,1,0.25f);
	playerColliderShape->visible = false;

	emberColliderShape = new ScenePrimitive(ScenePrimitive::TYPE_SPHERE, 1.0f, 8.0f, 8.0f);
	emberColliderShape->setColor(1.0f, 0.0f, 1.0f, 0.2f);
	emberColliderShape->visible = false;

	spearColliderShape = new ScenePrimitive(ScenePrimitive::TYPE_SPHERE, 0.5f, 8.0, 8.0);
	spearColliderShape->setColor(0.0f,1.0f,1.0f,0.25f);
	spearColliderShape->visible = false;

	emberGlow1 = new ScenePrimitive(ScenePrimitive::TYPE_CIRCLE, 2.0, 2.0, 9.0f);
	emberGlow1->billboardMode = true;
	emberGlow1->billboardRoll = true;
	emberGlow1->visible = false;
	emberGlow1->alphaTest = false;
	emberGlow1->setColor(ColorTable::emberYellow.r, ColorTable::emberYellow.g, ColorTable::emberYellow.b, 0.5);
	emberGlow2 = new ScenePrimitive(ScenePrimitive::TYPE_CIRCLE, 1.0, 1.0, 7.0f);
	emberGlow2->billboardMode = true;
	emberGlow2->billboardRoll = true;
	emberGlow2->visible = false;
	emberGlow2->alphaTest = false;
	emberGlow2->setColor(ColorTable::emberOrange.r, ColorTable::emberOrange.g, ColorTable::emberOrange.b, 0.5);

	lastSpearPosition = Vector3(0);

	movementValues = MovementValues();

	playerState = new PlayerState(Vector3(0));

	for (int i = 0; i < allPlayerMeshes.size(); i++)
	{
		allPlayerMeshes.at(i)->setMaterialByName("SceneryShader");

		allPlayerMeshes.at(i)->rebuildTransformMatrix();
		allPlayerMeshes.at(i)->getLocalShaderOptions()->addParam(ProgramParam::PARAM_MATRIX, "transformMatrix")
			->setMatrix4(allPlayerMeshes.at(i)->getConcatenatedMatrix());

		allPlayerMeshes.at(i)->rebuildRotation();
		Quaternion q = allPlayerMeshes.at(i)->getRotationQuat();
		allPlayerMeshes.at(i)->getLocalShaderOptions()->addParam(ProgramParam::PARAM_COLOR, "rotationQuat")
			->setColor(Color(q.w, q.x, q.y, q.z));
	}

	animState = new PlayerAnimationInfo();
	animState->meshHeightOffset = 0.95f;
	animState->dashWeight = 0.0f;
	animState->spearInputVec = Vector2(0,0);
	animState->walkCyclePeriod = 0.0f;
	animState->walkPoseIndex = 0;
	animState->walkStep = 0.0f;
	animState->equipSwitchIndex = -1;
	compassLightDirection = Vector2(-1, 0);
}

Player::~Player(void)
{

}

void Player::addToScene(CollisionScene* scene)
{
	collisionSphere = scene->addCollisionChild(playerColliderShape, CollisionEntity::SHAPE_CYLINDER, 1);
	emberSphere = scene->addCollisionChild(emberColliderShape, CollisionEntity::SHAPE_SPHERE);
	spearTip = scene->addCollisionChild(spearColliderShape, CollisionEntity::SHAPE_SPHERE);
	scene->addEntity(body);
	scene->addEntity(emberGlow2);
	scene->addEntity(emberGlow1);
}

void Player::removeFromScene(CollisionScene* scene)
{
	scene->removeCollision(playerColliderShape);
	scene->removeCollision(emberColliderShape);
	scene->removeCollision(spearColliderShape);
	scene->removeEntity(playerColliderShape);
	collisionSphere = NULL;
	emberSphere = NULL;
	spearTip = NULL;
	scene->removeEntity(body);
	scene->removeEntity(emberGlow2);
	scene->removeEntity(emberGlow1);
}

float Player::getCurseCoeff()
{
	float curveStartY = 0.0f;
	float curveHandle1X = 0.0f;
	float curveHandle1Y = 0.25f;

	float curveEndY = 1.0f;
	float curveHandle2X = 1.0f - 0.33f;
	float curveHandle2Y = 1.0f;

	BezierCurve curseCoeffCurve = BezierCurve();
	curseCoeffCurve.addControlPoint2dWithHandles(0.0f, curveStartY, 0.0f, curveStartY, curveHandle1X, curveHandle1Y);
	curseCoeffCurve.addControlPoint2dWithHandles(curveHandle2X, curveHandle2Y, 1.0f, curveEndY, 1.0f, curveEndY);

	float normalizedCurseValue = playerState->curseBuildUp / 1000.0f;
	return curseCoeffCurve.getYValueAtX(normalizedCurseValue);
}

float Player::getEmberCoeff()
{
	float curveStartY = 0.0f;
	float curveHandle1X = 0.0f;
	float curveHandle1Y = 0.25f;

	float curveEndY = 1.0f;
	float curveHandle2X = 1.0f - 0.33f;
	float curveHandle2Y = 1.0f;

	BezierCurve curseCoeffCurve = BezierCurve();
	curseCoeffCurve.addControlPoint2dWithHandles(0.0f, curveStartY, 0.0f, curveStartY, curveHandle1X, curveHandle1Y);
	curseCoeffCurve.addControlPoint2dWithHandles(curveHandle2X, curveHandle2Y, 1.0f, curveEndY, 1.0f, curveEndY);

	float normalizedCurseValue = playerState->emberMeter / 1000.0f;
	return curseCoeffCurve.getYValueAtX(normalizedCurseValue);
}

float Player::spendEmber()
{
	if(playerState->currentEquip == PlayerState::Equipment::Spear || 
		!playerState->emberExtended || playerState->emberMeter <= 0.0f) return 0.0f;

	const float standardEmberSpend = 1.8f;

	float spend = standardEmberSpend;
	float waste = getCurseCoeff() * spend;

	float total = spend + waste;
	if (total > playerState->emberMeter) {
		total = playerState->emberMeter;
		spend = total * (spend / waste);
	}

	playerState->emberMeter -= total;
	return spend;
}

void Player::damage(float damage)
{
	if(hitTimer > 0) return;
	hitTimer = 30;

	float curseCoeffStart = getCurseCoeff();
	playerState->curseBuildUp += damage;
	float curseCoeffEnd = getCurseCoeff();

	float emberLost = curseCoeffEnd - curseCoeffStart;
	playerState->emberMeter -= emberLost;
}

void Player::animate()
{
	body->setPosition(collisionSphere->getEntity()->getPosition());
	body->setPositionY(-0.0f);

	Vector3 facing = playerState->direction;
	float yawAngle = (Vector3(0, 0, 1.0).angleBetween(facing) * (180.0f / 3.141592653f));
	float quikCross = Vector3(0,0,1.0).crossProduct(facing).y;
	if(quikCross < 0.0) yawAngle *= -1.0f;
	body->setYaw(yawAngle);

	animState->walkCyclePeriod += playerState->speed;
	if (animState->walkCyclePeriod > 2.0 * 3.1415926 * 0.5) 
		animState->walkCyclePeriod -= (2.0 * 3.1415926 * 0.5);

	float walkBump = std::abs(std::cos(animState->walkCyclePeriod / (0.5f)))/2.0f;
	walkBump *= 0.08f;
	walkBump *= std::max(playerState->speed, movementValues.runSpeed) / movementValues.runSpeed;
	body->setPositionY(walkBump);

	const float maxTilt = 7.0f;
	float tiltIntoRun = maxTilt * playerState->speed / movementValues.runSpeed;
	body->setPitch(tiltIntoRun);

	animateLegs(facing);

	{
		Vector3 handPosition = armsSkeley->getBoneByName("RightHand")->getConcatenatedMatrixRelativeTo(body)
			.getPosition();
		Vector3 pointerPosition = armsSkeley->getBoneByName("RightPointer")->getConcatenatedMatrixRelativeTo(body)
			.getPosition();
		Vector3 direction = pointerPosition - handPosition;
		direction.Normalize();
		Vector3 up = Vector3(0, 1, 0);
		Vector3 axis = up.crossProduct(direction);
		float angle = up.angleBetween(direction);


		playerState->direction.Normalize();
		emberSphere->getEntity()->setPosition(collisionSphere->getEntity()->getPosition() + playerState->direction);

		Quaternion spearRot = Quaternion();
		spearRot.fromAngleAxis(angle, axis);

		spearMesh->setPosition(handPosition);
		spearMesh->setPositionY(spearMesh->getPosition().y - 0.05f);
		spearMesh->setRotationByQuaternion(spearRot);
	}

	emberGlow1->visible = playerState->currentEquip == PlayerState::Equipment::Ember;
	emberGlow2->visible = emberGlow1->visible;
	emberGlow1->setPosition(armsSkeley->getBoneByName("LeftHand")->getConcatenatedMatrix().getPosition());
	emberGlow1->setPositionY(emberGlow1->getPosition().y - 0.1);
	emberGlow2->setPosition(emberGlow1->getPosition());

	const float maxScaleJitter = 0.12f;
	float currentScale1 = emberGlow1->getScale().x;
	float currentScale2 = emberGlow2->getScale().x;
	float scaleJitter1 = (((float)(rand() % 100)) / 100.0) * maxScaleJitter;
	float scaleJitter2 = (((float)(rand() % 100)) / 100.0) * maxScaleJitter;
	emberGlow1->setScale(Vector3(scaleJitter1 + currentScale1));
	emberGlow2->setScale(Vector3(scaleJitter2 + currentScale2));
	const float emberGlowRollSpeed = 13.0f;
	emberGlow1->setRoll(emberGlow1->getRoll() + emberGlowRollSpeed);
	emberGlow2->setRoll(emberGlow2->getRoll() - emberGlowRollSpeed);


	for (int i = 0; i < allPlayerMeshes.size(); i++)
	{
		allPlayerMeshes.at(i)->rebuildRotation();
		allPlayerMeshes.at(i)->rebuildTransformMatrix();
		allPlayerMeshes.at(i)->getLocalShaderOptions()->getLocalParamByName("transformMatrix")
			->setMatrix4(allPlayerMeshes.at(i)->getConcatenatedMatrix());

		Quaternion q = allPlayerMeshes.at(i)->getConcatenatedQuat();
		allPlayerMeshes.at(i)->getLocalShaderOptions()->getLocalParamByName("rotationQuat")
			->setColor(Color(q.w, q.x, q.y, q.z));
	}
}

void Player::animateLegs(Vector3 facing)
{
	legsSkeley->stopAllAnimations();
	struct WalkAnimator
	{
		Skeleton* legsSkeleton;
		String part1;
		String part2;
		String part3;
		String part4;
		float stepSection;
		float maxWeight;

		WalkAnimator(Skeleton* legsSkeleton)
		{
			this->legsSkeleton = legsSkeleton;
		}

		void Animate()
		{
			BezierCurve emphasizeFirst = BezierCurve();
			emphasizeFirst.addControlPoint2dWithHandles(0,0, 0,0, 0.25,0.15);
			emphasizeFirst.addControlPoint2dWithHandles(0.85,0.75, 1,1, 1,1);
			BezierCurve emphasizeLast = BezierCurve();
			emphasizeLast.addControlPoint2dWithHandles(0,0, 0,0, 0.15,0.25);
			emphasizeLast.addControlPoint2dWithHandles(0.75,0.85, 1,1, 1,1);

			const int stepSectionDivisions = 12;

			if(stepSection < 1.0f)
			{
				int var1 = (1.0f - stepSection) * stepSectionDivisions;
				int var2 = (stepSection)* stepSectionDivisions;
				float w1 = maxWeight * emphasizeLast.getYValueAtX(((float)var1) / ((float)stepSectionDivisions));
				float w2 = maxWeight * emphasizeLast.getYValueAtX(((float)var2) / ((float)stepSectionDivisions));
				legsSkeleton->playAnimationByName(part1, w1);
				legsSkeleton->playAnimationByName(part2, w2);
			}
			else if(stepSection >= 1.0f && stepSection < 2.0f)
			{ 
				int var1 = (2.0f - stepSection) * stepSectionDivisions;
				int var2 = (stepSection - 1.0f) * stepSectionDivisions;
				float w1 = maxWeight * emphasizeFirst.getYValueAtX(((float)var1) / ((float)stepSectionDivisions));
				float w2 = maxWeight * emphasizeFirst.getYValueAtX(((float)var2) / ((float)stepSectionDivisions));
				legsSkeleton->playAnimationByName(part2, w1);
				legsSkeleton->playAnimationByName(part3, w2);
			}
			else if(stepSection >= 2.0f && stepSection < 3.0f)
			{  
				int var1 = (3.0f - stepSection) * stepSectionDivisions;
				int var2 = (stepSection - 2.0f) * stepSectionDivisions;
				float w1 = maxWeight * emphasizeLast.getYValueAtX(((float)var1) / ((float)stepSectionDivisions));
				float w2 = maxWeight * emphasizeLast.getYValueAtX(((float)var2) / ((float)stepSectionDivisions));
				legsSkeleton->playAnimationByName(part3, w1);
				legsSkeleton->playAnimationByName(part4, w2);
			}
			else if(stepSection >= 3.0f)
			{  
				int var1 = (4.0f - stepSection) * stepSectionDivisions;
				int var2 = (stepSection - 3.0f) * stepSectionDivisions;
				float w1 = maxWeight * emphasizeFirst.getYValueAtX(((float)var1) / ((float)stepSectionDivisions)); 
				float w2 = maxWeight * emphasizeFirst.getYValueAtX(((float)var2) / ((float)stepSectionDivisions));
				legsSkeleton->playAnimationByName(part4, w1);
				legsSkeleton->playAnimationByName(part1, w2);
			}
		}
	};
	WalkAnimator walker = WalkAnimator(legsSkeley);

	Vector3 moveDir = playerState->direction;
	moveDir.Normalize();
	float moveFaceCross = moveDir.crossProduct(facing).y;
	float moveFaceDot = moveDir.dot(facing);

	float maxRunWeight = playerState->speed / (movementValues.runSpeed);
	if(maxRunWeight > 1.0f) maxRunWeight = 1.0f;
	//maxRunWeight -= animState->dashWeight;
	//if(maxRunWeight < 0.0f) maxRunWeight = 0.0f;

	//float standWeight = 1.0f - maxRunWeight - animState->dashWeight;
	//if(standWeight < 0.0f) standWeight = 0.0f;
	//if(standWeight >= 1.0f) standWeight = 1.0f;

	//legsSkeley->playAnimationByName("Idle", 1.0 - maxRunWeight);
	legsSkeley->playAnimationByName("Idle", 1.0);
	//if(animState->dashWeight != 0.0f) playerLegsMesh->getSkeleton()->playAnimationByName("Dash", animState->dashWeight);

	float stepSection = animState->walkCyclePeriod / (3.141592653f * 2.0f * 0.5f * 0.25f);

	/*if (moveFaceDot >= 0.0f)
	{
		walker.part1 = "F_Left_Pass";
		walker.part2 = "F_Left_Heel";
		walker.part3 = "F_Right_Pass";
		walker.part4 = "F_Right_Heel";
	}
	else if(moveFaceDot < 0.0f)
	{
		walker.part1 = "Back_Left_Pass";
		walker.part2 = "Back_Right_Heel";
		walker.part3 = "Back_Right_Pass";
		walker.part4 = "Back_Left_Heel";
	}*/

	//float fbWeight = std::abs(moveFaceDot) * maxRunWeight;
	float fbWeight = maxRunWeight;
	walker.maxWeight = fbWeight;
	walker.part1 = "Run_F_Stride1";
	walker.part2 = "Run_F_Pass1";
	walker.part3 = "Run_F_Stride2";
	walker.part4 = "Run_F_Pass2";
	walker.maxWeight = maxRunWeight;
	walker.stepSection = stepSection;
	walker.Animate();

	Vector3 leftBootPosition = legsSkeley->getBoneByName("LeftCalf")
		->getConcatenatedMatrixRelativeTo(body).getPosition();
	Matrix4 targetLeftBootTransform = (legsSkeley->getBoneByName("LeftCalf")->getRestMatrix() *
		legsSkeley->getBoneByName("LeftCalf")->getBoneMatrix());
	Quaternion leftBootRotQuat = Quaternion();
	leftBootRotQuat.createFromMatrix(targetLeftBootTransform);
	bootLeft->setPosition(leftBootPosition);
	bootLeft->setRotationByQuaternion(leftBootRotQuat);

	Vector3 rightBootPosition = legsSkeley->getBoneByName("RightCalf")
		->getConcatenatedMatrixRelativeTo(body).getPosition();
	Matrix4 targetRightBootTransform = (legsSkeley->getBoneByName("RightCalf")->getRestMatrix() *
		legsSkeley->getBoneByName("RightCalf")->getBoneMatrix());
	Quaternion rightBootRotQuat = Quaternion();
	rightBootRotQuat.createFromMatrix(targetRightBootTransform);
	bootRight->setPosition(rightBootPosition);
	bootRight->setRotationByQuaternion(rightBootRotQuat);
}

void Player::update(P_UpdatePacket packet)
{
	//packet.moveInput = Vector2(1.0f, -0.5f);
	//packet.moveInput.Normalize();
	if (hitTimer > 0) hitTimer--;

	Vector3 spearTipStart = spearTip->getEntity()->getPosition();
	Vector3 playerPOSStart = collisionSphere->getEntity()->getPosition();

	updateMovement(packet);
	updateEquipment(packet);

	Vector3 spearTipEnd = spearTip->getEntity()->getPosition();
	Vector3 playerPOSEnd = collisionSphere->getEntity()->getPosition();

	playerState->emberExtended = false;
	if (playerState->currentEquip == PlayerState::Ember &&
		packet.equipmentInput.length() > movementValues.equipmentInputMin)
	{
		playerState->emberExtended = true;
	}

	if (packet.equipmentInput.length() > movementValues.equipmentInputMin)
	{
		Vector2 equipmentInputFacing = packet.equipmentInput;
		equipmentInputFacing.Normalize();
		playerState->direction = Vector3(equipmentInputFacing.x, 0.0, equipmentInputFacing.y);
	}
	else if ((playerPOSEnd - playerPOSStart).length() > 0.05f)
	{
		playerState->direction = playerPOSEnd - playerPOSStart;
	}

	spearTip->getEntity()->setPosition(spearMesh->getChildAtIndex(0)->getConcatenatedMatrix().getPosition());

	if (playerState->currentEquip == PlayerState::Equipment::Spear)
	{
		playerState->spearMovementThisFrame = spearTipEnd - spearTipStart;
	}
	else
	{
		playerState->spearMovementThisFrame = Vector3(0);
	}
}

void Player::updateMovement(P_UpdatePacket packet)
{
	//setup
	playerState->position = collisionSphere->getEntity()->getPosition();

	if (packet.dashWasPressed && movementValues.dashLeft <= 0 && packet.moveInput.length() >= 0.9f)
	{
		movementValues.dashLeft = movementValues.dashLength;
		movementValues.dashVec = Vector3(packet.moveInput.x, 0.0f, packet.moveInput.y);
	}

	if (movementValues.dashLeft > 0) movementValues.movementState = MovementValues::Dash;
	else if (packet.equipmentInput.length() >= 0.33) movementValues.movementState = MovementValues::Step;
	else movementValues.movementState = MovementValues::Run;

	Vector3 nextPosition = collisionSphere->getEntity()->getPosition();
	Vector3 positionAdjust = Vector3(0,0,0);
	Vector3 direction = Vector3(0,0,0);

	float calculatedVelocity = 0.0f;
	float targetVelocity = 0.0f;

	//run movment algorithms
	if (movementValues.movementState == MovementValues::Run)
	{
		float adjustedRunSpeed = movementValues.runSpeed;
		targetVelocity = adjustedRunSpeed * packet.moveInput.length();
		if(packet.moveInput.length() > 0.95f) targetVelocity = adjustedRunSpeed;
		else if (packet.moveInput.length() < 0.25f) targetVelocity = 0.0f;
		calculatedVelocity = targetVelocity;

		float velocityDifference = targetVelocity - playerState->speed;
		int accelDirection = velocityDifference / std::abs(velocityDifference);

		if((movementValues.accelDirectionLastTic == accelDirection) && 
			(movementValues.accelerationIndex < movementValues.accelerationTotal))
		{
			movementValues.accelerationIndex++;
		}
		else
		{
			movementValues.accelerationIndex = 0;
			movementValues.accelDirectionLastTic = accelDirection;
		}

		calculatedVelocity = playerState->speed + velocityDifference *
			std::pow(((double)movementValues.accelerationIndex/(double)movementValues.accelerationTotal), 2.0);

		Vector3 inputDirection = Vector3(0,0,0);
		inputDirection.x += packet.moveInput.x;
		inputDirection.z += packet.moveInput.y;

		movementValues.directionQueue.push_back(inputDirection);
		movementValues.directionQueue.erase(movementValues.directionQueue.begin());

		Vector3 sumVec = Vector3(0,0,0);
		for(int i = 0; i < movementValues.directionQueue.size(); i++)
		{
			sumVec += movementValues.directionQueue.at(i);
		}

		sumVec.x /= (float)movementValues.directionQueue.size();
		sumVec.y /= (float)movementValues.directionQueue.size();
		sumVec.z /= (float)movementValues.directionQueue.size();
		sumVec.Normalize();

		direction = sumVec;
		movementValues.velocity = movementValues.runSpeed;

		float velDifference = calculatedVelocity - playerState->speed;
		if (velDifference < 0) calculatedVelocity = playerState->speed - movementValues.deccel;
		/*if (velDifference < 0 && calculatedVelocity > movementValues.deccel)
			calculatedVelocity = playerState->speed - movementValues.deccel;
		else if (velDifference < 0)
			calculatedVelocity = 0.0f;*/

		if (movementValues.dashCooldown > 12 && movementValues.dashLeft == 0)
		{
			calculatedVelocity -= (movementValues.runSpeed / 10.0f);
			if (calculatedVelocity < 0) calculatedVelocity = 0;
		}

	}
	else if (movementValues.movementState == MovementValues::Step)
	{
		targetVelocity = movementValues.stepSpeed * packet.moveInput.length();

		calculatedVelocity = targetVelocity;
		direction.x = packet.moveInput.x;
		direction.z = packet.moveInput.y;
		movementValues.velocity = movementValues.stepSpeed;

		Vector3 directionOverride = direction;
		directionOverride.Normalize();
		for(int i = 0; i < movementValues.directionQueue.size(); i++)
		{
			movementValues.directionQueue[i] = directionOverride;
		}
	}
	else if (movementValues.movementState == MovementValues::Dash)
	{
		direction = movementValues.dashVec;
		direction.Normalize();
		calculatedVelocity = (movementValues.dashVec * movementValues.dashSpeed).length();
		movementValues.dashLeft--;
	}

	positionAdjust = direction * calculatedVelocity;
	nextPosition = collisionSphere->getEntity()->getPosition() + positionAdjust;
	collisionSphere->getEntity()->setPosition(nextPosition);
	collisionSphere->getEntity()->setPositionY(1.0f);
	playerState->speed = calculatedVelocity;
	playerState->position = collisionSphere->getEntity()->getPosition();
}

void Player::updateEquipment(P_UpdatePacket packet)
{
	if (packet.switchEquipmentPressed && movementValues.swingState == -1)
	{
		if (playerState->currentEquip == PlayerState::Equipment::Spear)
			playerState->currentEquip = PlayerState::Equipment::Ember;
		else if (playerState->currentEquip == PlayerState::Equipment::Ember)
			playerState->currentEquip = PlayerState::Equipment::Spear;
	}

	armsSkeley->stopAllAnimations();

	if (playerState->currentEquip == PlayerState::Equipment::Spear) {

		if (movementValues.swingState == -1 && packet.equipmentActionWasPressed)
		{
			movementValues.swingState = 0;
		}

		if (movementValues.swingState >= 0) {
			if (movementValues.swingState <= movementValues.swingStartup)
			{
				float completion = (float)movementValues.swingState / (float)movementValues.swingStartup;
				completion = std::sqrtf(completion);

				float nonSwingValue = 1.0 - completion;
				float pointWeight = (packet.equipmentInput.length() - movementValues.equipmentInputMin)
					/ (1.0 - movementValues.equipmentInputMin);
				float idleValue = 1.0 - pointWeight;
				armsSkeley->playAnimationByName("SpearPoint", nonSwingValue * pointWeight);
				armsSkeley->playAnimationByName("SpearIdle", nonSwingValue * idleValue);
				armsSkeley->playAnimationByName("SpearBeginSwing", completion);
			}
			else
			{
				int swingPart = movementValues.swingState - movementValues.swingStartup;
				float completion = (float)swingPart / (float)movementValues.swingLength;

				BezierCurve weightCurve = BezierCurve();
				weightCurve.addControlPoint2dWithHandles(0, 0.55, 0, 0, 0, 0.55);
				weightCurve.addControlPoint2dWithHandles(0.7, 1, 1, 1, 0.7, 1);

				float weight = weightCurve.getYValueAtX(completion);
				armsSkeley->playAnimationByName("SpearBeginSwing", 1.0 - weight);
				armsSkeley->playAnimationByName("SpearEndSwing", weight);
			}

			movementValues.swingState++;
			if (movementValues.swingState > (movementValues.swingLength + movementValues.swingStartup))
			{
				movementValues.swingState = -1;
			}
		}
		else
		{
			if (packet.equipmentInput.length() > movementValues.equipmentInputMin)
			{
				float pointWeight = (packet.equipmentInput.length() - movementValues.equipmentInputMin)
					/ (1.0 - movementValues.equipmentInputMin);
				armsSkeley->playAnimationByName("SpearPoint", pointWeight);
				armsSkeley->playAnimationByName("SpearIdle", 1.0 - pointWeight);
			}
			else
			{
				armsSkeley->playAnimationByName("SpearIdle", 1.0f);
			}
		}
	}
	else if (playerState->currentEquip == PlayerState::Equipment::Ember)
	{
		if (packet.equipmentInput.length() > movementValues.equipmentInputMin)
		{
			float pointWeight = (packet.equipmentInput.length() - movementValues.equipmentInputMin)
				/ (1.0 - movementValues.equipmentInputMin);
			armsSkeley->playAnimationByName("EmberPoint", pointWeight);
			armsSkeley->playAnimationByName("EmberIdle", 1.0 - pointWeight);

			float const scaleChange = 0.3;
			float calculatedScaling = 0.0f;
			if (compassLightDirection.length() > 0.1) {
				float facingBoundNode = Vector2(playerState->direction.x, playerState->direction.z).
					dot(compassLightDirection);
				calculatedScaling = scaleChange * facingBoundNode;
			}

			calculatedScaling *= (pointWeight);
			calculatedScaling += 1.0f;
			emberGlow1->setScale(Vector3(calculatedScaling));
			emberGlow2->setScale(Vector3(calculatedScaling));
		}
		else
		{
			armsSkeley->playAnimationByName("EmberIdle", 1.0f);
			emberGlow1->setScale(Vector3(1.0));
			emberGlow2->setScale(Vector3(1.0));
		}
	}
}

void Player::setCompassPointVector2(Vector2 compassPoint)
{
	this->compassLightDirection = compassPoint;
}