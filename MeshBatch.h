#pragma once
#include <Polycode.h>
#include <PolyRenderer.h>
#include <PolyGLRenderer.h>
#include <PolyResourceManager.h>


using namespace Polycode;

struct DrawCallInfo
{
	Matrix4 matrix;
	Color color;
};

struct VBOBatch
{
	int count;
	int shaderIndex;
	String name;
	Texture* texture;
	Material* material;
	ShaderBinding* binding;
	VertexBuffer* buffer;
	std::vector<DrawCallInfo> drawCallInfo;
	VBOBatch()
	{
		count = 0;
		shaderIndex = -1;
	}

	VBOBatch(SceneMesh* mesh)
	{
		count = 0;
		texture = mesh->getTexture();
		material = mesh->getMaterial();
		binding = mesh->getLocalShaderOptions();
		CoreServices::getInstance()->getRenderer()->createVertexBufferForMesh(mesh->getMesh());
		//buffer = mesh->getMesh()->getVertexBuffer();
	}
};

struct BatchInfo
{
	int count;
	int shaderIndex;
	String name;
	Texture* meshTexture;
	Material* material;
	ShaderBinding* binding;
	VertexDataArray positions;
	VertexDataArray normals;
	VertexDataArray tangents;
	VertexDataArray texCoords;
	IndexDataArray indexArray;
	DrawCallInfo info[128];

	BatchInfo() : 
		positions(VertexDataArray::VERTEX_DATA_ARRAY),
		normals(VertexDataArray::NORMAL_DATA_ARRAY),
		tangents(VertexDataArray::TANGENT_DATA_ARRAY),
		texCoords(VertexDataArray::TEXCOORD_DATA_ARRAY),
		indexArray(IndexDataArray::INDEX_DATA_ARRAY)
	{
		count = 0;
	};

	BatchInfo(SceneMesh* mesh) : 
		positions(VertexDataArray::VERTEX_DATA_ARRAY),
		normals(VertexDataArray::NORMAL_DATA_ARRAY),
		tangents(VertexDataArray::TANGENT_DATA_ARRAY),
		texCoords(VertexDataArray::TEXCOORD_DATA_ARRAY),
		indexArray(IndexDataArray::INDEX_DATA_ARRAY)
	{
		count = 0;
		positions.data = mesh->getMesh()->vertexPositionArray.data;
		normals.data = mesh->getMesh()->vertexNormalArray.data;
		tangents.data = mesh->getMesh()->vertexTangentArray.data;
		texCoords.data = mesh->getMesh()->vertexTexCoordArray.data;
		indexArray.data = mesh->getMesh()->indexArray.data;
		meshTexture = mesh->getTexture();
		material = mesh->getMaterial();
		binding = mesh->getLocalShaderOptions();
	};
};

class MeshBatch : public Polycode::Entity
{
public:
	MeshBatch();
	~MeshBatch();

	void addMesh(SceneMesh* mesh, String name);
	void clearBatch(String name);
	void clearBatch(int index);
	void addVBOMesh(SceneMesh* mesh, String name);

	int getVBOBatchCount() { return vboBatches.size(); };

    virtual void Render();
private:	
	ResourceManager* rManager;
	std::vector<BatchInfo*> batches;
	std::vector<VBOBatch*> vboBatches;
};

