#pragma once
#include <Polycode.h>
#ifndef INPUT
#define INPUT
#include "P_UpdatePacket.h"

using namespace Polycode;

class Input
{
public:
	Input(CoreInput* input);
	~Input(void);

	void update();
	P_UpdatePacket createPInputPacket();

	Vector2 leftStick;
	Vector2 rightStick;
	bool rbIsDown;
	bool rbWasPressed;
	bool lbIsDown;
	bool lbWasPressed;
	bool xIsDown;
	bool xWasPressed;
	bool pauseWasPressed;
	bool selectWasPressed;

private:
	CoreInput* input;
	bool pauseLastState;
	bool selectLastState;
};
#endif
