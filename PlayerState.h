#include <Polycode.h>
#ifndef PLAYERSTATE
#define PLAYERSTATE
#include "P_UpdatePacket.h"

using namespace Polycode;

class PlayerState
{
public:
	enum Equipment
	{
		Ember, Spear
	};

	enum CompassBinding
	{
		FinalTorch, Pointer
	};

	PlayerState(Vector3 startPosition)
	{
		position = startPosition;
		spearOffset = Vector3(0);
		direction = Vector3(0);
		speed = 0.0f;
		curseBuildUp = 0.0f;
		emberMeter = 100.0f;
		emberExtended = false;
		currentEquip = Equipment::Spear;
		currentCompassBinding = CompassBinding::Pointer;
	}

	Vector3 position;
	Vector3 direction;
	Vector3 spearOffset;
	Vector3 spearMovementThisFrame;
	float curseBuildUp;
	float speed;
	float emberMeter;
	int currentEquip;
	int currentCompassBinding;
	bool emberExtended;
};
#endif