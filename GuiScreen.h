#include <Polycode.h>
#ifndef GUISCREEN
#define GUISCREEN
#include "Input.h"
#include "WorldMap.h"
#include "ColorTable.h"
#include "Player.h"

using namespace Polycode;

class GuiScreen
{
public:
	GuiScreen(Scene* guiScene, Input* input);
	virtual void update() = 0;
	virtual void loadin() = 0;
	virtual void unload() = 0;
	bool closeFlag;

protected:
	Scene* guiScene;
	Input* input;
};

class EmberCurseScreen : public GuiScreen
{
public:
	EmberCurseScreen(Scene* guiScene, Input* input, Player* player);

	void update();
	void loadin();
	void unload();

private:
	Color skinColor;
	Color curseColor;
	Player* player;

	SceneMesh* armMesh;
	SceneMesh* emberChainMesh;
	SceneMesh* emberGlow;
};

class MapScreen : public GuiScreen
{
public:
	MapScreen(Scene* guiScene, Input* input, WorldMap* worldMap);

	void update();
	void loadin();
	void unload();

private:
	WorldMap* worldMap;

	float scaling;

	SceneMesh* mapMesh;
	Scene* mapTextureRenderScene;
	SceneRenderTexture* mapTextureRenderTexture;

	ScenePrimitive* marker;
	ScenePrimitive* backdrop;
	std::vector<ScenePrimitive*> nodes;
	std::vector<SceneLine*> connections;

};

#endif