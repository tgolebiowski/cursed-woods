#ifndef WORLDMAP
#define WORLDMAP
#include <Polycode.h>
#include <algorithm>
#include "Delaunay_Src\Triangulator.h"
#include "Delaunay_Src\Poisson_Distributer.h"
#include "SaveStructs.h"
#include <set>

using namespace Polycode;

class WorldMap
{
public:
	struct MapNode
	{
		int identifier;
		Vector2 position;
		std::vector<int> lockedDoors;
		std::vector<MapNode*> neighbors;
		Site* baseSite;
		
		MapNode()
		{

		}
	};
	
	struct NodeConnection
	{
		MapNode* parent;
		MapNode* child;
	};

	WorldMap();
	MapNode* addNode(Site* site);
	void makeConnection(MapNode* parent, MapNode* child);

	int current;
	std::vector<MapNode*> nodes;
	std::vector<NodeConnection> connections;
	WorldMap* selfReference;

private:

};
#endif