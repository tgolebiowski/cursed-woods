#include "AI_Brain.h"
#include "Goals.h"
#include "Actions.h"

ActionPlanSolver::ActionPlanSolver()
{
	micropather = new MicroPather(((Graph*)this), 250, 6, false);
}

void ActionPlanSolver::solve(std::vector<Action*>* plan, Goal* goal, WorldState startState)
{
	micropather->Reset();

	WorldState goalTarget = goal->getTargetState();

	ActionStep newTarget = ActionStep();
	newTarget.action = NULL;
	newTarget.predictedState = goalTarget;
	currentTarget = &newTarget;

	ActionStep startStep = ActionStep();
	startStep.action = NULL;
	startStep.predictedState = startState;
	ActionStep* start = &startStep;

	micropather::MPVector<void*> sequence = micropather::MPVector<void*>();
	float cost = 0.0f;
	int solveReturn = micropather->Solve((void*)start, (void*)currentTarget, &sequence, &cost);

	for (int j = sequence.size() - 2; j > 0; j--)
	{
		if (j == 0) continue;
		plan->push_back(((ActionStep*)sequence[j])->action);
	}
	worldStateTarget = goalTarget;
	currentTarget = NULL;

	for (int j = 0; j < createdActionSteps.size(); j++)
		delete createdActionSteps.at(j);

	createdActionSteps.clear();
}

void ActionPlanSolver::AdjacentCost(void* state, MP_VECTOR<micropather::StateCost>* adjacent)
{
	ActionStep* lastStep = (ActionStep*)state;

	float const minDiffCutoff = 0.001f;
	float diff = lastStep->predictedState.getDiff(currentTarget->predictedState);
	if (diff > minDiffCutoff)
	{
		for (int i = 0; i < actions.size(); i++)
		{
			if (actions.at(i)->hasPrereq(&lastStep->predictedState))
			{
				ActionStep* nextStep = new ActionStep();
				createdActionSteps.push_back(nextStep);
				nextStep->predictedState = lastStep->predictedState;

				nextStep->action = actions.at(i);
				float actionCost = actions.at(i)->describeChangeInWorldState(&nextStep->predictedState,
					&currentTarget->predictedState);

				StateCost stateCost = StateCost();
				stateCost.cost = actionCost;
				stateCost.state = (void*)nextStep;
				adjacent->push_back(stateCost);
			}
		}
	}
	else
	{
		StateCost stateCost = StateCost();
		stateCost.cost = 0.0f;
		stateCost.state = (void*)currentTarget;
		adjacent->push_back(stateCost);
	}
}

float ActionPlanSolver::LeastCostEstimate(void* startState, void* endState)
{
	WorldState startWorldState = ((ActionStep*)startState)->predictedState;
	WorldState endWorldState = ((ActionStep*)endState)->predictedState;
	return (startWorldState.getDiff(endWorldState));
}

void ActionPlanSolver::PrintStateInfo(void* state)
{
	return;
}

NPC_Brain::NPC_Brain(NPC* npc, Player* player, std::vector<Goal*> goals, std::vector<Action*> actions)
{
	this->npc = npc;
	this->player = player;
	this->goals = goals;
	this->actions = actions;
	solver = new ActionPlanSolver();

	currentTarget = NULL;
	currentGoal = NULL;
}

NPC_Brain::~NPC_Brain()
{
	for (int i = 0; i < goals.size(); i++)
	{
		delete goals.at(i);
	}

	for (int i = 0; i < actions.size(); i++)
	{
		delete actions.at(i);
	}
}

void NPC_Brain::run()
{
	updatePlanning();

	if (currentPlan.size() > 0)
	{
		if (currentPlan.back()->doneFlag)
			currentPlan.pop_back();

		if (currentPlan.size() > 0)
		{
			Action* currentAction = currentPlan.back();
			WorldState current = WorldState(npc->collisionCollider->getEntity()->getPosition(),
				player->collisionSphere->getEntity()->getPosition(), npc->getEmberVal(), npc->getCurseVal(),
				player->getEmberCoeff(), player->getCurseCoeff());
			WorldState target = worldStateTarget;
			currentAction->run(&current, &target);
		}
	}
	else
	{
		createPlan(goals.front());
	}
}

void NPC_Brain::updatePlanning()
{
	if (currentPlan.size() > 0 && currentPlan.front()->doneFlag)
	{
		currentPlan.erase(currentPlan.begin());
		if (currentPlan.size() == 0)
			currentGoal = NULL;
	}

	for (int i = 0; i < goals.size(); i++)
	{
		if (goals.at(i) == currentGoal)
			continue;

		WorldState thisGoalsTargetState = goals.at(i)->getTargetState();
		if (thisGoalsTargetState.getDiff(worldStateTarget) > 0.5f && 
			(currentGoal == NULL || goals.at(i)->priority > currentGoal->priority))
		{
			createPlan(goals.at(i));
			currentGoal = goals.at(i);
			break;
		}
	}
}

void NPC_Brain::createPlan(Goal* goal)
{
	WorldState startState = WorldState(npc->collisionCollider->getEntity()->getPosition(),
		player->collisionSphere->getEntity()->getPosition(), npc->getEmberVal(), npc->getCurseVal(),
		player->getEmberCoeff(), player->getCurseCoeff());

	for (int i = 0; i < actions.size(); i++)
		actions.at(i)->doneFlag = false;

	currentPlan.clear();
	solver->actions = actions;
	solver->solve(&currentPlan, goal, startState);
}

