#pragma once
#include <Polycode.h>
#include <Polycode3DPhysics.h>
#ifndef PHYSMANG
#define PHYSMANG
#include "NPC.h"
#include "Level.h"


class PhysicsManager
{
public:
	PhysicsManager(std::vector<NPC*>* npcList);
	~PhysicsManager(void);

	void update();

	CollisionScene* scene;
	Level* level;
	std::vector<NPC*>* npcList;
	CollisionEntity* playerCollider;
private:
	void separateUnevenly(CollisionEntity* entity, CollisionEntity* staticObject, CollisionResult result);
	void separateEvenly(CollisionEntity* entity1, CollisionEntity* entity2, CollisionResult result);
};
#endif