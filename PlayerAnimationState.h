#include <Polycode.h>
#ifndef PLAYERANIMATIONINFO
#define PLAYERANIMATIONINFO

using namespace Polycode;
struct PlayerAnimationInfo
{
	float meshHeightOffset;
	float walkCyclePeriod;
	int walkPoseIndex;
	float walkStep;
	float dashWeight;
	int equipSwitchIndex;
	Vector2 spearInputVec;
};
#endif