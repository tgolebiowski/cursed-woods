#include "Goals.h"

Goal::Goal(NPC* npc, float priority)
{
	this->npc = npc;

	if (priority > 100.0f)
		priority = 100.0f;
	else if (priority < 0.0f)
		priority = 0.0f;
	this->priority = priority;
}

Goal::~Goal()
{

}

BeNextTo::BeNextTo(NPC* npc, Player* player, float priority) : Goal(npc, priority)
{
	this->player = player;
}

BeNextTo::~BeNextTo()
{

}

WorldState BeNextTo::getTargetState()
{
	Vector3 fromPtoNPC = npc->collisionCollider->getEntity()->getPosition() - 
		player->collisionSphere->getEntity()->getPosition();
	fromPtoNPC.y = 0.0f;
	fromPtoNPC.Normalize();
	fromPtoNPC.setLength(1.75f);

	WorldState desiredState = WorldState();
	desiredState.position = player->collisionSphere->getEntity()->getPosition() + fromPtoNPC;
	return desiredState;
}