#include <Polycode.h>
#ifndef AI_CORE_H
#define AI_CORE_H
#include "WorldState.h"
#include "micropather.h"

using namespace Polycode;
using namespace micropather;

class NPC;
class Player;
class Action;
class Goal;

struct ActionStep
{
	Action* action;
	WorldState predictedState;
};

class ActionPlanSolver : public Graph
{
public:
	std::vector<Action*> actions;
	ActionPlanSolver();

	void solve(std::vector<Action*>* plan, Goal* goal, WorldState startState);
	virtual float LeastCostEstimate( void* stateStart, void* stateEnd ); 
	virtual void AdjacentCost( void* state, MP_VECTOR< micropather::StateCost > *adjacent );
	virtual void PrintStateInfo( void* state );

private:
	MicroPather* micropather;
	ActionStep* currentTarget;
	WorldState worldStateTarget;
	std::vector<ActionStep*> createdActionSteps;
};

class NPC_Brain
{
public:
	NPC_Brain(NPC* npc, Player* player, std::vector<Goal*> goals, std::vector<Action*> actions);
	~NPC_Brain();

	void run();

private:
	NPC* npc;
	Player* player;
	ActionPlanSolver* solver;
	std::vector<Action*> currentPlan;

	std::vector<Goal*> goals;
	std::vector<Action*> actions;

	Goal* currentGoal;
	ActionStep* currentTarget;
	WorldState worldStateTarget;

	void updatePlanning();
	void createPlan(Goal* goal);
};


#endif