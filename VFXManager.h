#pragma once
#include <Polycode.h>
#ifndef VFX_H
#define VFX_H

using namespace Polycode;

class VFXManager;

class VEffect
{
public:

	VEffect(int length);
	virtual ~VEffect();

	virtual void update();
	static void setStatics(VFXManager* vfxManager, Scene* scene)
	{
		VEffect::vfxManager = vfxManager;
		VEffect::scene = scene;
	}

	bool killFlag;

protected:
	static Scene* scene;
	static VFXManager* vfxManager;
	int timeLeft;
};

class FlameEffect : public VEffect
{
public:
	FlameEffect(Vector3 center);
	~FlameEffect();

	void update();
	void toggle();
	bool burningToggle;
private:

	Vector3 center;
	BezierCurve flameCurve;
	int delayNewFlaneMesh;
	
	struct FlameMesh
	{
		SceneMesh* mesh;
		float currentT;
		float angle;
		float distance;

		FlameMesh(float angle, float distance)
		{
			this->angle = angle;
			this->currentT = 0.0f;
			this->distance = distance;

			mesh = new SceneMesh(Mesh::TRI_MESH);
			mesh->getMesh()->indexedMesh = true;
			
			//Vector3 v1 = Vector3(0.0, std::sqrt(2.0/3.0) - (1.0/(2.0*std::sqrt(6.0))), 0.0);
			Vector3 v1 = Vector3(0.0, 1.2, 0.0);
			Vector3 v2 = Vector3(-1.0/(2.0 * std::sqrt(3.0)), - (1.0/(2.0*std::sqrt(6.0))), -0.5f);
			Vector3 v3 = Vector3(-1.0/(2.0 * std::sqrt(3.0)), - (1.0/(2.0*std::sqrt(6.0))), 0.5f);
			Vector3 v4 = Vector3(1.0/std::sqrt(3.0), - (1.0/(2.0*std::sqrt(6.0))), 0.0f);
			Vector3 v5 = Vector3(0.0, -1.0, 0.0);
			//Vector3 v5 = Vector3(0.0, -(std::sqrt(2.0/3.0) - (1.0/(2.0*std::sqrt(6.0)))), 0.0);

			mesh->getMesh()->addVertex(v1.x, v1.y, v1.z);
			mesh->getMesh()->addVertex(v2.x, v2.y, v2.z);
			mesh->getMesh()->addVertex(v3.x, v3.y, v3.z);
			mesh->getMesh()->addVertex(v4.x, v4.y, v4.z);
			mesh->getMesh()->addVertex(v5.x, v5.y, v5.z);
			mesh->getMesh()->addIndexedFace(0,1,2);
			mesh->getMesh()->addIndexedFace(0,2,3);
			mesh->getMesh()->addIndexedFace(0,3,1);
			mesh->getMesh()->addIndexedFace(2,1,4);
			mesh->getMesh()->addIndexedFace(3,2,4);
			mesh->getMesh()->addIndexedFace(1,3,4);
			mesh->getMesh()->calculateNormals();
			mesh->setColor(240.0/255.0, 167.0/255.0, 30.0/255.0, 1.0f);
		};
	};
	std::vector<FlameMesh*> flameList;
};

class WarmingEffect : public VEffect
{
public: 
	WarmingEffect(Vector3 center, float strength);
	~WarmingEffect();

	void update();
private:
	std::vector<SceneLine*> lines;
	std::vector<float> heightOffsets;
	Vector3 center;
	BezierCurve curve;
};

class CurseAddEffect : public VEffect
{
public:
	CurseAddEffect(Vector3 start);
	~CurseAddEffect(void);

	void update();

private:
	struct CurseBubbleStatus
	{
		int tick;
		float angle;
		ScenePrimitive* attachedCloud;
	};

	Vector3 start;
	BezierCurve riseCurve;
	const Color curseColor = Color(56.0 / 255.0, 29.0 / 255.0, 61.0 / 255.0, 1.0);

	std::vector<CurseBubbleStatus> info;
};

class PointLaserEffect : public VEffect
{
public: 
	PointLaserEffect(Vector3 start, Vector3 direction);
	~PointLaserEffect();

	void update();
private:
	SceneLine* laser;
};

class VFXManager
{
public:
	VFXManager(Scene* scene);
	~VFXManager(void);

	void update();
	void addEffect(VEffect* effect);
	void deleteEffect(VEffect* effect);
private:
	Scene* scene;
	std::vector<VEffect*> effects;
};
#endif
