#include "GameLogicHub.h"

GameLogicHub::GameLogicHub()
{

}

GameLogicHub::GameLogicHub(CollisionScene* scene, Vector2 screenSize, Input* input)
{
	this->scene = scene;
	this->input = input;
	this->screenSize = screenSize;

	worldMap = new WorldMap();
	lightShaderHub = new LightShaderHub(screenSize);
	vfxManager = new VFXManager(scene);

	player = new Player(worldMap);
	npcManager = new NPCManager(player, vfxManager);
	physicsManager = new PhysicsManager(&npcManager->npcList);

	gameCam = new GameCam(scene, Vector3(0.0, std::sin(120.0f) * 30.0, -std::cos(120.0f) * 30.0), screenSize);

	transitioning = false;
	transitionDelayLeft = 0;

	loader = new LevelSaverLoader(player, vfxManager);
	CoreServices::getInstance()->getCore()->createThread(loader);

	bool done = false;
	WorldGeneration generator = WorldGeneration();
	generator.generateBigDebugWorld(worldMap, loader);

	done = false;
	loader->loadWorldWAD(this->worldMap, &done);

	Artifact::setGlobalVars(scene, worldMap, player, vfxManager, 
		this->allLevelState, loader->worldWAD.nodeCount);

	for (int i = 0; i < loader->worldWAD.nodeCount; i++)
	{
		bool done = false;
		allLevelState[i] = LevelStateInfo();
		loader->loadLevelStateFromFile(i, &allLevelState[i], &done);
	}

    done = false;
	LevelLayoutStruct firstLevelLoadInfo;
	int randIndex = rand() % worldMap->nodes.size();
	loader->loadLevelLayoutFromFile(randIndex, &firstLevelLoadInfo, &done);
	worldMap->current = randIndex;

	Level::scene = scene;
	currentLevel = new Level();
	done = false;
	loader->levelStructsToLevel(&firstLevelLoadInfo, &allLevelState[randIndex], &(*allLevelState), 
		loader->worldWAD.nodeCount, currentLevel, &done);

	currentLevel->loadin(player->playerState->position);
	scene->addEntity(currentLevel);

	lightShaderHub->setStaticShadowMap(currentLevel->groundLightMap);
	checkLoadedLevels();

	player->addToScene(scene);
	physicsManager->level = currentLevel;
	physicsManager->scene = scene;
	physicsManager->playerCollider = player->collisionSphere;

	Vector3 playerToTransition = currentLevel->transitionColliders.at(0)->getPosition() - 
		player->collisionSphere->getEntity()->getPosition();
	scriptVec = Vector2(playerToTransition.x, playerToTransition.z);
	scriptVec.Normalize();

	float shortestDistance = -1.0f;
	int nodeWithClosestBinding = -1;
	for (int i = 0; i < worldMap->nodes.size(); i++)
	{
		if (i == worldMap->current) continue;

		if (player->playerState->currentCompassBinding == PlayerState::CompassBinding::Pointer)
		{
			if (allLevelState[i].artifactType == LevelStateInfo::ArtifactTypes::Pointer)
			{
				float distance = (worldMap->nodes[worldMap->current]->position 
					- worldMap->nodes[i]->position).length();
				if (distance < shortestDistance || shortestDistance == -1.0f)
				{
					shortestDistance = distance;
					nodeWithClosestBinding = i;
				}
			}
		}
	}
	Vector2 newCompassDirection = worldMap->nodes[nodeWithClosestBinding]->position 
		- worldMap->nodes[worldMap->current]->position;
	newCompassDirection.Normalize();
	player->setCompassPointVector2(newCompassDirection);
}

GameLogicHub::~GameLogicHub(void)
{

}

void GameLogicHub::update()
{
	P_UpdatePacket packet = input->createPInputPacket();
	packet.moveInput = Vector2(1, 1);
	this->player->update(packet);
	this->npcManager->update();
	this->physicsManager->update();
	this->gameCam->update(this->player->playerState->position);
	this->currentLevel->level_update(player->playerState->position);

	vfxManager->update();
	lightShaderHub->update(player->playerState->position);

	if(!transitioning)
	{
		CollisionResult transitionTest;
		for(int i = 0; i < currentLevel->getTransitions()->size(); i++)
		{
			CollisionScene* scene = currentLevel->scene;
			transitionTest = scene->testCollision(currentLevel->getTransitions()->at(i), player->collisionSphere->getEntity());

			if(transitionTest.collided)
			{
				transitioning = true;
				transitionDelayLeft = 50;
				nextLevel = currentLevel->neighboringLevels.at(i);
			}
		}
	}

	if(transitioning)
	{
		if(transitionDelayLeft == 0)
		{
			switchToLevel(nextLevel);
			checkLoadedLevels();
		}
		transitionDelayLeft--;
	}

	for (int i = 0; i < requestedLevels.size(); i++)
	{
		if (requestedLevels.at(i)->donePointer)
		{
			levelsInMemory.push_back(requestedLevels.at(i)->levelPointer);
			delete requestedLevels.at(i);
			requestedLevels.erase(requestedLevels.begin() + i);
			i--;
		}
	}
}

void GameLogicHub::switchToLevel(int identifier)
{
	scriptVec = -scriptVec;

	for (int i = 0; i < npcManager->npcList.size(); i++)
		npcManager->npcList.at(i)->removeFromScene(currentLevel->scene);

	int lastLevelIdentifier = currentLevel->identifier;
	levelsInMemory.push_back(currentLevel);

	for (int i = 0; i < requestedLevels.size(); i++)
	{
		if (requestedLevels.at(i)->identifier == identifier)
		{
			//hang until level is ready
			while (!requestedLevels.at(i)->donePointer)
			{

			}

			levelsInMemory.push_back(requestedLevels.at(i)->levelPointer);
			requestedLevels.erase(requestedLevels.begin() + i);
			break;
		}
	}

	currentLevel->unload();
	scene->removeEntity(currentLevel);
	for(int i = 0; i < levelsInMemory.size(); i++)
	{
		if(levelsInMemory.at(i)->identifier == identifier)
		{
			currentLevel = levelsInMemory.at(i);
			scene->addEntity(currentLevel);
			levelsInMemory.erase(levelsInMemory.begin() + i);
			break;
		}
	}
	worldMap->current = currentLevel->identifier;

	float shortestDistance = -1.0f;
	int nodeWithClosestBinding = -1;
	for (int i = 0; i < worldMap->nodes.size(); i++)
	{
		if (player->playerState->currentCompassBinding == PlayerState::CompassBinding::Pointer)
		{
			if (allLevelState[i].artifactType == LevelStateInfo::ArtifactTypes::Pointer)
			{
				float distance = (worldMap->nodes[worldMap->current]->position 
					- worldMap->nodes[i]->position).length();
				if (distance < shortestDistance || shortestDistance == -1.0f)
				{
					shortestDistance = distance;
					nodeWithClosestBinding = i;
				}
			}
		}
	}
	Vector2 newCompassDirection = worldMap->nodes[nodeWithClosestBinding]->position 
		- worldMap->nodes[worldMap->current]->position;
	if (newCompassDirection.x != 0.0 && newCompassDirection.y != 0.0) newCompassDirection.Normalize();
	player->setCompassPointVector2(newCompassDirection);

	Vector2 startPosition;
	for(int i = 0; i < currentLevel->neighboringLevels.size(); i++)
	{
		if(lastLevelIdentifier == currentLevel->neighboringLevels.at(i))
		{
			startPosition = currentLevel->playerStarts.at(i);
			break;
		}
	}

	Vector3 playerPosition = Vector3(startPosition.x, 0.5f, startPosition.y);
	currentLevel->loadin(playerPosition);

	player->collisionSphere->getEntity()->setPosition(playerPosition);
	player->playerState->position = playerPosition;

	physicsManager->level = currentLevel;

	currentLevel->level_update(playerPosition);

	lightShaderHub->setStaticShadowMap(currentLevel->groundLightMap);

	transitioning = false;
	transitionDelayLeft = 0;
}

void GameLogicHub::checkLoadedLevels()
{
	for (int i = 0; i < levelsInMemory.size(); i++)
	{
		int identifier = levelsInMemory.at(i)->identifier;
		bool stillNeeded = false;
		for (int j = 0; j < currentLevel->neighboringLevels.size(); j++)
		{
			if (currentLevel->neighboringLevels.at(j) == identifier) { stillNeeded = true; break; }
		}

		if (!stillNeeded)
		{
			loader->deleteLevelRequests.push_back(DeleteLevelRequest(levelsInMemory.at(i), &allLevelState[i]));
			levelsInMemory.erase(levelsInMemory.begin() + i);
			i--;
		}
	}

	for (int i = 0; i < currentLevel->neighboringLevels.size(); i++)
	{
		int identifier = currentLevel->neighboringLevels.at(i);
		bool inMemory = false;
		for (int j = 0; j < levelsInMemory.size(); j++)
		{
			if (levelsInMemory.at(j)->identifier == identifier) { inMemory = true; break; }
		}

		if (!inMemory)
		{
			Level* allocatedSpace = new Level();
			LevelStateInfo* state = &allLevelState[identifier];

			RequestedLevelInfo* newRequestSave = new RequestedLevelInfo();
			newRequestSave->donePointer = false;
			newRequestSave->identifier = identifier;
			newRequestSave->levelPointer = allocatedSpace;

			LoadLevelRequest request = LoadLevelRequest(&newRequestSave->donePointer, identifier, allocatedSpace,
				&(allLevelState[identifier]), &(*allLevelState), (int)worldMap->nodes.size());
			loader->loadLevelRequests.push_back(request);
			requestedLevels.push_back(newRequestSave);
		}
	}
}
