#include "MeshBatch.h"

MeshBatch::MeshBatch() : Entity()
{

}


MeshBatch::~MeshBatch()
{
	for (int i = 0; i < batches.size(); i++)
	{
		delete batches.at(i);
	}

	for (int i = 0; i < vboBatches.size(); i++)
	{
		delete vboBatches.at(i);
	}
}

void MeshBatch::addMesh(SceneMesh* mesh, String name)
{
	BatchInfo* batch = NULL;
	for (int i = 0; i < batches.size(); i++)
	{
		if (batches.at(i)->name == name)
		{
			batch = batches.at(i);
			break;
		}
	}

	if (!batch)
	{
		batch = new BatchInfo(mesh);
		batch->name = name;
		batches.push_back(batch);
	}

	batch->info[batch->count].color = mesh->getCombinedColor();
	batch->info[batch->count].matrix = mesh->getConcatenatedMatrix();
	batch->count++;
}

void MeshBatch::addVBOMesh(SceneMesh* mesh, String name)
{
	VBOBatch* batch = NULL;
	for (int i = 0; i < vboBatches.size(); i++)
	{
		if (vboBatches.at(i)->name == name)
		{
			batch = vboBatches.at(i);
			break;
		}
	}

	if (!batch)
	{
		batch = new VBOBatch(mesh);
		batch->name = name;
		vboBatches.push_back(batch);
	}

	batch->drawCallInfo.push_back(DrawCallInfo());
	batch->drawCallInfo.back().color = mesh->getCombinedColor();
	batch->drawCallInfo.back().matrix = mesh->getConcatenatedMatrix();
	batch->count++;
}

void MeshBatch::clearBatch(String name)
{
	for (int i = 0; i < batches.size(); i++)
	{
		if (batches.at(i)->name == name)
		{
			BatchInfo* batch = batches.at(i);
			batches.erase(batches.begin() + i);
			delete batch;
			return;
		}
	}

	for (int i = 0; i < vboBatches.size(); i++)
	{
		if (vboBatches.at(i)->name == name)
		{
			VBOBatch* batch = vboBatches.at(i);
			vboBatches.erase(vboBatches.begin() + i);
			delete batch;
			return;
		}
	}
}

void MeshBatch::clearBatch(int index)
{
	if (vboBatches.size() > index)
	{
		VBOBatch* batch = vboBatches.at(index);
		vboBatches.erase(vboBatches.begin() + index);
		delete batch;
	}
}


void MeshBatch::Render()
{
	OpenGLRenderer* renderer = (OpenGLRenderer*)CoreServices::getInstance()->getRenderer();
	renderer->enableDepthWrite(true);
	renderer->enableDepthTest(true);
	renderer->enableAlphaTest(true);
	renderer->enableBackfaceCulling(true);

	for (int i = 0; i < batches.size(); i++)
	{
		BatchInfo* batch = batches.at(i);

		if (batch->material)
		{
			renderer->applyMaterial(batch->material, batch->binding, 0, false);
		}
		else
		{
			if (batch->meshTexture)
			{
				renderer->setTexture(batch->meshTexture);
			}
			else{
				renderer->setTexture(NULL);
			}
		}
		
		renderer->pushRenderDataArray(&batch->positions);
		renderer->pushRenderDataArray(&batch->normals);
		//renderer->pushRenderDataArray(&batch->tangents);
		renderer->pushRenderDataArray(&batch->texCoords);

		for (int i = 0; i < batch->count; i++)
		{
			renderer->pushVertexColor();
			renderer->pushMatrix();
			renderer->multiplyVertexColor(batch->info[i].color);
			renderer->multModelviewMatrix(batch->info[i].matrix);
			//renderer->drawBatchedArray(&batch->indexArray);
			renderer->popMatrix();
			renderer->popVertexColor();
		}

		renderer->clearShader();
		renderer->setTexture(NULL);
		//renderer->endBatchDraw();
	}

	for (int i = 0; i < vboBatches.size(); i++)
	{
		VBOBatch* batch = vboBatches.at(i);

		//renderer->bindVertexBuffer(batch->buffer);
		if (batch->material)
		{
			renderer->applyMaterial(batch->material, batch->binding, 0, false);
		}
		else
		{
			if (batch->texture)
			{
				renderer->setTexture(batch->texture);
			}
			else{
				renderer->setTexture(NULL);
			}
		}

		for (int i = 0; i < batch->count; i++)
		{
			renderer->pushMatrix();
			renderer->multModelviewMatrix(batch->drawCallInfo[i].matrix);
			renderer->pushVertexColor();
			renderer->multiplyVertexColor(batch->drawCallInfo[i].color);
			//renderer->drawBoundVertexBuffer(batch->buffer);
			renderer->popMatrix();
			renderer->popVertexColor();
		}

		renderer->clearShader();
		renderer->setTexture(NULL);
		//renderer->endBatchDraw();
	}
}