#include "Artifact.h"

WorldMap* Artifact::map = NULL;
VFXManager* Artifact::vfxManager = NULL;
Player* Artifact::player = NULL;
CollisionScene* Artifact::scene = NULL;
LevelStateInfo* Artifact::levelStatesArray = NULL;
int Artifact::stateArrayLength = 0;

Artifact::Artifact(LevelStateInfo* state)
{
	this->state = state;
}

Artifact::~Artifact()
{
	for (int i = 0; i < meshes.size(); i++)
	{
		delete meshes.at(i);
	}

	for (int i = 0; i < colliders.size(); i++)
	{
		delete colliders.at(i);
	}
}

void Artifact::setGlobalVars(CollisionScene* scene, WorldMap* map, Player* player, VFXManager* vfxManager,
	LevelStateInfo* levelStateArray, int arraySize)
{
	Artifact::scene = scene;
	Artifact::map = map;
	Artifact::player = player;
	Artifact::vfxManager = vfxManager;
	Artifact::levelStatesArray = levelStateArray;
	Artifact::stateArrayLength = arraySize;
}

void Artifact::loadin()
{
	for (int i = 0; i < meshes.size(); i++)
	{
		scene->addEntity(meshes.at(i));
	}

	for (int i = 0; i < colliders.size(); i++)
	{
		int colliderType = 0;
		if (ScenePrimitive::TYPE_CYLINDER == colliders.at(i)->getPrimitiveType())
		{
			colliderType = CollisionEntity::SHAPE_CYLINDER;
		}
		scene->addCollisionChild(colliders.at(i), colliderType);
	}
}

void Artifact::unload()
{
	for (int i = 0; i < meshes.size(); i++)
	{
		scene->removeEntity(meshes.at(i));
	}

	for (int i = 0; i < colliders.size(); i++)
	{
		scene->removeCollision(colliders.at(i));
		scene->removeEntity(colliders.at(i));
	}
}

void Artifact::bindMeshesToSceneryShader()
{
	for (int i = 0; i < meshes.size(); i++)
	{
		meshes.at(i)->setMaterialByName("SceneryShader");
		meshes.at(i)->rebuildRotation();
		meshes.at(i)->rebuildTransformMatrix();
		meshes.at(i)->getLocalShaderOptions()->addParam(ProgramParam::PARAM_MATRIX, "transformMatrix")
			->setMatrix4(meshes.at(i)->getConcatenatedMatrix());
		Quaternion q = meshes.at(i)->getRotationQuat();
		meshes.at(i)->getLocalShaderOptions()->addParam(ProgramParam::PARAM_COLOR, "rotationQuat")
			->setColor(Color(q.w, q.x, q.y, q.z));
	}
}

CurseCurer::CurseCurer(Vector2 center, LevelStateInfo* state) 
	: Artifact(state)
{
	 SceneMesh* debugMesh = new ScenePrimitive(ScenePrimitive::TYPE_SPHERE, 1.0f, 6.0, 6.0, 1.0);
	debugMesh->setPosition(center.x, 1.0f, center.y);
	debugMesh->billboardMode = true;
	meshes.push_back(debugMesh);
}

void CurseCurer::update()
{
	float distanceFromPlayer = (player->playerState->position - meshes.at(0)->getPosition()).length();

	if (distanceFromPlayer < 8.0f)
	{
		meshes.at(0)->setColor(1, 0, 0, 1);
	}
	else
	{
		meshes.at(0)->setColor(1, 1, 1, 1);
	}
}

AuraShrine::AuraShrine(Vector2 center, LevelStateInfo* state) : Artifact(state)
{
	 SceneMesh* debugMesh = new ScenePrimitive(ScenePrimitive::TYPE_SPHERE, 1.0f, 6.0, 6.0, 1.0);
	debugMesh->setPosition(center.x, 1.0f, center.y);
	debugMesh->billboardMode = true;
	meshes.push_back(debugMesh);
}

void AuraShrine::update()
{
	float distanceFromPlayer = (player->playerState->position - meshes.at(0)->getPosition()).length();

	if (distanceFromPlayer < 8.0f)
	{
		meshes.at(0)->setColor(1, 0, 0, 1);
	}
	else
	{
		meshes.at(0)->setColor(1, 1, 1, 1);
	}
}

CompassAlignment::CompassAlignment(Vector2 center, LevelStateInfo* state) : Artifact(state)
{
	 SceneMesh* debugMesh = new ScenePrimitive(ScenePrimitive::TYPE_SPHERE, 1.0f, 6.0, 6.0, 1.0);
	debugMesh->setPosition(center.x, 1.0f, center.y);
	debugMesh->billboardMode = true;
	meshes.push_back(debugMesh);
}

void CompassAlignment::update()
{
	float distanceFromPlayer = (player->playerState->position - meshes.at(0)->getPosition()).length();

	if (distanceFromPlayer < 8.0f)
	{
		meshes.at(0)->setColor(1, 0, 0, 1);
	}
	else
	{
		meshes.at(0)->setColor(1, 1, 1, 1);
	}
}

Pointer::Pointer(Vector2 center, LevelStateInfo* state) : Artifact(state)
{
	SceneMesh* post = new SceneMesh("Data/Artifacts/Pointer-Post.mesh");
	post->setPosition(center.x, 0.0f, center.y);
	post->setScale(0.9, 0.9, 0.9);
	post->setColor(ColorTable::purple_gray);
	meshes.push_back(post);

	SceneMesh* pointer = new SceneMesh("Data/Artifacts/Pointer-Top.mesh");
	pointer->setPosition(center.x, post->getHeight() * 0.50f * post->getScale().y, center.y);
	pointer->setScaleY(0.65);
	pointer->setScaleX(0.45);
	pointer->setScaleZ(0.45);
	pointer->setColor(ColorTable::lightGray_littlePurple);
	meshes.push_back(pointer);

	bindMeshesToSceneryShader();

	ScenePrimitive* collider = new ScenePrimitive(ScenePrimitive::TYPE_CYLINDER, 1.5, 0.65, 7);
	collider->setPosition(center.x, 0.75, center.y);
	collider->visible = false;
	colliders.push_back(collider);

	storedEmber = 0.0f;

	//if (state != NULL)
		//storedEmber = state->artifactSavedValue1;
}

Pointer::~Pointer()
{

}

void Pointer::update()
{ 
	const float storedEmberBurnRate = 1.0f;
	if (player->playerState->currentEquip == PlayerState::Equipment::Ember)
	{
		CollisionResult emberTest = scene->testCollision(player->emberSphere->getEntity(), colliders.back());
		if (emberTest.collided && player->playerState->emberExtended)
		{
			storedEmber += player->spendEmber();
			new WarmingEffect(player->emberSphere->getEntity()->getCombinedPosition(), 1.0f);
		}
		else
		{
			if (storedEmber > 0.0f) storedEmber -= storedEmberBurnRate;
		}
	}
	else
	{
		if (storedEmber > 0.0f) storedEmber -= storedEmberBurnRate;
	}

	const float storedEmberRequired = 15.0f;

	if (storedEmber > storedEmberRequired)
	{
		Vector3 start = meshes.at(1)->getPosition();
		Vector3 directionOfPoint = Vector3(-1.0f, 0.0f, 1.0f);

		for (int i = 0; i < stateArrayLength; i++)
		{
			if (levelStatesArray[i].artifactType == LevelStateInfo::ArtifactTypes::Torch)
			{
				WorldMap::MapNode* myNode = map->nodes[map->current];
				WorldMap::MapNode* torchNode = map->nodes[i];
				Vector2 direction = torchNode->position - myNode->position;

				directionOfPoint = Vector3(direction.x, 0.0f, direction.y);
				break;
			}
		}

		new PointLaserEffect(start, directionOfPoint);
	}

	state->artifactSavedValue1 = storedEmber;
}

Ember::Ember(Vector2 center, LevelStateInfo* state) : Artifact(state)
{
	 SceneMesh* debugMesh = new ScenePrimitive(ScenePrimitive::TYPE_SPHERE, 1.0f, 6.0, 6.0, 1.0);
	debugMesh->setPosition(center.x, 1.0f, center.y);
	debugMesh->billboardMode = true;
	meshes.push_back(debugMesh);
}

void Ember::update()
{
	float distanceFromPlayer = (player->playerState->position - meshes.at(0)->getPosition()).length();

	if (distanceFromPlayer < 8.0f)
	{
		meshes.at(0)->setColor(1, 0, 0, 1);
	}
	else
	{
		meshes.at(0)->setColor(1, 1, 1, 1);
	}
}

DivinationShrine::DivinationShrine(Vector2 center, LevelStateInfo* state) : Artifact(state)
{
	SceneMesh* debugMesh = new ScenePrimitive(ScenePrimitive::TYPE_SPHERE, 1.0f, 6.0, 6.0, 1.0);
	debugMesh->setPosition(center.x, 1.0f, center.y);
	debugMesh->billboardMode = true;
	meshes.push_back(debugMesh);
}

void DivinationShrine::update()
{
	float distanceFromPlayer = (player->playerState->position - meshes.at(0)->getPosition()).length();

	if (distanceFromPlayer < 8.0f)
	{
		meshes.at(0)->setColor(1, 0, 0, 1);
	}
	else
	{
		meshes.at(0)->setColor(1, 1, 1, 1);
	}
}

ItemPickup::ItemPickup(Vector2 center, LevelStateInfo* state) : Artifact(state)
{
	SceneMesh* debugMesh = new ScenePrimitive(ScenePrimitive::TYPE_SPHERE, 1.0f, 6.0, 6.0, 1.0);
	debugMesh->setPosition(center.x, 1.0f, center.y);
	debugMesh->billboardMode = true;
	meshes.push_back(debugMesh);
}

void ItemPickup::update()
{
	float distanceFromPlayer = (player->playerState->position - meshes.at(0)->getPosition()).length();

	if (distanceFromPlayer < 8.0f)
	{
		meshes.at(0)->setColor(1, 0, 0, 1);
	}
	else
	{
		meshes.at(0)->setColor(1, 1, 1, 1);
	}
}

Torch::Torch(Vector2 center, LevelStateInfo* state) : Artifact(state)
{
	tempMesh = new ScenePrimitive(ScenePrimitive::TYPE_CYLINDER, 0.5, 0.66, 8);
	tempMesh->setPosition(center.x, 0.25, center.y);
	tempMesh->setColor(ColorTable::yellow_gray);
	scene->addEntity(tempMesh);

	storedEmber = state->artifactSavedValue1;
}

void Torch::update()
{
	CollisionResult emberTest = scene->testCollision(player->emberSphere->getEntity(), colliders.back());
	if (emberTest.collided && player->playerState->emberExtended)
	{
		storedEmber += player->spendEmber();
	}

}