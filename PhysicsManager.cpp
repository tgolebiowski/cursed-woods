#include "PhysicsManager.h"

PhysicsManager::PhysicsManager(std::vector<NPC*>* npcList)
{
	playerCollider = NULL;
	scene = NULL;

	this->npcList = npcList;
}


PhysicsManager::~PhysicsManager(void)
{

}

void PhysicsManager::separateUnevenly(CollisionEntity* entity, CollisionEntity* staticObject, CollisionResult result)
{
	if(std::abs(result.colDist) < 0.0001) return;

	Vector3 adjust = result.colNormal;
	adjust.setLength(result.colDist);
	adjust.setLength(adjust.length() + std::abs((double)adjust.y));
	adjust.y = 0.0f;

	Vector3 entityToObject = staticObject->getEntity()->getPosition() - entity->getEntity()->getPosition();
	float dotTest = entityToObject.dot(adjust);

	if(dotTest > 0)
	{
		adjust = adjust * -1;
	}

	entity->getEntity()->setPosition(entity->getEntity()->getPosition() + adjust);
}

void PhysicsManager::separateEvenly(CollisionEntity* entity1, CollisionEntity* entity2, CollisionResult result)
{
	if(std::abs(result.colDist) < 0.0001f) return;

	Vector3 adjust = result.colNormal;
	adjust.setLength(result.colDist);
	adjust.setLength((adjust.length() + std::abs((double)adjust.y))/2.0f);
	adjust.y = 0.0f;

	Vector3 entityToObject = entity1->getEntity()->getPosition() - entity2->getEntity()->getPosition();
	float dotTest = entityToObject.dot(adjust);

	if(dotTest > 0)
	{
		adjust = adjust * -1;
	}

	entity1->getEntity()->setPosition(entity1->getEntity()->getPosition() - adjust);
	entity2->getEntity()->setPosition(entity2->getEntity()->getPosition() + adjust);
}

void PhysicsManager::update()
{
	//Check player-environment collisions
	std::vector<CollisionEntity*>* enviroColliders = level->getColliders();
	for(int i = 0; i < enviroColliders->size(); i++)
	{
		CollisionResult result = scene->testCollision(playerCollider->getEntity(), 
			enviroColliders->at(i)->getEntity());

		if(result.collided) separateUnevenly(playerCollider, enviroColliders->at(i), result);
	}	

	if (level->artifact != NULL)
	{
		for (int i = 0; i < level->artifact->colliders.size(); i++)
		{
			CollisionResult result = scene->testCollision(playerCollider->getEntity(), 
				level->artifact->colliders.at(i));

			if (result.collided) separateUnevenly(playerCollider, 
				scene->getCollisionByScreenEntity(level->artifact->colliders.at(i)), result);
		}
	}

	//Check npc-enviroment collisions
	for(int i = 0; i < npcList->size(); i++)
	{
		Entity* npc = npcList->at(i)->collisionCollider->getEntity();
		CollisionEntity* hitBox = npcList->at(i)->collisionCollider;

		for(int j = 0; j < enviroColliders->size(); j++)
		{
			CollisionResult result = scene->testCollision(npc, enviroColliders->at(j)->getEntity());

			if(result.collided) separateUnevenly(hitBox, enviroColliders->at(j), result);
		}

		CollisionResult playerResult = scene->testCollision(playerCollider->getEntity(), hitBox->getEntity());
		if(playerResult.collided) separateUnevenly(playerCollider, hitBox, playerResult);
	}

	for(int i = 0; i < npcList->size(); i++)
	{
		CollisionEntity* npc1 = npcList->at(i)->collisionCollider;

		for(int j = i + 1; j < npcList->size(); j++)
		{
			CollisionEntity* npc2 = npcList->at(j)->collisionCollider;
			CollisionResult result = scene->testCollision(npc1->getEntity(), npc2->getEntity());

			if(result.collided) separateEvenly(npc1, npc2, result);
		}
	}
}